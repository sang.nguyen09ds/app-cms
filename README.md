Follow the following steps and you're good to go!

1: Install GIT and nodejs

https://git-scm.com/downloads

https://nodejs.org/

2: Clone repo

3: Install packages

```
npm install
```
or use Yarn

```
yarn install
```

4: Start server (includes auto refreshing) and gulp watcher
```
npm start
```

Open browser to [`http://localhost:4200`](http://localhost:4200)

You need to instal CORS - a Chrome extension to run the project on local.

--Good Luck To You--

