var that = this;
var socket = require('socket.io'), http = require('http'),
    server = http.createServer(), socket = socket.listen(server);

setInterval(function () {
    var date = new Date();

    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    //console.log(hours + ":" + minutes + ":" + seconds);
    if (
        (parseInt(hours, 10) === 7 && parseInt(minutes, 10) === 0 && parseInt(seconds, 10) === 0) ||
        (parseInt(hours, 10) === 11 && parseInt(minutes, 10) === 0 && parseInt(seconds, 10) === 0) ||
        (parseInt(hours, 10) === 13 && parseInt(minutes, 10) === 0 && parseInt(seconds, 10) === 0) ||
        (parseInt(hours, 10) === 17 && parseInt(minutes, 10) === 0 && parseInt(seconds, 10) === 0)
    ) {
        action = {"action": "get_new_message", "description": "Tới giờ gọi API get notify rồi bé."};
        console.log(action);
        socket.emit('message', action);
    }
}, 1000);

socket.on('connection', function (connection) {
    var data = connection.handshake

    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();

    console.log("New connection from: " + connection.request.connection.remoteAddress +
        " at " + year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds);
    connection.on('message', function (msg) {
        console.log('Received (message): ' + JSON.stringify(msg));
        socket.emit('message', msg);
    });
    connection.on('culture_msg', function (msg) {
        console.log('Received (culture_msg): ' + JSON.stringify(msg));
        socket.emit('culture_msg', msg);
    });
    connection.on('system_msg', function (msg) {
        console.log('Received (system_msg): ' + JSON.stringify(msg));
        socket.emit('system_msg', msg);
    });
}.bind(this));

server.listen(3000, function () {
    console.log('Server listening...');
});
