// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const PAGE_URL = 'http://ssc.com.vn';
const DOMAIN_URL = 'http://ssc.kpis.vn';
const API_URL = 'ssc.local';
const DOMAINS = ['app-cms.local', 'ssc.local'];
const TOKEN = 'id_token';

export const environment = {
  production: true,
  API: {
    'apiBase': API_URL + '/v0',
    'apiCMS': 'http://2nong.vn/wp-json/wp/v2',
    'apiMaster': API_URL + '/v1',
    'apiCommon': API_URL + '/v1',
    'apiAuthen': API_URL + '/auth'
  },
  PAGE_URL: PAGE_URL,
  whitelistedDomains: DOMAINS,
  token: TOKEN
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
