import { Routes } from '@angular/router';
import { LoginComponent } from './components/front/login/login.component';
import { Page404Component } from './components/front/pages/index';
import {
  ForgotComponent,
  ForgotNoticeComponent,
  ResetComponent,
  SetupPasswordComponent
} from './components/front/password/index';
import { RegisterComponent } from './components/front/register/register.component';

export const ROUTES: Routes = [
  { path: 'forgot-password', component: ForgotComponent },
  { path: 'forgot-password-notice', component: ForgotNoticeComponent },
  { path: 'reset-password', component: ResetComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'setup-password', component: SetupPasswordComponent },
  { path: '', loadChildren: './components/home/home.module#HomeModule' },
  { path: '**', component: Page404Component }
];
