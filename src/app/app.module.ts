import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { StoreModule } from '@ngrx/store';
import { ToastyModule } from 'ng2-toasty';
import { NgxSpinnerModule } from 'ngx-spinner';
import { environment } from 'src/environments/environment';
import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { DenyComponent } from './components/front/deny/deny.component';
import { LoginComponent } from './components/front/login/login.component';
import { Page404Component } from './components/front/pages/index';
import {
  ForgotComponent,
  ForgotNoticeComponent,
  ResetComponent,
  SetupPasswordComponent
} from './components/front/password/index';
import { RegisterComponent } from './components/front/register/register.component';
import { HomeModule } from './components/home/index';
import { appReducer } from './reducers';
import { HttpClient, SharedModule } from './shared/shared.module';
import { AuthGuard } from './shared/_guards';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

export function tokenGetter() {
  return localStorage.getItem(environment.token);
}
@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    LoginComponent,
    RegisterComponent,
    ForgotComponent,
    ResetComponent,
    ForgotNoticeComponent,
    SetupPasswordComponent,
    DenyComponent
  ],
  imports: [
    HttpClientModule,
    NgxSpinnerModule,
    StoreModule.forRoot({ appState: appReducer }),
    BrowserAnimationsModule,
    NgxChartsModule,
    SharedModule,
    ToastyModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: environment.whitelistedDomains
      }
    }),
    RouterModule.forRoot(ROUTES, {
      useHash: true,
      preloadingStrategy: PreloadAllModules
    }),
    HomeModule,
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    PopoverModule.forRoot(),
    TooltipModule.forRoot()
  ],
  providers: [HttpClient, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
