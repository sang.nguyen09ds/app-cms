import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, BsDatepickerModule } from 'ngx-bootstrap';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { AssigneesPopUpComponent } from './components/assignees-pop-up/assignees-pop-up.component';
import { AutoCompleteDirectiveComponent } from './components/autocomplete/autocomplete';
import { AutocompleteService } from './components/autocomplete/autocomplete.service';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb';
import { ContactFormComponent } from './components/contact-form/contact-form';
import { ContactFormServices } from './components/contact-form/contact-form.service';
import { CustomInputComponent } from './components/custom-input/custom-input.component';
import { ExportsComponent } from './components/exports/exports';
import { GoBackComponent } from './components/goback/goback.directive';
import { HeaderComponent } from './components/header/header';
import { ImportDirectiveComponent } from './components/import/import';
import { MainBodyComponent } from './components/main-body/main-body';
import { MainMenuComponent } from './components/main-menu/main-menu';
import { WMSMessageComponent } from './components/messages/messages';
import {
  PaginatePipe,
  PaginationControlsCmpComponent,
  PaginationService
} from './components/pagination/ng2-pagination';
import { WMSPaginationComponent } from './components/pagination/wms-pagination';
import { PopupLoginComponent } from './components/popup-login/popup-login';
import { ProgressBarComponent } from './components/progress-bar/progress-bar';
import { Functions } from './functions';
import { OrderBy } from './pipes/order-by.pipe';
import { KeysPipe } from './pipes/pipes';
import { SearchPipe } from './pipes/search.pipe';
import { UppercaseToSpaceLowerPipe } from './pipes/string.pipe';
import { WMSNumberFormatter } from './pipes/wms-number-formatter';
import { BomService } from './services/bom.service';
import { APIConfig } from './services/config';
import { DataDetailService } from './services/data-detail.service';
import { DataStoreService } from './services/data-store.service';
import { DataTypeService } from './services/data-type.service';
import { DepartmentService } from './services/department.service';
import { EmployeeService } from './services/empoyee.service';
import { InventoryService } from './services/inventory.service';
import { ItemService } from './services/item.service';
import { EngLanguages } from './services/languages/en';
import { Languages } from './services/languages/languages.service';
import { MasterDataService } from './services/master-data.service';
import { MasterPlanService } from './services/master-plan.service';
import { MenuService } from './services/menu.service';
import { ProductService } from './services/product.service';
import { RoutingService } from './services/routing.service';
import { ScheduleService } from './services/schedule.service';
import { SocketProvider } from './services/socket-io.service';
import { SwalService } from './services/swal.service';
import { TimeService } from './services/time.service';
import { UserPermissionsService } from './services/user-permissions.service';
import { WorkCenterService } from './services/work-center.service';
import { GoogleAPI } from './_libraries/google-api';
import { HttpClient } from './_libraries/http.client';
import { ValidationService } from './_libraries/validators';
import { DatepickercustomComponent } from './components/datepickercustom/datepickercustom.component';

export {
  BreadcrumbComponent,
  ContactFormComponent,
  ContactFormServices,
  ExportsComponent,
  HeaderComponent,
  ImportDirectiveComponent,
  MainBodyComponent,
  MainMenuComponent,
  PaginationControlsCmpComponent,
  PaginationService,
  PopupLoginComponent,
  ProgressBarComponent,
  MenuService,
  SwalService,
  TimeService,
  UserPermissionsService,
  BomService,
  DataDetailService,
  DataStoreService,
  DataTypeService,
  EmployeeService,
  ItemService,
  MasterDataService,
  MasterPlanService,
  RoutingService,
  SocketProvider,
  WorkCenterService,
  AutoCompleteDirectiveComponent,
  AutocompleteService,
  CustomInputComponent,
  Languages,
  EngLanguages,
  Functions,
  HttpClient,
  ValidationService,
  GoogleAPI,
  APIConfig,
  WMSPaginationComponent,
  GoBackComponent,
  ScheduleService,
  ProductService,
  AssigneesPopUpComponent,
  DepartmentService,
  InventoryService
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    UiSwitchModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
  ],
  declarations: [
    AutoCompleteDirectiveComponent,
    BreadcrumbComponent,
    ContactFormComponent,
    ExportsComponent,
    HeaderComponent,
    ImportDirectiveComponent,
    MainBodyComponent,
    MainMenuComponent,
    PaginationControlsCmpComponent,
    PopupLoginComponent,
    ProgressBarComponent,
    CustomInputComponent,
    WMSMessageComponent,
    WMSPaginationComponent,
    KeysPipe,
    OrderBy,
    WMSNumberFormatter,
    GoBackComponent,
    PaginatePipe,
    SearchPipe,
    UppercaseToSpaceLowerPipe,
    AutoCompleteDirectiveComponent,
    AssigneesPopUpComponent,
    DatepickercustomComponent
  ],
  exports: [
    AutoCompleteDirectiveComponent,
    BreadcrumbComponent,
    ContactFormComponent,
    ExportsComponent,
    HeaderComponent,
    ImportDirectiveComponent,
    MainBodyComponent,
    MainMenuComponent,
    PaginationControlsCmpComponent,
    PopupLoginComponent,
    ProgressBarComponent,
    CustomInputComponent,
    WMSMessageComponent,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    WMSPaginationComponent,
    UiSwitchModule,
    KeysPipe,
    OrderBy,
    WMSNumberFormatter,
    GoBackComponent,
    PaginatePipe,
    SearchPipe,
    UppercaseToSpaceLowerPipe,
    AutoCompleteDirectiveComponent,
    AssigneesPopUpComponent,
    DatepickercustomComponent
  ],
  providers: [
    AutocompleteService,
    ContactFormServices,
    PaginationService,
    BomService,
    DataDetailService,
    DataStoreService,
    DataTypeService,
    EmployeeService,
    ItemService,
    MasterPlanService,
    MenuService,
    MasterDataService,
    RoutingService,
    SocketProvider,
    SwalService,
    TimeService,
    UserPermissionsService,
    WorkCenterService,
    Languages,
    EngLanguages,
    ValidationService,
    Functions,
    GoogleAPI,
    APIConfig,
    ScheduleService,
    ProductService,
    DepartmentService,
    InventoryService,
    DatePipe
  ]
})
export class SharedModule {}
