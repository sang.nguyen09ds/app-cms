import {FormGroup} from '@angular/forms';
export class UsersFunctions {

        public matchedPassword(formGroup: FormGroup) {
                if(formGroup.controls['password'].value!==formGroup.controls['retype_password'].value){
                        if(formGroup.controls['retype_password'].value){
                                formGroup.controls['retype_password'].setErrors({"matchedPassword":true});
                        }
                }
        }

        public renderIOption(data:Array<any>=[],value:any,label:any){

                data.forEach((item)=>{
                        item['value']=item[value];
                        item['label']=item[label];
                });

                return data;

        }
        
        

}