import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'searchPipe',
})
export class SearchPipe implements PipeTransform {
    public transform(value: any, key: string, term: string) {
        if (!value || !term) {
            return value;
        }
        return value.filter(item => item.hasOwnProperty(key) && item[key].toLowerCase().indexOf(term.toLocaleLowerCase()) !== -1);
    }
}