import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'uppercaseToSpaceLower' })

export class UppercaseToSpaceLowerPipe implements PipeTransform {
    transform(str: string): string {
        return `${str.charAt(0).toUpperCase()}${str.replace(/([a-z])([A-Z])/g, '$1 $2').slice(1)}`;
    }
}