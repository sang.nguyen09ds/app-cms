import * as dayjs from 'dayjs';
import * as R from 'ramda';

export const isNilOrEmpty = R.anyPass([R.isNil, R.isEmpty]);

export const isNotNillAndNotEmpty = R.compose(
  R.not,
  isNilOrEmpty
);

export const trimValue = value => {
  return R.is(String, value) ? R.trim(value) : value;
};

export const convertDateTimeWithFormat = (date, formatString) =>
  date ? dayjs(date).format(formatString) : null;
