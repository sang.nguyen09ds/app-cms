// import mark = ts.performance.mark;
declare let window: any;
declare let MarkerClusterer: any;
export interface LatLng {
    lat: number;
    lng: number;
}
const APIKey = 'AIzaSyCrQnf8bh4gcmlbaMmraiS8WJsaL7hZCXE'; // Ecom-Order key
// const APIKey = 'AIzaSyAYjrKLpZ1jX3SDlhpMv9s0FgglApVT89w'; // key from HDS Email
// const APIKey = 'AIzaSyClIp6RejHfCxCT50q_sCuvJU_kSitdSTs'; // key from T4L

const url = 'https://maps.googleapis.com/maps/api/js?key=' + APIKey + '&libraries=geometry,places&callback=__onGoogleLoaded';

export class GoogleAPI {
    public autocomplete;
    private loadAPI: Promise<any>;
    private locations: LatLng[] = [];
    // private defaultLocation: LatLng = {lat: 37.09024, lng:-95.712891};
    private defaultLocation: LatLng = {lat: 39.7691164, lng: -75.7110948};
    private map = null;
    private gapiService = null;
    private orders = [];
    private listOrderCheched = [];

    constructor() {
        this.loadAPI = new Promise((resolve) => {
            window['__onGoogleLoaded'] = (ev) => {
                // console.log('gapi loaded');
                resolve(window['google']);
            };
            this.loadScript();
        });

    }

    /* For Suggestion: Start */
    public initAutoComplete(idElement) {
        const that = this;
        return this.loadAPI.then((gapi) => {
            // console.log('ele', (<HTMLInputElement> (document.getElementById(idElement))));
            that.autocomplete = new gapi.maps.places.Autocomplete(

                /** @type {!HTMLInputElement} */(<HTMLInputElement> (document.getElementById(idElement))),
                {types: ['geocode']});
            // console.log('place2', this.autocomplete);
            // autocomplete.addListener('place_changed', that.fillInAddress)
        });
    }

    /* For Drawing Map: Start */
    private loadScript() {
        // console.log('loading Map..');
        const node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);

    }

    private initGoogleAPI() {
        return this.loadAPI.then((gapi) => {
            this.gapiService = gapi;
            // console.log('init google api finish');
        });
    }

    private drawMap(orders = []) {
        if (orders) {
            return this.loadAPI.then((gapi) => {
                this.gapiService = gapi;
                // let location = this.defaultLocation;
                this.convertOrdLocationToGoogleLocation(orders);
                let location = this.defaultLocation;
                if (this.locations.length > 0) {
                    location = this.locations[0];
                    // console.log('location 2', location);

                } else {
                    // console.log('length', this.locations.length);
                }

                this.orders = orders;
                this.map = new gapi.maps
                    .Map((<HTMLInputElement> document.getElementById('map')), {
                    zoom: 4,
                    center: location,
                    type: 'ROADMAP'
                });

                // console.log('locations' , this.locations);
                const that = this;
                const markers = this.locations.map(function(loc, index) {
                    const orderInfo = 'Order ID ' + that.orders[index].id + ': ' + that.orders[index].delivery;
                    const marker = that.drawSingleMarker(loc, index, orderInfo);

                    const oldIcon = marker.getIcon();
                    const newIcon = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
                    const theRow = (<HTMLInputElement> document.getElementById('gg_' + index));
                    const rowBgColor = theRow.style.backgroundColor;
                    const handleMouseOver =  function () {
                        marker.setIcon(newIcon);
                        theRow.style.backgroundColor = '#6990fd';
                    };

                    const handleMouseOut = function () {
                       marker.setIcon(oldIcon);
                       theRow.style.backgroundColor = rowBgColor;
                    };

                    theRow.addEventListener('mouseover', handleMouseOver);
                    theRow.addEventListener('mouseout', handleMouseOut);

                    const theInput = (<HTMLInputElement> document.getElementById('ckbox_' + index));
                    theInput.onclick = function () {
                        // console.log('input', theInput.checked);
                        if (theInput.checked) {
                           theRow.removeEventListener('mouseover', handleMouseOver);
                           theRow.removeEventListener('mouseout', handleMouseOut);
                           // selectPopup[index] = true;
                        } else if (!theInput.checked) {
                           theRow.addEventListener('mouseover', handleMouseOver);
                           theRow.addEventListener('mouseout', handleMouseOut);
                           // selectPopup[index] = false;
                        }
                    };
                });
                if (markers.length > 0) {
                    const imgObj = {
                        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
                    };
                    const markerCluster = new MarkerClusterer(this.map, markers, imgObj);
                }
            });

        }
        return null;
    }

    private drawMap2(orders = []) {
        // let tempLocations = this.convertOrdLocationToGoogleLocation(orders);
        const that = this;
        this.map = new this.gapiService.maps
            .Map((<HTMLInputElement> document.getElementById('map')), {
            zoom: 4,
            center: that.defaultLocation,
            type: 'ROADMAP'
        });

    }

    private calcDistance(p1: LatLng, p2: LatLng) {
        return (this.gapiService.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
    }

    private convertOrdLocationToGoogleLocation (orders) {
        const that = this;
        const temLocations: LatLng[] = [];
        if (orders.length > 0) {
            that.locations = [];
            orders.forEach(function (ord) {
                const location = <LatLng> {};
                location.lat = parseFloat(ord.dlvy_lattd);
                location.lng = parseFloat(ord.dlvy_lngtd);
                that.locations.push(location);
                temLocations.push(location);
            });
        }
        return temLocations;
    }

    private drawSingleMarker(location: LatLng = null, index, title = '') {

        if (this.gapiService) {
            const marker = new this.gapiService.maps.Marker({
                position: location, // GPS coords
                map: this.map,
                // icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                optimized: false,
                title: title
                // label: 'shopping'
            });
            const oldIcon = marker.getIcon();
            const newIcon = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png';
            if (title !== '') {
                marker.setTitle(title);
            }
            const theRow = (<HTMLInputElement> document.getElementById('gg_' + index));
            const rowBgColor = theRow.style.backgroundColor;
            const theInput = (<HTMLInputElement> document.getElementById('ckbox_' + index));
            const that = this;
            marker.addListener('click', function () {

                if (marker.getIcon() === newIcon) {
                    marker.setIcon(oldIcon);
                    theRow.style.backgroundColor = rowBgColor;
                    theInput.checked = false;
                    const ind = that.listOrderCheched.indexOf(index);
                    let listOrd = that.listOrderCheched;
                    listOrd = that.listOrderCheched.splice(ind, 1);
                    // console.log('listOrd', listOrd);

                } else {
                    marker.setIcon(newIcon);
                    theRow.style.backgroundColor = '#6990fd';
                    theInput.checked = true;
                    const ind = that.listOrderCheched.indexOf(index);
                    const listOrd = that.listOrderCheched;
                    if (ind === -1) {
                        that.listOrderCheched.push(index);
                    }
                    // console.log('listOrd', listOrd);

                }

            });
            return marker;
        }
    }
    /* For Drawing Map: End */

}
