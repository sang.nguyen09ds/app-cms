import { AbstractControl, FormControl } from '@angular/forms';
import { compose, equals, map, propOr, reject, trim } from 'ramda';
export class ValidationService {
  public validatePasswordByPolicy(newpwd: any, oldpwd = '') {
    const errMessages = [];
    if (newpwd) {
      if (newpwd.length < 8) {
        errMessages.push('User password must have at least 8 characters');
      }
      const obj = {
        isNum: false,
        isLowerCase: false,
        isUpperCase: false,
        isSpecialChar: false
      };
      const specialChars = '<>@!#$%^&*()_+[]{}?:;|\'"\\,./~`-=';

      for (let i = 0; i < newpwd.length; i++) {
        const letter = newpwd[i];
        if (!isNaN(newpwd.charAt(i) * 1)) {
          obj['isNum'] = true;
        } else {
          if (letter === letter.toUpperCase()) {
            obj['isUpperCase'] = true;
          }
          if (letter === letter.toLowerCase()) {
            obj['isLowerCase'] = true;
          }
        }
      }

      if (!obj['isNum']) {
        errMessages.push('User password must contain at least one number');
      }
      if (!obj['isLowerCase']) {
        errMessages.push(
          'User password must contain at least one lowercase letter'
        );
      }
      if (!obj['isUpperCase']) {
        errMessages.push(
          'User password must contain at least one capital letter'
        );
      }

      for (let j = 0; j < specialChars.length; j++) {
        if (newpwd.indexOf(specialChars[j]) > -1) {
          obj['isSpecialChar'] = true;
        }
      }
      if (!obj['isSpecialChar']) {
        errMessages.push(
          'User password must contain at least one special letter'
        );
      }

      if (newpwd !== oldpwd) {
        errMessages.push('User confirm password does not match');
      }
    } else {
      errMessages.push('New password cannot be empty');
    }
    return errMessages;
  }

  public customRequired(control: FormControl): { [key: string]: any } {
    return control.value && control.value.replace(/ /g, '')
      ? null
      : { required: true };
  }

  public trimData(data: any) {
    const submitData = data;
    for (const key in submitData) {
      if (submitData.hasOwnProperty(key)) {
        const val = submitData[key];
        if (val && typeof val === 'string') {
          submitData[key] = val.trim();
        }
      }
    }
    return submitData;
  }

  public inPutOnlyNumber(control: FormControl): { [key: string]: any } {
    const re = /^([0-9,.]*)$/;

    if (control.value && !re.test(control.value)) {
      return { invalidNumber: true };
    } else {
      return null;
    }
  }

  public inPutOnlyInteger(control: FormControl): { [key: string]: any } {
    const re = /^([0-9]*)$/;

    if (control.value && !re.test(control.value)) {
      return { invalidInteger: true };
    } else {
      return null;
    }
  }

  public isZero(control: FormControl) {
    const value = control.value;
    if ((value === '' && value !== 0) || value == null) {
      return null;
    } else {
      return value <= 0 ? { isZero: true } : null;
    }
  }

  public isDecimalNumber(control: FormControl) {
    return control.value && !/^\d{1,3}(\.\d{1,7})?$/.test(control.value)
      ? { invalidDecimalNumber: true }
      : null;
  }

  public invalidNameStr(control: FormControl) {
    return control.value && !/^[a-zA-Z0-9 _-]*$/i.test(control.value)
      ? { invalidNameStr: true }
      : null;
  }

  public invalidCodeStr(control: FormControl) {
    return control.value && !/^[a-zA-Z0-9]*$/i.test(control.value)
      ? { invalidCodeStr: true }
      : null;
  }

  // Validate space
  public validateSpace(control: FormControl): { [key: string]: any } {
    const str = '' + control.value;
    if (control.value && !str.replace(/\s/g, '').length) {
      return { invalidSpace: true };
    }
    return null;
  }

  public invalidLength2Or3(control: FormControl) {
    return control.value && !/^[a-zA-Z0-9]{2,3}$/i.test(control.value)
      ? { invalidLength2Or3: true }
      : null;
  }

  public isNumber(evt, int = false) {
    const charCode = evt.which ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      if (int) {
        evt.preventDefault();
      } else {
        if (charCode !== 46) {
          evt.preventDefault();
        }
      }
    }
  }

  public validationUniqueItem(itemName) {
    return (c: AbstractControl): { [key: string]: any } => {
      const getValueOfItemAndTrim = compose(
        reject(equals('')),
        map(
          compose(
            trim,
            propOr('', itemName)
          )
        )
      )(c.value);

      const valueOfItemInSet = new Set(getValueOfItemAndTrim);

      const isValid = getValueOfItemAndTrim.length === valueOfItemInSet.size;
      return isValid ? null : { itemNotUnique: { valid: true } };
    };
  }
}
