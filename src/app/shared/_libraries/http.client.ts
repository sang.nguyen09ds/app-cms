import { HttpClient as httpC } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Router } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/throw';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiTypeEndPoint } from './enum';
declare const $: any;

@Injectable()
export class HttpClient {
  public apiCMS = environment.API.apiCMS;
  public apiBase = environment.API.apiBase;
  public apiMaster = environment.API.apiMaster;
  public apiCommon = environment.API.apiCommon;
  public apiAuthen = environment.API.apiAuthen;

  constructor(
    private http: httpC,
    private router: Router,
    private toastyService: ToastyService,
    private spinner: NgxSpinnerService
  ) {}

  public get(url: string, options: any = {}) {
    this.spinner.show();
    return this.http.get(url, options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  private getApiEndPoint(apiType) {
    switch (apiType) {
      case ApiTypeEndPoint.API_AUTHEN:
        return this.apiAuthen;
      case ApiTypeEndPoint.API_BASE:
        return this.apiBase;
      case ApiTypeEndPoint.API_CMS:
        return this.apiCMS;
      case ApiTypeEndPoint.API_COMMON:
        return this.apiCommon;
      default:
        return this.apiMaster;
    }
  }
  public setApiEndPoint(apiType: ApiTypeEndPoint) {
    const apiEndPoint = this.getApiEndPoint(apiType);
    return {
      get: (url: string, options: any = {}) =>
        this.get(`${apiEndPoint}/${url}`, options),
      post: (url: string, data?: any, options?: any) =>
        this.post(`${apiEndPoint}/${url}`, data, options),
      put: (url: string, data?: any, options?: any) =>
        this.put(`${apiEndPoint}/${url}`, data, options),
      delete: (url: string, options?: any) =>
        this.delete(`${apiEndPoint}/${url}`, options),
      get_file: (url: string, options?: any) =>
        this.get_file(`${apiEndPoint}/${url}`, options),
      post_import: (url: string, data?: any, options?: any) =>
        this.post_import(`${apiEndPoint}/${url}`, data, options)
    };
  }

  public post(url: string, data?: any, options?: any) {
    this.spinner.show();
    return this.http.post(url, JSON.stringify(data), options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  public post_import(url: string, data?: any, options?: any) {
    this.spinner.show();
    return this.http.post(url, data, options).pipe(
      map(this.extractDataImport),
      catchError(this.handleErrorBlob)
    );
  }
  public put(url: string, data?: any, options?: any) {
    this.spinner.show();
    return this.http.put(url, JSON.stringify(data), options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  public delete(url: string, options?: any) {
    this.spinner.show();
    return this.http.delete(url, options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  public patch(url: string, data?: any, options?: any) {
    this.spinner.show();
    return this.http.patch(url, data, options).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
  public get_file(url: string, options?: any) {
  
    return this.http.get(url, options).pipe(
        map(this.extractDataImport),
        catchError(this.handleError)
    );
}
  public extractData = (res: Response | any) => {
    this.spinner.hide();
    if (res.status) {
      if (res.status !== 200) {
        this.toastyService.success(res.status);
      }
    }
    return res;
  };

  public handleError = (err: Response | any) => {
    this.spinner.hide();
    if (!window.localStorage.getItem(environment.token) || err.status === 401) {
      this.router.navigate(['/login']);
    }

    let errMsg: any = null;
    const that = this;
    errMsg = this.parseError(err);
    errMsg.forEach(msg => {
      if (msg) {
        that.toastyService.error(msg);
      }
    });
    return Observable.throw(errMsg);
  };

  public handleErrorBlob = (err: Response | any) => {
    this.spinner.hide();
    if (err.blob && err.blob() instanceof Blob) {
      const reader = new FileReader();

      reader.onloadend = e => {
        const errObject = JSON.parse(reader.result);

        if (errObject.data !== undefined) {
          if (errObject.data['message'] !== undefined) {
            this.toastyService.error(errObject.data.message);
          }
        }
      };

      // reader.addEventListener('loadend', (e) => {
      //
      //     let errText = e.srcElement.result;
      //     let errObject = JSON.parse(errText);
      //     if (errObject.data != undefined) {
      //
      //         if (errObject.data['message'] != undefined) {
      //
      //             this.toastyService.error(errObject.data.message);
      //         }
      //     }
      // });
      reader.readAsText(err.blob());
    }
    return Observable.throw(err);
  };

  public extractDataImport = (res: Response | any) => {
    this.spinner.hide();
    return res;
  };

  public parseError(err: any) {
    const errMessage = [];
    try {
      let error = err;

      console.log('__parseError() http.client ', error);

      // fix Http
      if (err._body) {
        console.log('Res from Http');
        error = JSON.parse(err._body);
      } else {
        console.log('Res from HttpClient', error);
        // if (error.statusText == 'Unknown Error') {
        //   //errMessage.push('Có vấn đề, vui lòng báo với quản trị viên');
        // }
      }
      if (error.error) {
        errMessage.push(error.error.message);
      }

      if (!error.error && error.message) {
        errMessage.push(error.message);
      }

      if ((error.error && error.error.errors) || error.errors) {
        const errorsArrayObject = error.errors
          ? error.errors
          : error.error.errors;
        if (Object.keys(errorsArrayObject).length) {
          Object.keys(errorsArrayObject).forEach(_err => {
            console.log('err___ ', _err);
            if (errorsArrayObject[_err].length) {
              errMessage.push(errorsArrayObject[_err][0]);
            }
          });
        }
      }
    } catch (err) {
      console.log('parse err', err);
    }
    if (!errMessage) {
      errMessage.push('Vui lòng liên hệ quản trị viên');
    }
    return errMessage;
  }

  public activeLoading() {
    this.spinner.show();
  }

  public disableLoading() {
    this.spinner.hide();
  }

  handleErrorFront(err: Response | any) {
    this.disableLoading();
    let errMsg: any = null;
    const that = this;
    errMsg = this.parseError(err);
    errMsg.forEach(msg => {
      if (msg) {
        that.toastyService.error(msg);
      }
    });
    return Observable.throw(errMsg);
  }
}
