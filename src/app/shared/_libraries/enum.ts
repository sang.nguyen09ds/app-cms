export enum ApiTypeEndPoint {
  API_CMS = 0,
  API_BASE = 1,
  API_MASTER = 2,
  API_COMMON = 3,
  API_AUTHEN = 4
}
