import { Directive, forwardRef, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[modelvalidator][ngModel]',
    providers: [{
        multi: true,
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => ModelValidator)
    }]
})
export class ModelValidator implements Validator {
    @Input() modelvalidator: ValidatorFn;

    validate(control: AbstractControl): { [key: string]: any; } {
        return this.modelvalidator(control);
    }
}
