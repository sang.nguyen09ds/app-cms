import { Directive, ElementRef, Input, Output, EventEmitter, HostListener } from '@angular/core';
declare var $: any;
@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[infinite-scroll-autocomplete]'
})
export class InfiniteScrollAutocompleteDirective {

    private _paginData = {};
    @Input('paginData') set paginData(value: any) {
        if (value) {
            this._paginData = value['pagination'];
        }
    }
    @Input('currentPage') currentPage: any = 1;
    @Output() updated = new EventEmitter();

    constructor(private el: ElementRef) {
    }

    @HostListener('scroll', ['$event'])
    onElementScroll($event) {
        console.log( this.currentPage < this._paginData['total_pages'] , this._paginData['total_pages']);
        if ($event.target.scrollTop + $event.target.clientHeight >= $event.target.scrollHeight
            && this.currentPage < this._paginData['total_pages']) {
            this.updated.emit(this.currentPage + 1);
        }
    }
}
