import { ModuleWithProviders, NgModule } from '@angular/core';
import { MainMenuItemDirective } from '../components/main-menu/directive';
import { DisableControlDirective } from './disable-control/disable-control.directive';
import { InfiniteScrollAutocompleteDirective } from './infinite-scroll-autocomplete/infinite-scroll-autocomplete';
import { InputRefDirective } from './input/input-ref';
import { ModelValidator } from './validator/model-validator';
import { FileManagerComponent } from '../components/file-manager/file-manager.component';
import { CommonModule } from '@angular/common';
import { TabsModule } from 'ngx-bootstrap';
import { SharedModule } from '../shared.module';

@NgModule({
  imports: [
    CommonModule,
    TabsModule.forRoot(),
    SharedModule
  ],
  entryComponents: [FileManagerComponent],
  declarations: [
    DisableControlDirective,
    InfiniteScrollAutocompleteDirective,
    InputRefDirective,
    ModelValidator,
    MainMenuItemDirective,
    FileManagerComponent,
    
  ],
  exports: [
    DisableControlDirective,
    InfiniteScrollAutocompleteDirective,
    InputRefDirective,
    ModelValidator,
    MainMenuItemDirective,
    FileManagerComponent
  ]
})
export class DirectiveModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: DirectiveModule, providers: [] };
  }
}
