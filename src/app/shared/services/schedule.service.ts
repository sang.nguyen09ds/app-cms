import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { Schedule } from 'src/app/models/schedule.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class ScheduleService {
  private apiEndpointMaster;
  private endpointName = 'schedules';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getList(paramsSearch: ParamsGetSchedule): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  getListBarCode(scheduleId): Observable<any> {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${scheduleId}/barcodes`
    );
  }

  create(scheduleDetail: Schedule): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, scheduleDetail);
  }
  update(id: number, scheduleDetail: Schedule): Observable<any> {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${id}`,
      scheduleDetail
    );
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }

  genereBarcodeForSchedule(scheduleId, param): Observable<any> {
    return this.apiEndpointMaster.post(
      `${this.endpointName}/${scheduleId}/barcodes/generate`,
      param
    );
  }

  exportBarCode(scheduleId: any) {
    return this.apiEndpointMaster.get_file(
      `${this.endpointName}/${scheduleId}/barcodes/export`,
      { responseType: 'blob' }
    );
  }

  reGenereBarcodeForSchedule(scheduleId, param): Observable<any> {
    return this.apiEndpointMaster.post(
      `${this.endpointName}/${scheduleId}/barcodes/re-generate`,
      param
    );
  }
}

export interface ParamsGetSchedule {
  page?: number;
  limit?: number;
  search_schedule_no?: string;
  search_plan_no?: string;
  search_itm?: string;
  search_bom?: string;
}
