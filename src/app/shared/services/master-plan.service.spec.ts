import { TestBed, inject } from '@angular/core/testing';

import { MasterPlanService } from './master-plan.service';

describe('MasterPlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MasterPlanService]
    });
  });

  it('should be created', inject([MasterPlanService], (service: MasterPlanService) => {
    expect(service).toBeTruthy();
  }));
});
