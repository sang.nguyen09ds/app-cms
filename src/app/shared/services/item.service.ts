import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';

@Injectable()
export class ItemService {
  private apiEndpointMaster;
  private endpointName = 'work-centers/item-codes';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getListItem(params = { search_string: '' }): Observable<any> {
    return this.apiEndpointMaster.get(this.endpointName, { params });
  }
}
