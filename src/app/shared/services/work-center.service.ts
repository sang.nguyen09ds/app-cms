import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { WorkCenter } from 'src/app/models/work-center.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class WorkCenterService {
  private apiEndpointMaster;
  private endpointName = 'work-centers';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getListWorkCenter(paramsSearch: ParamsGetWorkCenter): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  create(dataDetail: WorkCenter): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, dataDetail);
  }
  update(id: number, dataDetail: WorkCenter): Observable<any> {
    return this.apiEndpointMaster.put(`${this.endpointName}/${id}`, dataDetail);
  }
  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }
}

export interface ParamsGetWorkCenter {
  page?: number;
  limit?: number;
  search_work_center?: string;
  search_itm?: string;
}
