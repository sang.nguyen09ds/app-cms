import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
/*
  Generated class for the SocketProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SocketProvider {

  socket: any;
  serve_url = 'http://150.95.111.8:3000';
  constructor() {
    console.log('Hello SocketProvider Provider');
  }

  connect() {

    const observable = new Observable(observer => {
        this.socket = io(this.serve_url, {
          'path': '/socket.io/'
        });
        this.socket.on('connect', function() {
          observer.next('socket connected');
        });

        console.log('this.socket ', this.socket);
    });
    return observable;

  }

  checkDisConnect() {

    const observable = new Observable(observer => {
      this.socket.on('disconnect', (data) => {
        observer.next(data);
      });
    });
    return observable;

   }

  listenMessage() {

    const observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        observer.next(data);
      });
    });
    return observable;

  }

  listenCultureMsg() {

    const observable = new Observable(observer => {
      this.socket.on('culture_msg', (data) => {
        console.log('socket listen culture_msg');
        observer.next(data);
      });
    });
    return observable;

  }

  listenSystemMsg() {

    const observable = new Observable(observer => {
      this.socket.on('system_msg', (data) => {
        console.log('socket listen system_msg ', data);
        observer.next(data);
      });
    });
    return observable;

  }

  emitSystemMessage(data: any) {
    console.log('emitSystemMessage ', this.socket);
    this.socket.emit('system_msg', data);
  }

}
