import { Injectable } from '@angular/core';

@Injectable()
export class DataStoreService {
  constructor() {}

  set(key: DataStoreEnum, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }
  get(key: DataStoreEnum) {
    return JSON.parse(localStorage.getItem(key));
  }
}

export enum DataStoreEnum {
  DATA_ROUTING = 'DATA_ROUTING',
  DATA_BOM = 'DATA_BOM',
  DATA_PRODUCT = 'DATA_PRODUCT',
  DATA_PLAN = 'DATA_PLAN',
  DATA_SCHEDULE = 'DATA_SCHEDULE'
}
