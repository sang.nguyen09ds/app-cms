import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { Routing } from 'src/app/models/routing.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class RoutingService {
  private apiEndpointMaster;
  private endpointName = 'routes';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getList(paramsSearch: ParamsGetRouting): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  public getListRoutingHeader(headerId): Observable<any> {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${headerId}/details`
    );
  }

  createRouteHeaderDetail(headerId, data): Observable<any> {
    return this.apiEndpointMaster.post(
      `${this.endpointName}/${headerId}/details`,
      {
        details: data
      }
    );
  }

  create(routingDetail: Routing): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, routingDetail);
  }
  update(id: number, routingDetail: Routing): Observable<any> {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${id}`,
      routingDetail
    );
  }

  deleteHeaderDetail(headerId: any, deatailId: any): Observable<any> {
    return this.apiEndpointMaster.delete(
      `${this.endpointName}/${headerId}/details/${deatailId}`
    );
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }
}

export interface ParamsGetRouting {
  page?: number;
  limit?: number;
  search_rout?: string;
  search_itm?: string;
}
