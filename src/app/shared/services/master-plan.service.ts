import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { MasterPlan } from 'src/app/models/master-plane.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class MasterPlanService {
  private apiEndpointMaster;
  private endpointName = 'plans';

  constructor(private _http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getList(paramsSearch: ParamsGetMasterPlan): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  create(masterPlan: MasterPlan): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, masterPlan);
  }

  updatePlan(id: number, masterPlan: MasterPlan): Observable<any> {
    return this.apiEndpointMaster.put(`${this.endpointName}/${id}`, masterPlan);
  }

  getEmployeeOfPlan(id: number) {
    return this.apiEndpointMaster.get(`${this.endpointName}/${id}/employees`);
  }

  getCostNotFromBom(planId: any): any {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${planId}/costs/not-from-bom`
    );
  }

  getCostLabor(planId: any): Observable<any> {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${planId}/costs/labor`
    );
  }

  getCostEnergy(planId: any): any {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${planId}/costs/energy`
    );
  }

  getListCostProduct(planId: any): any {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${planId}/costs/production`
    );
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }

  deleteCostEnergy(planId: any, plan_energy_id: any): Observable<any> {
    return this.apiEndpointMaster.delete(
      `${this.endpointName}/${planId}/cost/energy/${plan_energy_id}`
    );
  }
  deleteCostNotFormBom(planId: any, plan_not_bom_id: any): Observable<any> {
    return this.apiEndpointMaster.delete(
      `${this.endpointName}/${planId}/cost/not-from-bom/${plan_not_bom_id}`
    );
  }

  deleteCostProduct(planId: any, plan_pro_id: any): Observable<any> {
    return this.apiEndpointMaster.delete(
      `${this.endpointName}/${planId}/cost/production/${plan_pro_id}`
    );
  }

  createOrUpdateCostEnergy(planId: any, listCostEnergy: any): any {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${planId}/costs/energy`,
      listCostEnergy
    );
  }

  createOrUpdateCostLabor(planId: any, listCostLabor: any): any {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${planId}/costs/labor`,
      listCostLabor
    );
  }

  createOrUpdateCostNotBom(planId: any, listCostNotBom: any): any {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${planId}/costs/not-from-bom`,
      listCostNotBom
    );
  }

  createOrUpdateCostProduction(planId: any, listCostProduction: any): any {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${planId}/costs/production`,
      listCostProduction
    );
  }
}

export interface ParamsGetMasterPlan {
  page?: number;
  limit?: number;
  search_no?: string;
  search_bom?: string;
  search_itm?: string;
}
