import { Injectable } from '@angular/core';
@Injectable()
export class EngLanguages {
    public MSG =
    {
        'PERMISSION_DENY': 'Permission deny.',
        'SELECT_ITEM': 'Please choose one item.',
        'SELECT_ITEM_CONSTRAINT': 'Please choose only one item.',
        'RESET_PASS_SUCCESS': 'Reset password of user success.',
        'CHANGE_PASS_SUCCESS': 'Change your password success.',
        'CREATE_SUCCESS': 'Created successfully.',
        'UPDATE_SUCCESS': 'Updated successfully.',
        'DELETE_SUCCESS': 'Deleted successfully.',
        'VERSION_CHANGE': 'A new version has just been deployed. Do you want to re-load the page to get up-to-date?',

        'VR121': 'Please choose at least one Order to assign CSR',
        'There is no CSR in charge of the selected Customers': 'There is no CSR in charge of the selected Customers',
        'Please choose only one Order to allocate': 'Please choose only one Order to allocate',
        // for edit
        'ED1': 'BackOrder can not be edited!',
        'ED2': 'Online Order can not be edited!',

        // for assign CSR
        'CSR1': 'Canceled Order can not be assigned CSR!',
        'CSR2': 'Shipped Order can not be assigned CSR!',
        'CSR3': 'Please select and assign CSR to orders that belong to one customer only!',
        // for assign pallet
        'AP1': 'Please choose at least one Order to assign pallet!',
        'AP2': 'Assign pallet can be created for Packed or Partial Packed Orders only!',

        // for print LPN
        'PR1': 'Please choose at least one Staging or Partial Staging Order to print LPN',
        'PR2': 'Only Staging or Partial Staging Orders can print LPN!',

        // for print WAP LPN
        'PRW1': 'Please choose only one Order to print WAP LPNs',
        'PRW2': 'Cancelled Orders can not be printed WAP LPNs!',

        // for print Wave Pick
        'PWP1': 'Please choose one Order to print Wave Pick!',
        'PWP2': 'Please choose only one Order to print Wave Pick!',
        'PWP3': 'Wave Pick can be printed for Picking/Picked or Partial Picking/Picked Orders only!',

        // for create work order
        'WO1': 'Please choose at least one Order to create Work Order!',
        'WO2': 'Please choose only one Order to create Work Order!',
        'WO3': 'Work Order can\'t be created for Order that has status of New, Canceled, Hold, Shipped or Partial Shipped!',
        'WO4': 'Work Order {{work-order-number}} is created for the selected Order already!',

        // for create BOL
        'BOL1': 'Please choose at least one Order to create Bill of Ladings!',
        'BOL2': 'Please choose only one Order to create Bill of Ladings!',
        'BOL3': 'Only Staging or Partial Staging Order can have Bill of Ladings!',
        'BOL4': 'Bill of Ladings has been created for this Order already!',

        // for cancel order
        'CC1': 'Please choose at least one Order to cancel!',
        'CC2': 'On Hold, Canceled or Shipped Orders are not processed!',
        'CC3': 'Please choose only one Order to cancel!',

        // for shipping
        'SP1': 'Please choose at least one Order to ship!',
        'SP2': 'Only Staging or Partial Staging Orders can be shipped!',
        'SP3': 'BOL has not been finalized. Order cannot be shipped.',
        'SP4': 'Please choose only one Order to ship!',

        // for update shipped date
        'USD1': 'Update Shipped Date Successfully!',
        'VR100' : `Sorry, there's a problem with the connection.`,
        'VR127': 'Please choose at least one Order to create Wave Pick',
        'VR128': 'Wave Pick can be created for Allocated or Partial Allocated Orders only!',

        'VR130': 'Order Pick can be executed for New Wave Pick only!',
        'VR131': 'Order Pick for Wave Pick {0} is executed successfully!',
        'VR132': 'Please choose one Order to pack',
        'VR133': 'Please choose only one Order to pack',
        'VR134': 'Only Picked or Packing Orders can be packed!',
        'VR135': 'Please choose items to pack!',
        'VR136': 'At least one item should have Pack Qty!',
        'VR137': 'Carton packed successfully!',
        'VR138': 'Order can not be allocated!',
        'VR140': 'Please choose one Online Order to print Packing Slip!',
        'VR141': 'Only New can be updated!',

        'IP01': 'Import file has some errors.',
        'IP02': 'Some items cannot be allocated. Please check the downloaded file.',

        'ODE01': 'Are you sure you want to delete?',
        'ODE02': 'Input data will be reset if you make this change. Are you sure you want to change this?',

        'ECM01': 'Created successfully.',
        'ECM02': 'Updated successfully.',
        'ECM03': 'Deleted successfully.',

        'VR122': 'Please choose one Order to allocate',
        'VR123': 'Please choose only one Order to allocate',
        'VR124': 'Only New Orders can be allocated!',
        'VR125': 'Order has not been assigned CSR. Do you want to assign CSR to this Order?',
        'ALC006': 'Available quantity is less than requested quantity. Partial allocate is not supported for this order!',
        // User Management
        'VR142': 'Please select one item to remove.',
        'VR143': 'Do you want to remove role/roles?',
    };
}
