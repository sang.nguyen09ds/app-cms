import { Injectable } from '@angular/core';
import { EngLanguages } from './en';
@Injectable()
export class Languages {
  constructor(private lang: EngLanguages) {}
  public msg(txt: string) {
    return this.lang.MSG[txt] ? this.lang.MSG[txt] : txt;
  }
}
