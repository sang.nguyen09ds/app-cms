import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { HttpClient } from '../_libraries/http.client';
import { APIConfig } from './config';
import { LOGGED } from 'src/app/reducers';

@Injectable()
export class UserPermissionsService {
  currentUser: any;
  loginURL;
  constructor(
    private http: HttpClient,
    private store: Store<User>,
    private router: Router,
    private api: APIConfig
  ) {
    this.loginURL = this.api.LOGIN;
  }

  public getUserPermissions(
    options: permissionOptions = { viewPage: '', deny: false, permissions: [] }
  ) {
    return new Observable((observer) => {

      if(!this.currentUser) {
          this.http.get(this.http.apiMaster + '/users/info?permissions=1').subscribe(
              data=> {

                  this.currentUser = data['data'];
                  // only for store
                  this.store.dispatch({payload: data['data'], type: LOGGED});
                  // processPermission
                  if(this.processPermission(options)){
                      observer.next(data['data']);
                  }else{
                      observer.error("The user don't have permission "+ options.viewPage);
                  }

                  observer.complete();

              },
              err=>{
                  observer.error("The user don't have permission "+ options.viewPage);
                  observer.complete();
              }
          );
      }else{

          if(this.processPermission(options)){
              observer.next(this.currentUser);
              observer.complete();
          }

      }

  });



}

public allowAcessPage(options:permissionOptions){

  if(options.viewPage && this.checkViewPagePermission(this.currentUser['permissions'],options)){
       return true; // allowed
  }

  this.deny();
  return false;

}

public checkPermission(permissions: any, permission: any) {
  if (permissions && permissions.length) {
      for (const per of permissions) {
          if (per.name === permission) {
              return true;
          }
      }
  }
  return false;
}

/*=================================================
  For now , this function just check and return true if the user
  have at least one of those permissions that user have.
  @ param : [ redirecToDenyPage ,  ]
*=================================================*/
public processPermission( options: permissionOptions ) {

  if(!options) return;
  const userPermission = this.currentUser['permissions'];

  // 1. check viewPage permission
  if(this.checkViewPagePermission(userPermission, options)) {
      return true;
  }

  // check permission required for that page
  if(options.permissions && options.permissions.length){
      for(const per of options.permissions) {
          if(userPermission[per]) {
              console.log('Found one permission valid for this page.');
              return true;
          }
      }
  }

  // don't have any permisson => redirect to deny page.
  if(options.deny) {
      this.deny();
  }

  // don't have any permisson
  return false;
}

public checkExistAnyPermission(permissions: Array<any> = []) {
  const userPermission = this.currentUser['permissions'];
  for(const permission of permissions) {
      if(userPermission[permission]) {
          return true;
      }
  }
  return false;
}

private checkViewPagePermission(permissionArray = {} , options:permissionOptions){
  if(options.viewPage && !permissionArray[options.viewPage]) {
      if(options.deny){
          this.deny();
      }
      return false;
  }

  return true;
}

private deny(){
  this.router.navigate(['deny']);
}
login(userInfo) {
  return this.http.post(this.loginURL, userInfo);
}
}

export interface permissionOptions {
viewPage:any;
deny?:any;
permissions?:any;
}