import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { Bom } from 'src/app/models/bom.models';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class BomService {
  private apiEndpointMaster;
  private endpointName = 'boms';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  getListBomHeaderDetail(bomId: any): Observable<any> {
    return this.apiEndpointMaster.get(`${this.endpointName}/${bomId}/details`);
  }

  public getList(paramsSearch: ParamsGetBom): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  getBomDetail(bomId: any): Observable<any> {
    return this.apiEndpointMaster.get(`${this.endpointName}/${bomId}/details`);
  }

  create(bomDetail: Bom): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, bomDetail);
  }

  createBomHeaderDetail(headerId: any, data: any): Observable<any> {
    return this.apiEndpointMaster.post(
      `${this.endpointName}/${headerId}/details`,
      {
        details: data
      }
    );
  }

  deleteHeaderDetail(headerId, detailId): Observable<any> {
    return this.apiEndpointMaster.delete(
      `${this.endpointName}/${headerId}/details/${detailId}`
    );
  }

  deleteHeaderDetailsWithWorkCenter(headerId: any, work_center_id: any): any {
    return this.apiEndpointMaster.delete(
      `${this.endpointName}/${headerId}/work-centers/${work_center_id}`
    );
  }

  update(id: number, bomDetail: Bom): Observable<any> {
    return this.apiEndpointMaster.put(`${this.endpointName}/${id}`, bomDetail);
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }
}

export interface ParamsGetBom {
  page?: number;
  limit?: number;
  search_bom?: string;
  search_itm?: string;
}
