import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';

@Injectable()
export class MasterDataService {
  private apiEndpointMaster;
  private endpointName = 'master-data';

  constructor(private _http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getListSupplier(): Observable<any> {
    return this.apiEndpointMaster.get('boms/suppliers');
  }
  public getListManufacture(): Observable<any> {
    return this.apiEndpointMaster.get(this.endpointName, {
      params: {
        md_type: 'MAN',
        limit: 500,
        md_sts: 1
      }
    });
  }
}
