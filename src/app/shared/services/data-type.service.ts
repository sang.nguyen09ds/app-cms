import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { DataType } from 'src/app/models/data-type.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';
@Injectable()
export class DataTypeService {
  private apiEndpointMaster;
  private endpointName = 'master-data-types';
  constructor(private _http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getDataTypes(paramsSearch: ParamsGetDataTypes): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  public getAllDataTypes(param): Observable<any> {
    return this.apiEndpointMaster.get(this.endpointName + param);
  }

  create(dataType: DataType): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, dataType);
  }
  update(id: number, dataType: DataType): Observable<any> {
    console.log(dataType);

    return this.apiEndpointMaster.put(`${this.endpointName}/${id}`, dataType);
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }
}

export interface ParamsGetDataTypes {
  page?: number;
  limit?: number;
  md_code?: string;
  md_name?: string;
}
