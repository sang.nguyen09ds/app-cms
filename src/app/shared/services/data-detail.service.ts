import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { DataDetail } from 'src/app/models/data-detail.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';
@Injectable()
export class DataDetailService {
  private apiEndpointMaster;
  private endpointName = 'master-data';

  constructor(private _http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getlistDataDetail(paramsSearch: ParamsGetDataDetail): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  create(dataDetail: DataDetail): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, dataDetail);
  }
  update(id, dataDetail: DataDetail): Observable<any> {
    return this.apiEndpointMaster.put(`${this.endpointName}/${id}`, dataDetail);
  }
  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }
}

export interface ParamsGetDataDetail {
  page?: number;
  limit?: number;
  md_code?: string;
  md_name?: string;
  md_type?: string;
}
