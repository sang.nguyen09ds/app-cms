import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class DepartmentService {
  private apiEndpointMaster;
  private endpointName = 'departments';
  constructor(private _http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getList(paramsSearch: ParamsGetDepartment): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  public getEmployeeOfDepartment(departmentId) {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${departmentId}/employees`
    );
  }
}

export interface ParamsGetDepartment {
  page?: number;
  limit?: number;
  search_department?: string;
}
