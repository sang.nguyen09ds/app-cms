import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product.model';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class ProductService {
  private apiEndpointMaster;
  private endpointName = 'pro-orders';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getList(paramsSearch: ParamsGetProduct): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  create(productDetail: Product): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, productDetail);
  }
  update(id: number, productDetail: Product): Observable<any> {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${id}`,
      productDetail
    );
  }

  getBomDetailsOfProduct(productId: any): Observable<any> {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${productId}/bom-details`
    );
  }

  getDetailsOfProduct(productId: any): Observable<any> {
    return this.apiEndpointMaster.get(
      `${this.endpointName}/${productId}/details`
    );
  }

  saveProductDetail(productId, listDetailProduct: any): Observable<any> {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${productId}/details`,
      listDetailProduct
    );
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }
}

export interface ParamsGetProduct {
  page?: number;
  limit?: number;
  search_product_no?: string;
  search_pr_no?: string;
  search_itm?: string;
  search_bom?: string;
}
