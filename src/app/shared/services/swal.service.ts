import { Injectable } from '@angular/core';
// import swal from 'sweetalert';
import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;

@Injectable()
export class SwalService {
  private defaultOpts = {
    title: '',
    icon: '',
    text: 'Are you sure you want to cancel?',
    buttons: [true]
  };

  constructor() {}

  public cancel(okCallback, newOpts = {}) {
    console.log(swal);
    const opts = Object.assign({}, this.defaultOpts, newOpts);
    return swal(opts).then((ok: boolean) => {
      if (ok) {
        okCallback();
      }
    });
  }
  public info(text: any, callBack) {
    const defaultOpts = {
      text: text
    };

    const opts = Object.assign({}, defaultOpts);
    return swal(opts).then(() => {
      callBack();
    });
  }

  public confirm(text: any, okCallback) {
    const defaultOpts = {
      title: '',
      icon: '',
      text: text ? text : 'Are you sure?',
      buttons: ['No', 'Yes']
    };

    const opts = Object.assign({}, defaultOpts);
    return swal(opts).then((ok: boolean) => {
      if (ok) {
        okCallback();
      }
    });
  }
}
