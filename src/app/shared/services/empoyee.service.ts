import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';
@Injectable()
export class EmployeeService {
  private apiEndpointMaster;
  private endpointName = 'employees';

  constructor(private _http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getListEmployee(paramsSearch: ParamsGetEmployee): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }
}

export interface ParamsGetEmployee {
  page?: number;
  limit?: number;
  search_name?: string;
}
