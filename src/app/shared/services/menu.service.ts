import { Injectable } from '@angular/core';
import { HttpClient } from '../_libraries/http.client';

@Injectable()
export class MenuService {
    constructor(
        private http: HttpClient
    ) {}

    public getGroup() {
        return this.http.get(this.http.apiMaster + '/menus');
    }

    public addGroup(data: any) {
        return this.http.post(this.http.apiMaster + '/menus', data);
    }

    public editGroup(id: number, data: any) {
        return this.http.post(this.http.apiMaster + '/menus' + id, data);
    }

    public getMenusByGroupId(id: number) {
        return this.http.get(this.http.apiMaster + '/menus/' + id);
    }

    public updateMenusByGroupId(id: number, data: any) {
        return this.http.post(this.http.apiMaster + '/menus/' + id, data);
    }

    public getLeftMenu() {
        return this.http.get(this.http.apiMaster + '/menu/left-menu');
    }

    public getFavoriteMenu() {
        return this.http.get(this.http.apiMaster + '/menu/favourite-menu');
    }

    public addFavorite(id: number, position: number) {
        const data = {
            menu_id: id,
            display_order: position
        };
        return this.http.post(this.http.apiMaster + '/menu/favourite-menu', data);
    }

    public removeItemFavorite(id: number) {
        return this.http.delete(this.http.apiMaster + '/menu/favourite-menu/' + id);
    }

}
