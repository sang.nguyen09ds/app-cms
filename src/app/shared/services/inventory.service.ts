import { Injectable } from '@angular/core';
import { compose, map, pickBy } from 'ramda';
import { Observable } from 'rxjs';
import { Inventory } from 'src/app/models/inventory.models';
import { ApiTypeEndPoint } from '../_libraries/enum';
import { HttpClient } from '../_libraries/http.client';
import { isNotNillAndNotEmpty, trimValue } from '../_libraries/utils';

@Injectable()
export class InventoryService {
  private apiEndpointMaster;
  private endpointName = 'inventories';

  constructor(_http: HttpClient) {
    this.apiEndpointMaster = _http.setApiEndPoint(ApiTypeEndPoint.API_MASTER);
  }

  public getList(paramsSearch: ParamsGetInventory): Observable<any> {
    const funcGetValueValid = pickBy(isNotNillAndNotEmpty);
    const paramsClean = compose(
      map(trimValue),
      funcGetValueValid
    )(paramsSearch);

    return this.apiEndpointMaster.get(this.endpointName, {
      params: paramsClean
    });
  }

  create(inventoryDetail: Inventory): Observable<any> {
    return this.apiEndpointMaster.post(this.endpointName, inventoryDetail);
  }

  update(id: number, inventoryDetail: Inventory): Observable<any> {
    return this.apiEndpointMaster.put(
      `${this.endpointName}/${id}`,
      inventoryDetail
    );
  }

  delete(id): Observable<any> {
    return this.apiEndpointMaster.delete(`${this.endpointName}/${id}`);
  }

  importIventory(fileToUpload: File): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('inventories_xlsx_file', fileToUpload, fileToUpload.name);
    return this.apiEndpointMaster.post_import(
      `${this.endpointName}/import`,
      formData,
      {
        responseType: 'blob'
      }
    );
  }
  exportIventory() {
    return this.apiEndpointMaster.get_file(`${this.endpointName}/export`, {
      responseType: 'blob'
    });
  }
}

export interface ParamsGetInventory {
  page?: number;
  limit?: number;
  search_itm?: string;
}
