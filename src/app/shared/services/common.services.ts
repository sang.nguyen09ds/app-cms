import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient } from '../shared.module';
@Injectable()
export class CommonService {
    constructor(private http: HttpClient) {
    }
    public create(endPoint:any,data: any) {
        return this.http.post(this.http.apiMaster + endPoint, data);
    }

    public update(endPoint:any, id: any, data: any) {
        return this.http.put(this.http.apiMaster + endPoint + id, data);
    }

    public get(params: any) {
        return this.http.get(this.http.apiMaster + '/files/upload' ,{params});
    }

    public getExternal(endPoint: any) {
        return this.http.get(endPoint);
    }
  
    public delete(endPoint:any, id: any) {
        return this.http.delete(this.http.apiMaster + endPoint + id);
    }

}