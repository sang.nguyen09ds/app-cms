import { Component, ContentChild, Input, OnInit } from '@angular/core';
import { InputRefDirective } from '../../_directives/input/input-ref';

@Component({
  selector: 'custom-input',
  templateUrl: './custom-input.component.html'
})
export class CustomInputComponent implements OnInit {
  @Input() label: string;
  @Input() validations: { [index: string]: string };
  @Input() info: string;
  @Input() isInTable: boolean = false;
  @Input() disabled: boolean = false;

  @ContentChild(InputRefDirective) input: InputRefDirective;

  get isError() {
    return this.input.hasError;
  }

  get errorMessages() {
    const errors = this.input.errors;
    const messages = [];
    if(this.validations)
    {
      const keys = Object.keys(this.validations);
      keys.forEach(key => {
        if (errors[key]) {
          messages.push(this.validations[key]);
        }
      });
    }
    return messages;
  }

  ngOnInit() {}

  constructor() {}
}
