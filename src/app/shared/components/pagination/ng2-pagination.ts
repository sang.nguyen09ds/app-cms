export { PaginatePipe } from './paginate-pipe';
export { PaginationService, IPaginationInstance } from './pagination-service';
export { PaginationControlsCmpComponent } from './pagination-controls-cmp';
