import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'wms-pagination',
  templateUrl: 'wms-pagination.html'
})
export class WMSPaginationComponent {
  @Input() public pagingData: any;
  @Input() public perPage: number;

  @Output() public pageChange = new EventEmitter();

  constructor() {}

  setCurrentPage(pageNum: number) {
    this.pageChange.emit(pageNum);
  }
}
