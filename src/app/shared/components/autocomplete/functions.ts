import {
    FormGroupName,FormGroup,
    FormControl,AbstractControl ,
    NG_VALIDATORS,Validator} from '@angular/forms';
export class AutoCompleteFunctionHelper{


    getValueByKey(formControl:AbstractControl,key:any,label:any,items:Array<any>=[]){

        if(formControl){

            for (let i in items){
                if(items[i][key]==formControl.value){
                    return items[i][label];
                }
            }

        }


    }

    public trim(str:string=''){
        const cvtoString = str + '';
        if (typeof cvtoString !== 'string') {
            throw new Error('only string parameter supported!');
        }
        return cvtoString.replace(/ /g, '');
    }


}