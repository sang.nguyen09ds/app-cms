import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { debounceTime } from 'rxjs/operators';
import { HttpClient } from '../../_libraries/http.client';
@Injectable()
export class AutocompleteService {
  constructor(private http: HttpClient) {}
  public backendRequest(param$) {
    return new Observable(observer => {
      this.http
        .get(param$)
        .pipe(debounceTime(1000))
        .subscribe(
          data => {
            observer.next(data);
            observer.complete();
          },
          err => {
            observer.next(false);
            observer.complete();
          }
        );
    });
  }
}
