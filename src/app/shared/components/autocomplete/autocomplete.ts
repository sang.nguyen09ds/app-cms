/* tien.nguyen@seldatinc.com */
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Host,
  Input,
  OnDestroy,
  Optional,
  Output,
  SkipSelf
} from '@angular/core';
import {
  AbstractControl,
  ControlContainer,
  ControlValueAccessor,
  FormControl,
  FormGroup,
  FormGroupName,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import 'rxjs/add/observable/fromEvent';
import { debounceTime, map } from 'rxjs/operators';
import { AutocompleteService } from './autocomplete.service';
import { AutoCompleteFunctionHelper } from './functions';

@Component({
  selector: 'autocomplete-directive',
  templateUrl: './autocomplete.html',
  styleUrls: ['style.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AutoCompleteDirectiveComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => AutoCompleteDirectiveComponent),
      multi: true
    },
    AutoCompleteFunctionHelper,
    AutocompleteService
  ]
})
export class AutoCompleteDirectiveComponent
  implements ControlValueAccessor, OnDestroy, Validator {
  @Output() selected = new EventEmitter();
  @Output() focus = new EventEmitter();
  @Output() type = new EventEmitter();
  @Input('label') label: any;
  @Input('key') key: any;
  @Input('placeholder') placeholder = 'Autocomplete...';
  onChange: any = () => {};
  onTouched: any = () => {};
  @Input('formControlKey') formControlKey: string;
  formControl: AbstractControl;
  @Input('isdisabled') isdisabled = false;
  @Input('ignorekeyExistCheck') ignorekeyExistCheck = false;
  @Input('enableLoading') enableLoading = false;
  @Input('custom_display_label') custom_display_label: Array<any> = [];
  @Input('headers') headers: Array<any> = [];
  @Input('divider_open') divider_open: string;
  @Input('divider_close') divider_close: string;
  @Input('api_url') api_url: string;
  @Input('position') position: string;
  @Input('') filteredItems: Array<any> = [];
  debounceTime: number = 1000;
  private subscribe: Subscription;
  public changed: any;
  private _onChangeValidator: () => void;

  public _value = '';
  errorState = false;

  @Input('items') set items(val: any) {
    if (Array.isArray(val)) {
      this.filteredItems = val;
    }
  }

  @Input('value') set value(val: any) {
    try {
      this._value = val;
      this.onChange(val);
      this.ref.detectChanges();
      if (this._onChangeValidator) this._onChangeValidator();
    } catch (err) {
      // console.log(err);
    }
  }

  get value() {
    return this._value;
  }

  public action: any;

  constructor(
    private ref: ChangeDetectorRef,
    private elm: ElementRef,
    private auservice: AutoCompleteFunctionHelper,
    private activatedRoute: ActivatedRoute,
    @Optional() @Host() @SkipSelf() private parentForm: ControlContainer,
    private service: AutocompleteService
  ) {
    this.activatedRoute.data.subscribe(params => {
      this.action = params['action'];
    });
  }

  ngOnInit(): void {
    const formControlName = this.elm.nativeElement.getAttribute(
      'formControlName'
    );
    if (this.parentForm && formControlName) {
      if (this.parentForm['form']) {
        this.formControl = (<FormGroup>this.parentForm['form']).get(
          formControlName
        );
      } else if (this.parentForm instanceof FormGroupName) {
        this.formControl = (<FormGroupName>this.parentForm).control.controls[
          formControlName
        ];
      }
      if (this.formControl instanceof FormControl) {
        (this.formControl as FormControl).registerOnDisabledChange(
          isDisabled => (this.isdisabled = isDisabled)
        );
      }
    }

    this.subscribe = Observable.fromEvent(this.elm.nativeElement, 'keyup')
      .pipe(map(i => i))
      .pipe(debounceTime(this.debounceTime))
      .subscribe(this.changeValue.bind(this));
  }

  ngOnDestroy(): void {
    this.subscribe.unsubscribe();
  }

  public changeValue($event: any) {
    let keyCode = $event.code;
    console.log('change autocomplete key api_url', this.api_url);
    if (
      keyCode != 'ArrowUp' &&
      keyCode != 'ArrowDown' &&
      keyCode != 'ArrowLeft' &&
      keyCode != 'ArrowRight' &&
      keyCode != 'MetaLeft'
    ) {
      this.changed = true;
      if (this._onChangeValidator) this._onChangeValidator();

      this.type.emit($event.target.value);

      if (this.api_url) {
        const param$ = this.api_url + encodeURIComponent($event.target.value);
        this.sendRequestToSever($event.target.value, param$);
      }
    }
  }

  private sendRequestToSever(value: any, $event) {
    this.service.backendRequest($event).subscribe(data => {
      this.filteredItems = data['data'];
    });
  }

  /*==================================
   * Check Key exist
   *================================= */
  checkKeyExist() {
    setTimeout(() => {
      for (let item of this.filteredItems) {
        if (
          item[this.key] &&
          this.auservice.trim(item[this.key].toUpperCase()) ==
            this.auservice.trim('' + this.value.toUpperCase())
        ) {
          this.selectedItem(item);
          return true;
        }
      }
      if (this.value) {
        this.formControl.setErrors({ invalid: true });
      }
      return false;
    });
  }

  public _blurCheckKeyExist = true;
  blurCheckKeyExist() {
    // TMS2-979
    if (this.changed) {
      console.log('check key blurCheckKeyExist');
      setTimeout(() => {
        if (this._blurCheckKeyExist) {
          this.checkKeyExist();
        }
      }, 200);
    }
  }

  resetError() {
    this._blurCheckKeyExist = true;
    if (this.formControl && this.formControl.errors) {
      delete this.formControl.errors['invalid'];
      if (!Object.keys(this.formControl.errors).length) {
        this.formControl.setErrors(null);
      }
    }
  }

  selectedItem(item: any) {
    this.value = item[this.key];
    this._blurCheckKeyExist = false;
    this.selected.emit(item);
    if (this._onChangeValidator) this._onChangeValidator();

    if (this.formControl && this.key) {
      this.formControl.setValue(item[this.key]);
    }
    this.ref.detectChanges();
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  writeValue(value: any) {
    this.value = value;
    if (!value) {
      this.filteredItems = [];
    }
    this.onChange;
  }

  // not used, used for touch input
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  // the method set in registerOnChange to emit changes back to the form
  private propagateChange = (_: any) => {};

  public isArray(value: any) {
    return typeof value == 'object' ? true : false;
  }

  registerOnValidatorChange?(fn: () => void): void {
    this._onChangeValidator = fn;
  }

  validate(control: AbstractControl) {
    const { value } = control;
    const isExits =
      this.filteredItems.length > 0
        ? this.filteredItems.some(
            obj => this.key in obj && obj[this.key] == value
          )
        : true;

    return isExits
      ? null
      : {
          required: {
            valid: false
          }
        };
  }
}
