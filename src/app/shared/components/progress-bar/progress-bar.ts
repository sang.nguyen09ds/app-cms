import { Component, Directive, Output, ElementRef, EventEmitter, HostListener, Input } from '@angular/core';
@Component ({
    selector: 'wms2-progress-bar',
    providers: [],
    templateUrl: 'progress-bar.html'
})
export class ProgressBarComponent {
    public _progressValue = 0;
    public _progressText = '';
    @Input('progressText') set progressText(value: any) {
        this._progressText = value;
    }
    @Input('progressValue') set progress(value: any) {
        this._progressValue = value;
    }
    constructor() {}
}
