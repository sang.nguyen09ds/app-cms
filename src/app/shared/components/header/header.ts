import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '../../_libraries/http.client';

@Component({
  selector: 'header',
  templateUrl: 'header.html'
})
export class HeaderComponent implements OnInit {
  public profile: any = {};
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

  ngOnInit() {
    this.profile = this.jwtHelper.decodeToken(
      localStorage.getItem(environment.token)
    );
  }

  signOut() {
    this.http.get(this.http.apiAuthen + '/logout').subscribe(
      () => {
        console.log('You have been successfully logged out!');
      },
      error => {}
    );
  }
}
