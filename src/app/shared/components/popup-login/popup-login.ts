import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';
import { UserPermissionsService } from '../../services/user-permissions.service';
import { HttpClient } from '../../_libraries/http.client';

@Component({
  selector: 'popup-login',
  templateUrl: 'popup-login.html'
})
export class PopupLoginComponent implements OnInit, AfterViewInit {
  public modalLogin: any;
  public bodyElm: any;
  public overlayLogin: any;
  public showPopupLogin = false;
  public hasModalOpen = false;
  public form: FormGroup;
  public loading: Boolean = false;
  public submitted: Boolean = false;

  constructor(
    private httpClient: HttpClient,
    private toastyService: ToastyService,
    private userService: UserPermissionsService
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  ngAfterViewInit() {
    // this.modalLogin = jQuery('#modal-login').css('z-index', 1070);
    // this.bodyElm = jQuery('body');
    // if (jQuery('#overlay-login').length) {
    //   this.overlayLogin = jQuery('#overlay-login');
    // } else {
    //   const elmLogin =
    //     '<div id="overlay-login" class="modal-backdrop fade" style="z-index: 1060;display:none;"></div>';
    //   this.overlayLogin = jQuery(elmLogin).appendTo(this.bodyElm);
    // }
    // this.bodyElm.on('showLoginPopup', () => {
    //   this.showModalLogin();
    // });
  }

  public showModalLogin() {
    if (!this.showPopupLogin) {
      this.showPopupLogin = true;
      this.hasModalOpen = this.bodyElm.hasClass('modal-open') ? true : false;
      this.bodyElm.addClass('modal-open').css('padding-right', 15);
      this.modalLogin.addClass('in').show();
      this.overlayLogin.addClass('in').show();
    }
  }

  public hideModalLogin() {
    this.modalLogin.removeClass('in').hide();
    this.overlayLogin.removeClass('in').hide();
    this.showPopupLogin = false;
  }

  private buildForm() {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  public onSubmit() {
    this.submitted = true;
    if (this.form.valid) {
      this.loading = true;
      this.userService.login(this.form.value).subscribe(
        res => {
          this.loading = false;
          if (res.data.token) {
            localStorage.setItem(environment.token, res.data.token);
          }
          this.hideModalLogin();
        },
        err => {
          this.loading = false;
          const errMsg = this.httpClient.parseError(err);
          errMsg.forEach(v => {
            this.toastyService.error(v);
          });
        }
      );
    }
  }
}
