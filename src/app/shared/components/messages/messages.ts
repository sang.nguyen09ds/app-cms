import { Component, Input } from '@angular/core';
@Component({
  selector: 'wms-messages',
  templateUrl: 'template.html'
})
export class WMSMessageComponent {
  @Input('messages') messages: any;
}
