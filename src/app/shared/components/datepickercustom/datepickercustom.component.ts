import { DatePipe } from '@angular/common';
import { Component, forwardRef, Injector, OnInit } from '@angular/core';
import { ControlValueAccessor, NgControl, NG_VALUE_ACCESSOR } from '@angular/forms';
declare var require : any
var moment = require('moment');
@Component({
  selector: 'app-datepickercustom',
  templateUrl: './datepickercustom.component.html',
  styleUrls: ['./datepickercustom.component.css'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => DatepickercustomComponent),
    }
  ],
})
export class DatepickercustomComponent implements ControlValueAccessor, OnInit {

  private onChange: any = () => { }
  private onTouched: any = () => { }
  value: string = '';
  // private ngControl: NgControl;

  constructor(
    private datePipe: DatePipe,
    // private injector: Injector,
  ) { }

  ngOnInit() {
    // this.ngControl = this.injector.get(NgControl);
    this.value= moment().format('DD-MM-YYYY hh:mm');
  }

  public bsValueChange(value) {
    this.writeValue(this.value);
    this.onTouched();
  }

  public writeValue(value: any) {
    this.value = value ? this.transformDate(value) : '';
    this.onChange(this.value);
    console.log('writeValue', 'old: ' + value, 'new: ' + this.value);
  }

  public registerOnChange(fn: any) {
    this.onChange = fn;
    this.onChange(this.value); // for OnInit cycle
  }

  public registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean) {
    //
  }

  private transformDate(value: any) {
    console.log('transformDate:', value);
    return moment(value,'DD-MM-YYYY hh:mm').format('DD-MM-YYYY hh:mm');
    
  } 

}
