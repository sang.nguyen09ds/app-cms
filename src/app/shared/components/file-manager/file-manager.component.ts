import { Component, ViewChild, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import 'rxjs/add/operator/map';
import { Functions, HttpClient } from '../../shared.module';
import { CommonService } from '../../services/common.services';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
declare var $: any;
@Component({
  selector: 'file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss'],
  providers: [CommonService]
})
export class FileManagerComponent implements OnInit {
  
  files:Array<any> = [];
  html:any;
  folders:Array<any> = [];
  fileToUpload: File = null;
  public perPage = 20;
  public pagination: any;
  selectedImage:any;
  @ViewChild('filemediamodal') filemediamodal:ElementRef;
  @Output() selected =  new EventEmitter();
  dataValueChange: Subject<Object>;
  constructor(
    private _func: Functions,
    private httpClient:HttpClient,
    private toastyService: ToastyService,
    private bsModalRef: BsModalRef,
    private commonService: CommonService) { }

  ngOnInit() {
    this.getFiles();
    this.dataValueChange = new Subject();
    //this.openFileManager();
    //this.getFolders();
  }

  getFiles(params = { page: 1 }){
    this.commonService.get({...params, limit: this.perPage }).subscribe((res)=>{
      this.files = res.data;
      this._func.initPagination(res['meta']);
    },err=>{

    })
  }

  getFolders(){
    this.commonService.get('/folders?limit=30').subscribe((res)=>{
      this.folders = res.data;
    },err=>{

    })
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    console.log('fileToUpload', this.fileToUpload);
  }

  uploadFileToActivity() {
    console.log('this.fileToUpload ', this.fileToUpload);
    this.uploadFile(this.fileToUpload).subscribe(data => {
      // do something, if upload success
      this.toastyService.success('Upload file thành công');
      }, error => {
        console.log(error);
      });
  }

  uploadFile(fileToUpload: File) {
      const options = {};
      options['headers'] = this._func.HeaderPostFile();
      const formData: FormData = new FormData();
      formData.append('file', fileToUpload, fileToUpload.name);
      console.log('options', options);
      return this.httpClient.post(this.httpClient.apiCommon + '/files/upload',formData,options);
  }

  _selectedImage(file:any){
    this.selectedImage = file;
  }

  finishSeleced(){
   this.selected.emit(this.selectedImage);
  }

  copyImageUrl(val: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastyService.success('Copy hoàn tất');
  }
  close() {
    this.bsModalRef.hide();
  }
  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }
  public pageChange($event) {
    this.getFiles({page: $event });
  }
}
