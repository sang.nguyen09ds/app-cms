import { Component,Input, OnInit } from '@angular/core';
import * as FileSaver from 'file-saver';
import { ToastyService } from 'ng2-toasty/index';
import { Functions } from '../../functions';
import { HttpClient } from '../../_libraries/http.client';
@Component({
  selector: 'directive-export',
  templateUrl: 'template.html'
})
export class ExportsComponent implements OnInit {
  @Input() printerAPIUrl = '';
  @Input() enableCsv = true;
  @Input() enablePdf = true;
  @Input() fileName = '';
  

  constructor(
    private toastyService: ToastyService,
    private funcs: Functions,
    private http: HttpClient
  ) {}

  ngOnInit() {}

  exportFile(type = '') {

    const that = this;
    try {
        that.downloadFileService(this.printerAPIUrl).subscribe((response: any) => {

            let blob:any;
            if(type == 'pdf'){
                blob = new Blob([response], {type: "application/pdf"});
            }
            if (type == 'csv'){
                blob = new Blob([response], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"});
            }

            //const blob = response.blob();
            FileSaver.saveAs(blob, this.fileName);

        
        }, (err: any) => {
        });


    } catch (err) {
        that.toastyService.warning('Không thể in được dữ liệu. vui lòng thử lại');
    }
}

private downloadFileService(url: any) {
    const options = {};
    options['headers'] = this.funcs.AuthHeader();
    options['responseType'] = 'blob';
    return this.http.get_file(url, options);
}
}
