import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { Languages } from '../../services/languages/languages.service';
import { GoogleAPI } from '../../_libraries/google-api';
import { ValidationService } from '../../_libraries/validators';
import { ContactFormServices } from './contact-form.service';

// import { type } from 'os';
declare var $: any;

@Component({
  selector: 'contact-form',
  templateUrl: 'contact-form.html'
})
export class ContactFormComponent {
  public Loading = [];
  public title;
  public _action: string;
  private _cusId: string;
  public ContactForm: FormGroup;
  private submitted = false;
  private listType = [];
  private Country = [];
  private StateProvinces = [];
  public showForm = true;
  private autocomplete;

  @Output() message = new EventEmitter();

  @Input('cusId') set cusId(value: string) {
    this._cusId = value;
  }

  @Input('openForm') set openForm(value: any) {
    if (value) {
      if (this.ContactForm) {
        this.resetForm();
      } else {
        this.buildContactForm();
      }

      // if (!this.autocomplete) {
      //     this.addAutoComplete();
      // }

      this.getListType();
      this.GetCountry();
    }
  }

  @Input('action') set action(action: any) {
    this._action = action;
    switch (action) {
      case 'add':
        this.title = 'Add Third Party';
        break;
      case 'view':
        this.title = 'View Third Party';
        break;
      case 'edit':
        this.title = 'Edit Third Party';
        break;
      default:
        break;
    }
  }

  constructor(
    private valid: ValidationService,
    private serivce: ContactFormServices,
    private messages: Languages,
    private toastyService: ToastyService,
    private googleAPI: GoogleAPI,
    private fb: FormBuilder
  ) {}

  private getListType() {
    this.listType = [
      { code: 'CUS', name: 'End Customer' },
      { code: 'SHP', name: 'Shipper' }
    ];
  }

  private GetCountry() {
    const param = '?sort[sys_country_name]=asc&limit=500';
    this.serivce.getCountry(param).subscribe(res => {
      this.Country = res.data;
    });
  }

  private GetState(idCountry, idState = '') {
    const param = '?sort[sys_state_name]=asc&limit=500';
    this.serivce.State(idCountry, param).subscribe(
      res => {
        this.StateProvinces = res.data;
      },
      err => {
        console.log(err);
      },
      () => {
        this.ContactForm.patchValue({ state: idState });
      }
    );
  }

  private ChangeCountry(idCountry) {
    if (idCountry) {
      this.GetState(idCountry);
    } else {
      this.StateProvinces = [];
    }
    this.ContactForm.patchValue({ state: '' });
  }

  private resetForm(item = {}) {
    this.showForm = false;
    setTimeout(() => {
      this.showForm = true;
      this.buildContactForm(item);
      this.submitted = false;
    });
  }

  private buildContactForm(item = {}) {
    this.ContactForm = this.fb.group({
      type: [item['type'] || 'CUS', Validators.required],
      name: [
        item['name'] || '',
        [
          Validators.required,
          this.valid.validateSpace,
          this.valid.invalidNameStr
        ]
      ],
      phone: item['phone'] || '',
      mobile: item['mobile'] || '',
      add_1: [
        item['add_1'] || '',
        [Validators.required, this.valid.validateSpace]
      ],
      add_2: item['add_2'] || '',
      country: [item['country'] || '', [Validators.required]],
      state: [item['state'] || '', [Validators.required]],
      city: [
        item['city'] || '',
        [Validators.required, this.valid.validateSpace]
      ],
      zip: [item['zip'] || '', [Validators.required, this.valid.validateSpace]],
      ct_first_name: [item['ct_first_name'] || '', [this.valid.invalidNameStr]],
      ct_last_name: [item['ct_last_name'] || '', [this.valid.invalidNameStr]],
      des: item['des'] || ''
    });

    $('#ContactForm').modal('show');
    setTimeout(() => {
      this.addAutoComplete();
    }, 300);
  }

  private SaveThirdParty() {
    this.submitted = true;
    if (this.ContactForm.valid) {
      const data = this.ContactForm.value;
      for (const x in data) {
        if (data.hasOwnProperty(x) && typeof data[x] === 'string') {
          data[x] = data[x] ? data[x].trim() : data[x];
        }
      }

      const dataJson = JSON.stringify(data);
      if (this._action === 'edit') {
        this.updateThirdParty(dataJson);
      } else {
        this.createThirdParty(dataJson);
      }
    }
  }

  private createThirdParty(data) {
    this.serivce.createThirdParty(this._cusId, data).subscribe(
      res => {
        this.message.emit({
          type: 'success',
          message: this.messages.msg('VR107')
        });
        $('#ContactForm').modal('hide');
      },
      err => {
        // this.message.emit({type: 'error', message: err});
      }
    );
  }

  private updateThirdParty(data = {}) {
    this.serivce.updateThirdParty(this._cusId, data['tp_id'], data).subscribe(
      res => {
        this.message.emit({
          type: 'success',
          message: this.messages.msg('VR109')
        });
        $('#add-thirdParty').modal('hide');
      },
      err => {
        // this.message.emit({type: 'error', message: err});
      }
    );
  }

  private isNumber(evt, int: boolean = false) {
    this.valid.isNumber(evt, int);
  }

  private addAutoComplete() {
    const that = this;
    this.googleAPI.initAutoComplete('autocomplete').then(() => {
      that.autocomplete = that.googleAPI.autocomplete;
      that.autocomplete.addListener('place_changed', function() {
        const place = that.autocomplete.getPlace();
        let addr = '';
        let state = '';
        let zipCode = '';
        let city = '';
        let country = '';
        // var length = place.address_components.length;
        console.log('place', place);
        if (place.address_components) {
          for (let i = 0; i < place.address_components.length; i++) {
            const addressType = place.address_components[i].types[0];
            if (addressType === 'street_number') {
              // number address
              addr += place.address_components[i].short_name;
            }
            if (addressType === 'route') {
              // street
              addr += ' ' + place.address_components[i].short_name;
            }
            if (addressType === 'locality') {
              // city
              city = place.address_components[i].short_name;
            }
            if (addressType === 'administrative_area_level_1') {
              // state
              state = place.address_components[i].short_name;
            }
            if (addressType === 'postal_code') {
              // zip code
              zipCode = place.address_components[i].short_name;
            }
            if (addressType === 'country') {
              // country
              country = place.address_components[i].short_name;
            }
          }

          // let location = place.geometry.location;
          const paramObj = {
            add_1: addr,
            city: city,
            zip: zipCode,
            country: country,
            state: state
          };
          that._autoCompleteUpdateForm(that.ContactForm, paramObj);
        }
      });
    });
  }

  private _autoCompleteUpdateForm(form, paramObj) {
    form.patchValue(paramObj);
    this.GetState(paramObj.country, paramObj.state);
  }
}
