import { Injectable } from '@angular/core';
import { Functions } from '../../functions';
import { HttpClient } from '../../_libraries/http.client';

@Injectable()
export class ContactFormServices {
  private API_SHIPPING = this.http.apiMaster + '/core/shipping/v1';
  private API_COUNTRY =
    this.http.apiCommon + '/core/common-service/v1/countries';
  private HEADER_JSON: any;

  constructor(private http: HttpClient, private func: Functions) {
    this.HEADER_JSON = this.func.AuthHeaderPostJson();
  }

  public getThirdParty(cusId, param = '') {
    return this.http.get(
      this.API_SHIPPING + '/cus/' + cusId + '/third-party' + param
    );
  }

  public createThirdParty(cusId, data) {
    return this.http.post(
      `${this.API_SHIPPING}/cus/${cusId}/third-party`,
      data,
      { headers: this.HEADER_JSON }
    );
  }

  public updateThirdParty(cusId, id, data) {
    return this.http.put(
      this.API_SHIPPING + '/cus/' + cusId + '/third-party/' + id,
      data
    );
  }

  public getCountry(param) {
    return this.http.get(this.API_COUNTRY + param);
  }

  public State(idCountry, param) {
    return this.http.get(
      this.API_COUNTRY + '/' + idCountry + '/states' + param
    );
  }
}
