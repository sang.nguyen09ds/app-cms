import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Http, Response, ResponseContentType } from '@angular/http';
import { ToastyService } from 'ng2-toasty';
import 'rxjs/add/operator/map';
import { Functions } from '../../functions';
import { HttpClient } from '../../_libraries/http.client';
declare const $: any;
declare var saveAs: any;
@Component({
  templateUrl: 'import.html',
  selector: 'import-directive',
  providers: []
})
export class ImportDirectiveComponent {
  @ViewChild('itemFileView') itemFileView: any;
  @Output() success = new EventEmitter<any>();
  itemFile: any;
  Form: FormGroup;
  @Input() popupTile = 'Import Dữ liệu (.xlsx|.csv|.xls)';
  validFile: any;
  @Input() importApiUrl: any;
  @Input() import_sample_file_url =
    '/assets/templates/ImportProduct.xlsx?v=1.1';

  constructor(
    private _fb: FormBuilder,
    private _func: Functions,
    private http: Http,
    private httpClient: HttpClient,
    private toastyService: ToastyService
  ) {}

  open() {
    $('#import-modal').modal('show');
    this.itemFileView.nativeElement.value = '';
    this.validFile = false;
  }

  file: any;
  fileChange(event: any) {
    this.validFile = false;
    const filename = event.target.files.length
      ? event.target.files[0].name
      : '';
    const validFormats = ['xlsx', 'csv', 'xls'];
    const ext = filename.substr(filename.lastIndexOf('.') + 1);

    if (ext == '') return;
    if (validFormats.indexOf(ext) == -1) {
      this.toastyService.warning('Chỉ hỗ trợ định dạng .xlsx, .xls and .csv');
      return false;
    }
    this.itemFile = <Array<File>>event.target.files[0];
    this.validFile = true;
  }

  import() {
    const that = this;
    try {
      const formData: any = new FormData();
      const filename = this.itemFile.name;
      const ext = filename.substr(filename.lastIndexOf('.') + 1);
      formData.append('file', that.itemFile, that.itemFile.name);
      formData.append('file_type', ext);

      const xhr = new XMLHttpRequest();

      xhr.open('POST', this.importApiUrl, true);
      xhr.setRequestHeader('Authorization', 'Bearer ' + that._func.getToken());
      xhr.responseType = 'blob';

      xhr.onreadystatechange = function() {
        if (xhr.readyState === 2) {
          if (xhr.status === 200) {
            xhr.responseType = 'blob';
          } else {
            xhr.responseType = 'text';
          }
        }

        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            that.toastyService.warning('Import có lỗi');
            const blob = new Blob([this.response], {
              type:
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            });
            saveAs.saveAs(blob, 'Import_Fail.xlsx');
          } else if (xhr.status === 201) {
            const resp = JSON.parse(xhr.response);
            that.toastyService.success(resp.message);
            that.success.emit(true);
            setTimeout(function() {
              $('#import-modal').modal('hide');
            }, 500);
          } else {
            if (xhr.response) {
              const resp = JSON.parse(xhr.response);
              that.toastyService.warning(resp.message);
            } else {
              that.toastyService.warning('Import có lỗi');
            }
          }
        }
      };

      xhr.send(formData);
    } catch (err) {
      that.toastyService.warning('Import có lỗi');
    }

    // this.uploadFile(formData).subscribe((response:any)=>{
    //
    //     console.log(response);
    //
    //     if(response.status==200){
    //
    //         that.toastyService.warning(that.message.msg('Import file has some errors.'));
    //         let blob = response.blob();
    //         saveAs.saveAs(blob, 'Import_KPI_Master_Fail.xlsx');
    //
    //
    //     }else if(response.status==201){
    //
    //         console.log('blob ', response.blob());
    //         that.toastyService.success(that.message.msg(response.message));
    //         this.success.emit(true);
    //     }
    //
    // },(err:any)=> {
    //
    //     console.log('err ', err);
    //     this.httpClient.handleError(err);
    // });
  }

  uploadFile(formData: any) {
    const options = {};
    options['headers'] = this._func.AuthHeader();
    options['responseType'] = ResponseContentType.Blob;
    return this.http
      .post(this.importApiUrl, formData, options)
      .map((res: Response) => res);
  }
}
