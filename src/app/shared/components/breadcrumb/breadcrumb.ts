import { Component, Output, Input, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, UrlSegment, NavigationEnd } from '@angular/router';

@Component({
    selector: 'breadcrumb',
    templateUrl: 'breadcrumb.html'
})
export class BreadcrumbComponent implements OnInit {
    public breadcrumbs: Array <{name: string, url: string}> = [];

    constructor(
        private router: Router
    ) {}

    ngOnInit() {
        // first case reload page
        this.parseRoute(this.router.routerState.snapshot.root);
        // rest cases subscribe's called when navigate to other route
        this.router.events.filter((event) => event instanceof NavigationEnd).subscribe(() => {
            this.breadcrumbs = [];
            this.parseRoute(this.router.routerState.snapshot.root);
        });
    }

    public parseRoute(node: ActivatedRouteSnapshot) {
        if (node.data['name']) {
            let urlSegments: UrlSegment[] = [];
            node.pathFromRoot.forEach((routerState) => {
                urlSegments = urlSegments.concat(routerState.url);
            });
            const url = urlSegments.map((urlSegment) => {
                return urlSegment.path;
            }).join('/');
            this.breadcrumbs.push({
                name: node.data['name'],
                url: '/' + url
            });
        }
        if (node.firstChild) {
            this.parseRoute(node.firstChild);
        }
    }
}
