import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MenuService } from '../../services/menu.service';
declare var $:any;
@Component({
  selector: 'main-menu',
  templateUrl: 'main-menu.html',
  providers: [MenuService]
})
export class MainMenuComponent {
  public isOpen: any;
  public menuData: Array<any> = [];
  public openingMenu = {};
  @ViewChild('sidebarElement') sidebarElement:ElementRef;
  constructor(
    private menuService: MenuService,
    private activatedRoute: Router
  ) {
    
    this.activatedRoute.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.resetActiveMenu();
      }
    });
  }
  ngAfterViewInit(){
    this.initMenuStyleJs();
}
  private loadLeftMenu() {
    this.menuService.getLeftMenu().subscribe(res => {
      this.menuData = res.data.map(menu => {
        if (menu.nodes && menu.nodes.length > 0) {
          menu['has_sub_menu'] = true;
        }

        for (const subMenu of menu.nodes) {
          if (location.hash.indexOf(subMenu.url) > -1) {
            this.openingMenu = menu['menu_id'];
          }
        }
        return menu;
      });
    });
  }

  public clickOnParentMenu(menu: any = {}) {
    for (const menuItem of this.menuData) {
      if (menuItem['menu_id'] === menu['menu_id']) {
        continue;
      }
      menuItem['is_open'] = false;
    }

    this.openingMenu = menu['menu_id'];

    if (!menu['has_sub_menu']) {
      this.isOpen = false;
      return;
    }

    menu['is_open'] = !menu['is_open'];
  }

  public resetActiveMenu() {
    for (const menuItem of this.menuData) {
      menuItem['is_open'] = false;
    }
  }

  public clickSubMenu(menu: any) {
    setTimeout(() => {
      menu['has_sub_menu'] = true;
      menu['is_open'] = true;
      this.isOpen = false;
    });
  }
  screenHeight=0;
  initMenuStyleJs(){

    this.screenHeight = window.outerHeight;
    $(this.sidebarElement.nativeElement).css('height',this.screenHeight);

    $('.main-menu li a').click(function(){
        $(this).siblings('ul').slideToggle();
    });

}
  
}
