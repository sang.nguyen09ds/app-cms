import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssigneesPopUpComponent } from './assignees-pop-up.component';

describe('AssigneesPopUpComponent', () => {
  let component: AssigneesPopUpComponent;
  let fixture: ComponentFixture<AssigneesPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssigneesPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssigneesPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
