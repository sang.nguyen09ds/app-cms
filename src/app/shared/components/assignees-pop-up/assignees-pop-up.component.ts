import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import {
  EmployeeService,
  ParamsGetEmployee
} from 'src/app/shared/services/empoyee.service';
import { Functions } from '../../functions';
@Component({
  selector: 'app-assignees-pop-up',
  templateUrl: './assignees-pop-up.component.html',
  styleUrls: ['./assignees-pop-up.component.scss']
})
export class AssigneesPopUpComponent implements OnInit {
  public perPage = 20;
  public pagination: any;
  public searchForm: FormGroup;
  public formGroupCheckBox: FormArray;
  public listEmployee = [];
  public arrayRemove = [];
  assigness = new Map();
  changeAssigness: Subject<Object>;
  isView = false;
  isOpenAssigness = false;
  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private bsModalRef: BsModalRef,
    private func: Functions
  ) {}

  ngOnInit() {
    if (!this.isOpenAssigness) {
      this.buildSearchForm();
      this.getListEmployee();
    }
    this.changeAssigness = new Subject();
  }

  getListEmployee(params: ParamsGetEmployee = { page: 1 }) {
    this.employeeService
      .getListEmployee({ ...params, limit: this.perPage })
      .subscribe(response => {
        this.listEmployee = response.data;
        // this.buildGroupCheckBoxEmployee(this.listEmployee);
        this.initPagination(response);
      });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }

  buildSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      search_employee: ['']
    });
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getListEmployee({ ...valuesSearch, page: pageNumber });
  }

  public search() {
    const valuesSearch = this.searchForm.value;
    this.getListEmployee(valuesSearch);
  }

  close() {
    this.bsModalRef.hide();
  }

  changeChecked($event) {
    const id = $event.target.id;

    if ($event.target.checked) {
      this.assigness.set(+id, { emp_id: id, name: $event.target.value });
    } else {
      this.arrayRemove.push(id);
    }
  }

  changeCheckedRemove($event) {
    const id = $event.target.id;
    console.log(id);
    if ($event.target.checked) {
      this.arrayRemove.push(id);
    } else {
      this.assigness.set(+id, { emp_id: id, name: $event.target.value });
    }
  }

  add() {
    this.changeAssigness.next(this.assigness);
    this.close();
  }

  remove() {
    console.log(this.arrayRemove);
    console.log(this.assigness);
    for (const id of this.arrayRemove) {
      console.log(id);
      this.assigness.delete(+id);
      console.log(this.assigness);
    }
    this.changeAssigness.next(this.assigness);
  }
}
