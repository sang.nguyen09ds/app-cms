import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ToastyConfig } from 'ng2-toasty';
import 'rxjs/add/operator/filter';

@Component({
    selector: 'main-body',
    templateUrl: 'main-body.html',
    providers: [Title]
})

export class MainBodyComponent implements OnInit {
    public title: string;
    public currentTitle: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private titleService: Title,
        private toastyConfig: ToastyConfig
    ) {
        this.toastyConfig.position = 'top-right';
    }

    ngOnInit() {
        this.currentTitle = this.titleService.getTitle();
        // first case reload page
        let currentRoute = this.route.root; // route is an instance of ActiveRoute
        let data;
        while (currentRoute.children[0] !== undefined) {
            currentRoute = currentRoute.children[0];
        }
        data = currentRoute.snapshot.data;
        this.title = data['name'];
        // rest cases subscribe's called when navigate to other route
        this.router.events.filter((event) => event instanceof NavigationEnd).subscribe(() => {
            this.parseRoute(this.router.routerState.snapshot.root);
        });
    }

    private parseRoute(node: ActivatedRouteSnapshot) {
        if (node.data['name']) {
            this.title = node.data['name'];
            // this.titleService.setTitle(this.currentTitle + ' - ' + node.data['name'].toUpperCase());
            this.titleService.setTitle(this.title);
        }
        if (node.firstChild) {
            this.parseRoute(node.firstChild);
        }
    }
}
