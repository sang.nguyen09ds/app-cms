import { Location } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'goback',
  template: `
    <a (click)="goBack()" class="btn btn-outline red no-margin-left pull-left"
      ><i class="icon-action-undo"></i>Back</a
    >
  `
})
export class GoBackComponent {
  constructor(private _location: Location) {}

  public goBack() {
    this._location.back();
  }
}
