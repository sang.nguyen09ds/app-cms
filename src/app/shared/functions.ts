import { FormGroup, FormControl } from '@angular/forms';
export class Functions {
    public pagination: any;
    public processPaging(data: any = {}) {
        const pageCount = data['total_pages'];
        const currentPage = data['current_page'];
        const numLinks = 3;
        let start = 1;
        let end = 1;
        let tmpPageCount = [];

        if (currentPage > numLinks) {
            start = currentPage - numLinks;
        } else {
            start = 1;
        }
        if (currentPage + numLinks < pageCount) {
            end = currentPage + numLinks;
        } else {
            end = pageCount;
        }

        // Create tmp for loop perPage in view
        for (let i = start; i <= end; i++) {
            tmpPageCount[i] = i;
        }

        tmpPageCount = tmpPageCount.filter(function (item) {
            return item !== undefined;
        });

        return tmpPageCount;
    }

    public whs_id() {
        return JSON.parse(localStorage.getItem('whs_id'));
    }

    public getIds(array: Array<any> = [], key: any, selected: boolean = false) {

        const ids: Array<any> = [];
        array.forEach((item) => {
            if (selected) {
                if (item['selected']) {
                    ids.push(item[key]);
                }
            } else {
                ids.push(item[key]);
            }
        });
        return ids;

    }

    /*================================
     Get Token
     =================================*/
    public getToken() {
        return localStorage.getItem('id_token');
    }
    /*================================
    User
    ================================*/
    currentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }
    /*=================================
     AuthHeader
     ==================================*/
    public AuthHeader() {
        const authHeader = new Headers();
        const id_token = this.getToken();
        authHeader.append('Authorization', 'Bearer ' + id_token);
        console.log('AuthHeader ', authHeader, id_token);
        return authHeader;
    }
    public HeaderPostFile() {
        const authHeader = new Headers();
        const id_token = this.getToken();
        authHeader.append('Accept', 'application/json');
        authHeader.append('Authorization', 'Bearer ' + id_token);
        authHeader.append('Content-Type', 'multipart/form-data');
        return authHeader;
    }

    public AuthHeaderNoTK() {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return headers;
    }

    public AuthHeaderPostJson() {
        const authHeader = new Headers();
        authHeader.append('Content-Type', 'application/json');
        authHeader.append('Authorization', 'Bearer ' + this.getToken());
        return authHeader;
    }
    public swalDeleteOption(){
        return {
          title: 'Are you sure you want to delete?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          buttonsStyling: false
        }
    }
    public trim(str: string) {
        const cvtoString = str + '';
        if (typeof cvtoString !== 'string') {
            throw new Error('only string parameter supported!');
        }
        return cvtoString.replace(/ /g, '');
    }

    public getSortParam(sortData: any) {

        if (sortData.length) {
            sortData = sortData[0];
            return '&sort[' + sortData.colId + ']=' + sortData.sort;
        }
        return '';

    }
    public renderDataSelect(data:Array<any>=[],value:any,label:any){

        data.forEach((item)=>{
          item['text']=item[label] ? item[label].toString() : '';
        });
    
        return data;
    
      }
    public Messages(type: String, text: String) {
        const mes = {
            status: type,
            txt: text
        };
        return mes;
    }

    // Error handle
    public parseErrorMessageFromServer(err) {
        let errMessage = '';
        try {
            if (typeof err === 'object' && err._body) {
                err = JSON.parse(err._body);
            } else {
                err = err.json();
            }
            if (err.error) {
                if (typeof err.error === 'object') {
                    for (const key in err.error) {
                        if (err.error.hasOwnProperty(key)) {
                            const msgErr = typeof err.error[key] === 'string' ? err.error[key] : err.error[key][0];
                            errMessage += '<p>' + msgErr + '</p>';
                        }
                    }
                }
                if (typeof err.error === 'string') {
                    errMessage = err.error;
                }
            } else if (err.message || err.detail) {
                errMessage = err.message || err.detail;
            } else if (err.errors) {
                err = err.errors;
                errMessage = err.message || err.detail;
            }
        } catch (err) {
            // console.log('parse err', err);
        }
        if (!errMessage) {
            errMessage = 'Sorry, there\'s a problem with the connection.';
        }
        return errMessage;
    }

    genSearchCriteriaFromForm(searchForm: FormGroup) {
        const keys = Object.keys(searchForm.controls);
        const sObj = {};
        if (Object.keys(sObj).length === 0) {
            // console.log('null');
        }
        keys.forEach(function (value) {
            sObj[value] = searchForm.controls[value].value;
        });
        // console.log('s', sObj);
        return sObj;
    }

    public initPagination(data) {
        const { meta } = data;
        if (meta && meta['pagination']) {
          this.pagination = meta['pagination'];
        } else {
          const { perPage, currentPage, totalCount } = meta;
          const count =
            perPage * currentPage > totalCount
              ? perPage * currentPage - totalCount
              : perPage;
          this.pagination = {
            total: totalCount,
            count: count,
            per_page: perPage,
            current_page: currentPage,
            total_pages: meta['pageCount'],
            links: {
              next: data['link']['next']
            }
          };
        }
        this.pagination['numLinks'] = 3;
        this.pagination['tmpPageCount'] = this.processPaging(this.pagination);
      }

    /*=============================================
     * Example :
     * Checkall :
     * <input [(ngModel)]="dataOrigin['checkAll']" (ngModelChange)="checkedAll(dataOrigin,$event)"  type="checkbox"/>
     * Checked Item
     * <input [(ngModel)]="item.selected" (ngModelChange)="checkedItem(cartonDataListMaster,item,$event)"  type="checkbox">
     * =========================================*/

    public selectedAll(originData = {}, label = 'data') {

        const array = originData[label];
        if (originData[label].length) {
            array.forEach((item) => {
                item['selected'] = originData['checkAll'];
            });
        }

    }

    selectedItem(originData = {}, label = 'data') {

        let checked = 0;
        const array = originData[label];
        array.forEach((item) => {
            if (item['selected']) {
                checked++;
            }
        });
        if (checked === array.length) {
            originData['checkAll'] = true;
        } else {
            originData['checkAll'] = false;
        }

    }

    public formatWMSNumber(value: number): string {
        return value ? value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '0';
    }

    public Pagination(data) {
        const totalCount = data['total'];
        const ItemOnPage = data['count'];
        const pageCount = data['total_pages'];
        const currentPage = data['current_page'];
        const perPage = data['per_page'];
        const numLinks = data['numLinks'] ? data['numLinks'] : 3;
        let start = 1;
        let end = 1;
        let tmpPageCount = [];
        if (currentPage > numLinks) {
            start = currentPage - numLinks;
        } else {
            start = 1;
        }
        if (currentPage + numLinks < pageCount) {
            end = currentPage + numLinks;
        } else {
            end = pageCount;
        }
        // Create tmp for loop perPage in view
        for (let i = start; i <= end; i++) {
            tmpPageCount[i] = i;
        }
        tmpPageCount = tmpPageCount.filter(function (item) {
            return item !== undefined;
        });
        return tmpPageCount;
    }

    object2Array(object: Object) {
        const temp_arr: Array<any> = [];
        Object.keys(object).forEach((key: any) => {
            temp_arr.push({
                key: key,
                value: object[key]
            });
        });
        console.log('temp_arr ', temp_arr);
        return temp_arr;
    }

    
}
