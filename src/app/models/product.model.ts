export interface Product {
  rn: number;
  pro_odr_hdr_id: number;
  pr_no: string;
  sche_pro_hdr_no: string;
  sche_pro_hdr_id: string;
  itm_code: string;
  is_active: string;
  odr_date: Date;
  exp_date: Date;
  emp_id: string;
  description: string;
  sort: string;
  bom_name: string;
}
