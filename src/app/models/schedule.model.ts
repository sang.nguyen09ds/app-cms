export interface Schedule {
  sche_pro_hdr_id: number;
  sche_pro_hdr_no: string;
  plan_hdr_no: string;
  plan_hdr_id: string;
  is_active: string;
  sort: string;
  qty: string;
  itm_code: string;
  plan_date: string;
  start_date: string;
  complete_date: string;
  bom_code: string;
  description: string;
}
