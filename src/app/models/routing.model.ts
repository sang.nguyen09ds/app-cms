export interface Routing {
  rn: number;
  rout_hdr_id: number;
  rout_code: string;
  rout_name: string;
  description: string;
  is_active: string;
  deleted: number;
  created_at: string;
  created_by: string;
  updated_at: string;
  updated_by: string;
  deleted_at: string;
  deleted_by: string;
  itm_code: string;
  itm_name: string;
  sort: number;
}
