export interface Bom {
  rn: number;
  bom_hdr_id: number;
  bom_code: string;
  bom_name: string;
  wc_code: string;
  description: string;
  is_active: string;
  deleted: number;
  created_at: string;
  created_by: string;
  updated_at: string;
  updated_by: string;
  deleted_at: string;
  deleted_by: string;
  itm_code: string;
  itm_name: string;
  sort: number;
}
