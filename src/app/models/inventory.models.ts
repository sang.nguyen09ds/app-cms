export interface Inventory {
  inventory_id: number;
  sort: string;
  itm_code: string;
  itm_name: string;
  qty: string;
  md_sts: string;
  unit_price: string;
  uom_name: string;
  uom_code: string;
}
