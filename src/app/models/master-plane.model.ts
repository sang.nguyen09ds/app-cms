export interface MasterPlan {
  rn: number;
  plan_hdr_id: number;
  no: string;
  note: string;
  is_active: string;
  deleted: number;
  created_at: string;
  created_by: string;
  updated_at: string;
  updated_by: string;
  deleted_at: string;
  deleted_by: string;
  itm_code: string;
  itm_name: string;
  sort: string;
  qty: number;
  plan_date: string;
  start_date: string;
  complete_date: string;
  bom_hdr_id: string;
  bom_code: string;
}
