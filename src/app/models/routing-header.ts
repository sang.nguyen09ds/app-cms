export interface Routing {
  rout_dtl_id: number;
  rout_hdr_id: string;
  work_center_id: string;
  time_est: string;
  labor_est: string;
  is_active: string;
  deleted: number;
  created_at: string;
  created_by: string;
  updated_at: string;
  updated_by: string;
  deleted_at: string;
  deleted_by: string;
  sort: number;
  wc_code: string;
}
