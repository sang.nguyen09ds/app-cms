export interface DataType {
  md_type: string;
  mdt_name: string;
  mdt_des: string;
  mdt_data: string;
  md_sts: string;
}
