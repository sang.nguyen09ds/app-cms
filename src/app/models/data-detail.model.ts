export interface DataDetail {
  id: number;
  md_type: string;
  dataTypeId: number;
  md_name: string;
  md_code: string;
  md_data: string;
  md_des: string;
  sort: number;
  md_sts: string;
}
