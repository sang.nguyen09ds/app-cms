export interface User{
    email:string;
    is_super:number;
    permissions:Object;
    role_code:string;
    role_name:string;
    user_id:number;
    username:string;
}