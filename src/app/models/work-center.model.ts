export interface WorkCenter {
  rn: number;
  work_center_id?: number;
  wc_code: string;
  wc_name: string;
  itm_code: string;
  itm_name: string;
  description: string;
  is_active: string;
  deleted: number;
  created_at: string;
  created_by: string;
  updated_at: string;
  updated_by: string;
  deleted_at: string;
  deleted_by: string;
}
