import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, ValidationService } from 'src/app/shared/shared.module';
import { APIConfig } from '../../../shared/services/config';

@Component({
  templateUrl: 'setup-password.html'
})
export class SetupPasswordComponent implements OnInit {
  public loading = false;
  public tokenValue: string;
  public errorMessages: Array<string> = [];
  public form: FormGroup;
  public msgNotification: any;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: Http,
    private validators: ValidationService,
    private httpClient: HttpClient,
    private api: APIConfig
  ) {}

  ngOnInit() {
    this.buildForm();

    this.activatedRoute.queryParams.subscribe(params => {
      const headerObj = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      });
      this.tokenValue = params['token'];
    });
  }

  private buildForm() {
    this.form = new FormGroup({
      new_password: new FormControl('', [Validators.required]),
      repeat_new_password: new FormControl('', [Validators.required])
    });
  }

  public redirectToLogin() {
    this.router.navigate(['/login']);
  }

  public onSubmit(): void {
    if (this.checkPasswordPolicy()) {
      this.loading = true;
      this.httpClient.activeLoading();
      this.form.value['token'] = this.tokenValue;
      this.http.post(this.api.SETUP_PASS, this.form.value).subscribe(
        data => {
          this.httpClient.disableLoading();
          this.msgNotification = {
            status: 'success',
            content: `Password was changed successfully! Please <a href="#/login">login</a> again.`
          };
        },
        err => {
          this.httpClient.handleErrorFront(err);
          this.loading = false;
          this.msgNotification = {
            status: 'danger',
            content: this.httpClient.parseError(err)
          };
        }
      );
    }
  }

  public checkPasswordPolicy() {
    const value = this.form.value;
    this.errorMessages = this.validators.validatePasswordByPolicy(
      value['new_password'],
      value['repeat_new_password']
    );
    if (this.errorMessages.length > 0) {
      return false;
    }
    return true;
  }

  public checkPasswordConfirm() {
    const newPass = this.form.value['new_password'];
    const repeatPass = this.form.value['repeat_new_password'];
    return newPass && newPass > 7 && newPass === repeatPass;
  }

  public removeMessage(message: string) {
    const index = this.errorMessages.indexOf(message);
    this.errorMessages.splice(index, 1);
  }
}
