import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Headers, Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, ValidationService } from 'src/app/shared/shared.module';
import { APIConfig } from '../../../shared/services/config';

@Component({
  templateUrl: 'reset.html'
})
export class ResetComponent implements OnInit {
  public loading = false;
  public tokenValue: string;
  public isExpiredToken = false;
  public errorMessages: Array<string> = [];
  public form: FormGroup;
  public msgNotification: any;
  public messages: any = null;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: Http,
    private validators: ValidationService,
    private httpClient: HttpClient,
    private api: APIConfig
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const headerObj = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      });
      this.tokenValue = params['token'];
      if (this.tokenValue) {
        // check expired of token is valid
        this.http
          .get(this.api.RESET_PASS_TOKEN + '/' + this.tokenValue, {
            headers: headerObj
          })
          .subscribe(
            data => {
              this.buildForm();
            },
            err => {
              this.isExpiredToken = true;
              this.messages = this.httpClient.parseError(err);
            }
          );
      }
    });
  }

  private buildForm() {
    this.form = new FormGroup({
      password: new FormControl('', [Validators.required]),
      password_confirmation: new FormControl('', [Validators.required])
    });
  }

  public redirectToLogin() {
    this.router.navigate(['/login']);
  }

  public onSubmit(): void {
    if (this.checkPasswordPolicy()) {
      this.loading = true;
      this.httpClient.activeLoading();
      this.form.value['token'] = this.tokenValue;
      this.http.post(this.api.RESET_PASS, this.form.value).subscribe(
        data => {
          this.httpClient.disableLoading();
          this.msgNotification = {
            status: 'success',
            content: `Password was changed successfully! Please <a href="#/login">login</a> again.`
          };
        },
        err => {
          this.loading = false;
          this.httpClient.handleErrorFront(err);
          this.msgNotification = {
            status: 'danger',
            content: this.httpClient.parseError(err)
          };
        }
      );
    }
  }

  public checkPasswordPolicy() {
    const pass = this.form.value['password'];
    const passConfirm = this.form.value['password_confirmation'];
    this.errorMessages = this.validators.validatePasswordByPolicy(
      pass,
      passConfirm
    );
    if (this.errorMessages.length > 0) {
      return false;
    }
    return true;
  }

  public checkPasswordConfirm() {
    return (
      this.form.value['password'] &&
      this.form.value['password'].length > 7 &&
      this.form.value['password'] === this.form.value['password_confirmation']
    );
  }

  public removeMessage(message: string) {
    const index = this.errorMessages.indexOf(message);
    this.errorMessages.splice(index, 1);
  }
}
