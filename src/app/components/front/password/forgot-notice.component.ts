import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'forgot-notice',
    templateUrl: 'forgot-notice.html'
})

export class ForgotNoticeComponent {
    constructor(public router: Router) {}

    public goBack() {
        this.router.navigate(['/login']);
    }
}
