import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { APIConfig, HttpClient } from 'src/app/shared/shared.module';

@Component({
  templateUrl: 'forgot.html'
})
export class ForgotComponent implements OnInit {
  public form: FormGroup;
  public loading = false;
  public submitted = false;

  constructor(
    private http: Http,
    private router: Router,
    private api: APIConfig,
    private httpClient: HttpClient
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  private buildForm() {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required]),
      reset_password_url: new FormControl(
        window.location.origin + '/#/reset-password'
      )
    });
  }

  public onSubmit() {
    this.submitted = true;
    if (this.form.valid) {
      this.httpClient.activeLoading();
      this.http
        .post(this.api.FORGOT_PASS, this.form.value)
        .map((res: Response) => res.json())
        .subscribe(
          res => {
            this.httpClient.disableLoading();
            this.router.navigate(['/forgot-password-notice']);
          },
          err => {
            this.httpClient.handleErrorFront(err);
            this.loading = false;
          }
        );
    }
  }
}
