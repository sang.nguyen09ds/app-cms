import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { WMSMessageComponent } from '../../../_directives/index';
import 'rxjs/add/operator/map';
import {
  HttpClient,
  UserPermissionsService
} from 'src/app/shared/shared.module';

@Component({
  templateUrl: 'login.html'
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginURL: string;
  public form: FormGroup;
  public loading: Boolean = false;
  public submitted: Boolean = false;
  public messages = [];
  public intervalCheckVersion = 300000;
  public callCheckVersion: any;
  token:any;
  constructor(
    private router: Router,
    private httpClient: HttpClient,
    private formBuilder: FormBuilder,
    private userService: UserPermissionsService
  ) {
    this.userService.currentUser = null;
  }

  public ngOnInit() {
    this.buildForm();
    localStorage.clear();
  }
  public ngOnDestroy() {}

  private buildForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      device_type: ['']
    });
  }

  private onSubmit() {
    this.submitted = true;
    if (this.form.valid) {
      this.loading = true;
      this.httpClient.activeLoading();
      this.userService.login(this.form.value).subscribe(
        res => {
          this.httpClient.disableLoading();
          this.loading = false;
          if (res.code === 405) {
            this.messages.push({
              status: 'danger',
              txt:
                res.message +
                '. Please click <a id="resetPass" href="#/reset-password?token=' +
                res.token +
                '">here</a> to reset password.'
            });
          } else {
            if (res.token) {
              localStorage.setItem('id_token', res.token);
              localStorage.setItem('jwt', res.token);
              this.router.navigate(['']);
            } else {
              this.messages[0] = {
                status: 400,
                message: 'No Token Found.'
              };
            }
          }
        },
        err => {
          this.httpClient.handleErrorFront(err);
        }
      );
    }
  }
}
