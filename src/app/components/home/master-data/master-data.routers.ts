import { Routes } from '@angular/router';
import { DataDetailComponent } from './data-detail/data-detail.component';
import { DataTypeComponent } from './data-type/data-type.component';
import { MasterDataComponent } from './master-data.component';

export const masterDataRoutes: Routes = [
  {
    path: '',
    component: MasterDataComponent,
    children: [
      { path: '', redirectTo: '/data-detail', pathMatch: 'full' },
      {
        path: 'data-type',
        component: DataTypeComponent,
        data: { name: 'Data Type' }
      },
      {
        path: 'data-detail',
        component: DataDetailComponent,
        data: { name: 'Data Detail' }
      }
    ]
  }
];
