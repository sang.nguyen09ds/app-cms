import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDetailPopUpComponent } from './data-detail-pop-up.component';

describe('DataDetailPopUpComponent', () => {
  let component: DataDetailPopUpComponent;
  let fixture: ComponentFixture<DataDetailPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataDetailPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDetailPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
