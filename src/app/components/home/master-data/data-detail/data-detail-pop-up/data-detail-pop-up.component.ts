import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import {
  DataTypeService,
  ValidationService
} from 'src/app/shared/shared.module';

@Component({
  selector: 'app-data-detail-pop-up',
  templateUrl: './data-detail-pop-up.component.html',
  styleUrls: ['./data-detail-pop-up.component.css']
})
export class DataDetailPopUpComponent implements OnInit {
  public disabled: boolean;
  public Form: FormGroup;
  public data;
  dataValueChange: Subject<Object>;

  public listDataType;
  private defaultActiveValue = '1';
  private defaultDataType;

  labelPopUp;

  constructor(
    private formBuilder: FormBuilder,
    private _validationService: ValidationService,
    private _dataTypeService: DataTypeService,
    private _toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) {}

  ngOnInit() {
    this.buildForm(this.data);
    this.dataValueChange = new Subject();
  }

  getListDataType($event = '') {
    const param = '?mdt_name=' + $event;
    this._dataTypeService.getAllDataTypes(param).subscribe(reponse => {
      this.listDataType = reponse.data;
      if (this.listDataType.length) {
        this.defaultDataType = this.listDataType[0].md_type;
      } else {
        this._toastyService.error('Not Found !');
      }
    });
  }

  selectDataType($event) {}

  buildForm(data = { md_sts: '1' }) {
    this.labelPopUp = data['md_type'] ? 'Update' : 'Create';
    const status = data['md_sts'] == '1' ? true : false;
    if (!this.disabled) {
      this.loadData(data);
    }
    this.Form = this.formBuilder.group({
      id: [data['id']],
      md_type: [data['md_type']],
      sort: [data['sort'], [this._validationService.inPutOnlyNumber]],
      md_code: [data['md_code'], Validators.required],
      md_name: [data['md_name'], Validators.required],
      md_des: [data['md_des']],
      md_data: [data['md_data']],
      md_sts: [{ value: status, disabled: this.disabled }]
    });
  }

  loadData(data) {
    this.getListDataType(data['md_type']);
  }

  close() {
    this.bsModalRef.hide();
  }

  public save() {
    const dataDetail = this.Form.value;
    this.dataValueChange.next({
      ...dataDetail,
      md_sts: dataDetail['md_sts'] ? '1' : '0'
    });
  }
}
