import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { DataDetail } from 'src/app/models/data-detail.model';
import { ParamsGetDataDetail } from 'src/app/shared/services/data-detail.service';
import {
  DataDetailService,
  Functions,
  SwalService
} from 'src/app/shared/shared.module';
import { DataDetailPopUpComponent } from '../data-detail-pop-up/data-detail-pop-up.component';

@Component({
  selector: 'app-data-detail-list',
  templateUrl: './data-detail-list.component.html',
  styleUrls: ['./data-detail-list.component.css']
})
export class DataDetailListComponent implements OnInit, OnDestroy {
  public listDataDetail: DataDetail[];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;
  private inputDataDetail;
  private subscriber;
  constructor(
    private _dataDetailService: DataDetailService,
    private swalService: SwalService,
    private formBuilder: FormBuilder,
    private func: Functions,
    private toastyService: ToastyService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getListDataDetail();
    this.buildSearchForm();
  }
  buildSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      md_name: [''],
      md_code: [''],
      md_type: ['']
    });
  }
  getListDataDetail(params: ParamsGetDataDetail = { page: 1 }): void {
    this.subscription = this._dataDetailService
      .getlistDataDetail({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.listDataDetail = reponse.data;
        this.initPagination(reponse);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openPopUpCreate() {
    this.isCreate = true;
    this.inputDataDetail = this.modalService.show(DataDetailPopUpComponent,{ backdrop:'static'});
    this.subscriber = this.inputDataDetail.content.dataValueChange.subscribe(
      data => {
        this.saveData(data);
      }
    );
  }

  openPopUpView(data: DataDetail) {
    this.modalService.show(DataDetailPopUpComponent, {
      backdrop:'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }

  openPopUpEdit(data: DataDetail) {
    this.isCreate = false;
    this.inputDataDetail = this.modalService.show(DataDetailPopUpComponent, {
      backdrop:'static',
      initialState: {
        data,
        disabled: false
      }
    });

    this.subscriber = this.inputDataDetail.content.dataValueChange.subscribe(
      data => {
        this.saveData(data);
      }
    );
  }
  saveData(dataDetail: DataDetail) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this._dataDetailService.create(dataDetail);
    } else {
      observerChangeData = this._dataDetailService.update(
        dataDetail.md_code,
        dataDetail
      );
    }
    observerChangeData.subscribe(reponse => {
      this.toastyService.success(
        `${this.isCreate ? 'Create' : 'Update'} success`
      );
      this.inputDataDetail.hide();
      this.subscriber.unsubscribe();
      this.pageChange(this.pagination['current_page']);
    });
  }

  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this._dataDetailService.delete(id).subscribe(reponse => {
        this.toastyService.success(`Delete success`);
        this.pageChange(this.pagination['current_page']);
      });
    });
  }
  public search() {
    const valuesSearch = this.searchForm.value;
    this.getListDataDetail(valuesSearch);
  }

  public reset() {
    this.searchForm.reset();
    this.getListDataDetail();
  }

  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange($event) {
    const valuesSearch = this.searchForm.value;
    this.getListDataDetail({ ...valuesSearch, page: $event });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }
}
