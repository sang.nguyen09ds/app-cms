import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDetailListComponent } from './data-detail-list.component';

describe('DataDetailListComponent', () => {
  let component: DataDetailListComponent;
  let fixture: ComponentFixture<DataDetailListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataDetailListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDetailListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
