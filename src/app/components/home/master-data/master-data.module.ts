import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { DataDetailListComponent } from './data-detail/data-detail-list/data-detail-list.component';
import { DataDetailPopUpComponent } from './data-detail/data-detail-pop-up/data-detail-pop-up.component';
import { DataDetailComponent } from './data-detail/data-detail.component';
import { DataTypePopUpComponent } from './data-type/data-type-edit/data-type-pop-up.component';
import { DataTypeListComponent } from './data-type/data-type-list/data-type-list.component';
import { DataTypeComponent } from './data-type/data-type.component';
import { MasterDataComponent } from './master-data.component';
import { masterDataRoutes } from './master-data.routers';

@NgModule({
  imports: [
    SharedModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    DirectiveModule,
    RouterModule.forChild(masterDataRoutes)
  ],
  entryComponents: [DataTypePopUpComponent, DataDetailPopUpComponent],
  declarations: [
    MasterDataComponent,
    DataTypeComponent,
    DataTypeListComponent,
    DataTypePopUpComponent,
    DataDetailComponent,
    DataDetailListComponent,
    DataDetailPopUpComponent
  ]
})
export class MasterDataModule {}
