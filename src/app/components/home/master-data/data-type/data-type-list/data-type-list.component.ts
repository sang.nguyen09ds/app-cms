import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { DataType } from 'src/app/models/data-type.model';
import { ParamsGetDataTypes } from 'src/app/shared/services/data-type.service';
import {
  DataTypeService,
  Functions,
  SwalService
} from 'src/app/shared/shared.module';
import { DataTypePopUpComponent } from '../data-type-edit/data-type-pop-up.component';

@Component({
  selector: 'app-data-type-list',
  templateUrl: './data-type-list.component.html'
})
export class DataTypeListComponent implements OnInit, OnDestroy {
  public dataTypes: DataType[];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;
  private dataTypePopUp;
  private subscriber;

  constructor(
    private _dataTypeService: DataTypeService,
    private swalService: SwalService,
    private func: Functions,
    private toastyService: ToastyService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getListDataTypes();
    this.buildSearchForm();
  }

  getListDataTypes(params: ParamsGetDataTypes = { page: 1 }) {
    this.subscription = this._dataTypeService
      .getDataTypes({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.dataTypes = reponse.data;
        this.initPagination(reponse);
      });
  }

  buildSearchForm() {
    this.searchForm = new FormGroup({
      md_type: new FormControl(''),
      md_name: new FormControl('')
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openPopUpCreate() {
    this.isCreate = true;

    this.dataTypePopUp = this.modalService.show(DataTypePopUpComponent,{ backdrop:'static'});
    this.subscriber = this.dataTypePopUp.content.dataValueChange.subscribe(
      data => {
        this.saveData(data);
      }
    );
  }

  openPopUpView(data: DataType) {
    this.modalService.show(DataTypePopUpComponent, {
      backdrop:'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }

  openPopUpEdit(data: DataType) {
    this.isCreate = false;

    this.dataTypePopUp = this.modalService.show(DataTypePopUpComponent, {
      backdrop:'static',
      initialState: {
        data,
        disabled: false
      }
    });
    this.subscriber = this.dataTypePopUp.content.dataValueChange.subscribe(
      data => {
        this.saveData(data);
      }
    );
  }

  saveData(dataType) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this._dataTypeService.create(dataType);
    } else {
      observerChangeData = this._dataTypeService.update(
        dataType.md_type,
        dataType
      );
    }
    observerChangeData.subscribe(reponse => {
      this.toastyService.success(
        `${this.isCreate ? 'Create' : 'Update'} success`
      );
      this.dataTypePopUp.hide();
      this.subscriber.unsubscribe();
      this.pageChange(this.pagination['current_page']);
    });
  }

  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this._dataTypeService.delete(id).subscribe(reponse => {
        this.toastyService.success(`Delete success`);
        this.pageChange(this.pagination['current_page']);
      });
    });
  }

  public search() {
    const valuesSearch = this.searchForm.value;
    this.getListDataTypes(valuesSearch);
  }

  public reset() {
    this.searchForm.reset();
    this.getListDataTypes();
  }

  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange($event) {
    const valuesSearch = this.searchForm.value;
    this.getListDataTypes({ ...valuesSearch, page: $event });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }
}
