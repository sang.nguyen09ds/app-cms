import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DataTypePopUpComponent } from './data-type-pop-up.component';

describe('DataTypeEditComponent', () => {
  let component: DataTypePopUpComponent;
  let fixture: ComponentFixture<DataTypePopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DataTypePopUpComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTypePopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
