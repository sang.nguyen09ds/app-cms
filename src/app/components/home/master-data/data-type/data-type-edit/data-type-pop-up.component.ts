import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-data-type-pop-up',
  templateUrl: './data-type-pop-up.component.html',
  styleUrls: ['./data-type-pop-up.component.css']
})
export class DataTypePopUpComponent implements OnInit {
  public disabled: boolean;
  public data;
  public Form: FormGroup;
  defaultActiveValue = '1';
  labelPopup;

  dataValueChange: Subject<Object>;

  constructor(
    private formBuilder: FormBuilder,
    private bsModalRef: BsModalRef
  ) {}

  ngOnInit() {
    this.buildForm(this.data);
    this.dataValueChange = new Subject();
  }

  buildForm(data = { md_sts: '1' }) {
    this.labelPopup = data['md_type'] ? 'Update' : 'Create';
    const status = data['md_sts'] == '1' ? true : false;

    this.Form = this.formBuilder.group({
      id: [data['id']],
      md_type: [data['md_type'], Validators.required],
      mdt_name: [data['mdt_name'], Validators.required],
      mdt_des: [data['mdt_des']],
      mdt_data: [data['mdt_data']],
      md_sts: [{ value: status, disabled: this.disabled }]
    });
  }

  public save() {
    const dataType = this.Form.value;
    this.dataValueChange.next({
      ...dataType,
      md_sts: dataType['md_sts'] ? '1' : '0'
    });
  }

  close() {
    this.bsModalRef.hide();
  }
}
