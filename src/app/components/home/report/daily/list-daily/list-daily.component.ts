import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { DailyService} from '../daily.service';
import { SwalService, Functions, UserPermissionsService } from 'src/app/shared/shared.module';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-list-daily',
  templateUrl: './list-daily.component.html',
})
export class ListDailyComponent implements OnInit {

  public dataList: Array<any> = [];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;
  dailyAction: any;
  printerAPIUrl = '';
  constructor(
    private userPermissionsService: UserPermissionsService,
    private _Service: DailyService,
    private swalService: SwalService,
    private func: Functions,
    private toastyService: ToastyService
  ) {}

  ngOnInit() {
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-REPORT-MODULE-DAILY',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.getList();
        this.buildSearchForm();
      }
    );
  }
  getList(pageNum = { page: 1 }) {
    let params = `&limit=${this.perPage}&page=${pageNum}&sort[id]=desc`;
    this.printerAPIUrl = this._Service.getExportDaily(); 
    console.log('printerAPIUrl',this.printerAPIUrl)
    this._Service.get(params).subscribe(
        (data) => {
            this.dataList = data['data'];
            if (data['meta']) {
              this.func.initPagination(data);
            }
        },
        (err) => { }
    );
  }
  buildSearchForm() {
    this.searchForm = new FormGroup({
      name: new FormControl(''),
      workTime: new FormControl(''),
      issueToday: new FormControl('')
    });
  }
  public search() {
    const valuesSearch = this.searchForm.value;
    this.getList(valuesSearch);
  }

  public reset() {
    this.searchForm.patchValue({
      name: '',
      workTime:'',
      issueToday:'',
  });
  this.getList();
  }

  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getList({ ...valuesSearch, page: pageNumber });
  }
}
