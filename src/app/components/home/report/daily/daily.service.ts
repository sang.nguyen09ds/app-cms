import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class DailyService {


  constructor(private http: HttpClient) {}

  public get(params: any) {
    return this.http.get(this.http.apiCommon + '/report-module-status-userDaily', {params});
  }
  public getExportDaily(){
    return this.http.apiCommon + '/report-module-status-userDaily/export';
}
}

