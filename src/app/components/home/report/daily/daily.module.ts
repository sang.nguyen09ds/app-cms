import { NgModule } from '@angular/core';
import { DailyService } from './daily.service';
import { BsModalRef, ModalModule, BsDatepickerModule, TabsModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { CKEditorModule } from 'ngx-ckeditor';
import { ListDailyComponent } from './list-daily/list-daily.component';
import { DailyComponent } from './daily.component';
import { dailyRoutes } from './daily.routes';
@NgModule({
  imports: [
    RouterModule.forChild(dailyRoutes),
    SharedModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DirectiveModule,
    CKEditorModule,
    TabsModule.forRoot(),
  ],
  providers: [DailyService,BsModalRef],
  declarations: [ListDailyComponent, DailyComponent]
})
export class DailyModule { }
