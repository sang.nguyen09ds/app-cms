import { Routes } from '@angular/router';
import { DailyComponent } from './daily.component';
import { ListDailyComponent } from './list-daily/list-daily.component';


export const dailyRoutes: Routes = [
    {
        path: '',
        component: DailyComponent,
        data: {name: 'Hàng ngày'},
        children: [
            { path: '', component: ListDailyComponent, data: { name: 'Danh sách' } },
        ]
    }
];
