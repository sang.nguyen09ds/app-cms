import { Routes } from '@angular/router';
import { DepartmentComponent } from './department.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentPopupComponent } from './department-popup/department-popup.component';

export const departmentRoutes: Routes = [
    {
        path: '',
        component: DepartmentComponent,
        data: {name: 'Department'},
        children: [
            { path: '', component: DepartmentListComponent, data: { name: 'Department List' } },
            { path: 'new', component: DepartmentPopupComponent, data: { name: 'Add Department', action: 'new' } },
            { path: ':id/:edit', component: DepartmentPopupComponent, data: { name: 'Edit Department', action: 'edit' } },
            { path: ':id', component: DepartmentPopupComponent, data: { name: 'Detail Department', action: 'view' } }
        ]
    }
];
