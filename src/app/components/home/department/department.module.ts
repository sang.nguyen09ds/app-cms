import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { departmentRoutes } from './department.routes';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule, BsModalRef } from 'ngx-bootstrap';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { Service } from './service';
import { DepartmentComponent } from './department.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentPopupComponent } from './department-popup/department-popup.component';

@NgModule({
  imports: [
    RouterModule.forChild(departmentRoutes),
    SharedModule,
    ModalModule.forRoot(),
    DirectiveModule,
  ],
  providers: [Service,BsModalRef],
  declarations: [DepartmentComponent, DepartmentListComponent, DepartmentPopupComponent]
})
export class DepartmentModule { }
