import { Component, OnInit } from '@angular/core';
import { HttpClient, UserPermissionsService } from 'src/app/shared/shared.module';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Service } from '../service';
import { BsModalRef } from 'ngx-bootstrap';
import { ToastyService } from 'ng2-toasty';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-department-popup',
  templateUrl: './department-popup.component.html',
})
export class DepartmentPopupComponent implements OnInit {
  labelButtonSave;
  public disabled: boolean;
  public Form: FormGroup;
  popupTile: 'Create Department'
  private data;
  dataValueChange: Subject<Object>;

  constructor(
    private userPermissionsService: UserPermissionsService,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public __service: Service,
    public toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.buildForm(this.data);
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-DEPARTMENTS',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.dataValueChange = new Subject();
      }
    );
  }
  buildForm(data = { is_active: '1' }) {
    this.labelButtonSave = data['id'] ? 'Update' : 'Create';
    const is_active = data['is_active'] == '1' ? true : false;
    this.Form = this.formBuilder.group({
      id: [data['id']],
      code: [data['code'], Validators.required],
      name: [data['name'], Validators.required],
      description: [data['description']],
      is_active: [is_active]
    });
  }
  public save() {
    const dept = this.Form.value;

    this.dataValueChange.next({
      ...dept,
      is_active: dept['is_active'] ? '1' : '0'
    });
    this.close();
  }

  close() {
    this.bsModalRef.hide();
  }
}
