import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {
  constructor(private http: HttpClient) {}

  public get(params: any) {
    return this.http.get(this.http.apiCommon + '/departments' ,{params});
  }
  public getUserID(id) {
    return this.http.get(this.http.apiCommon + '/departments/' + id);
  }
  public create(dept: any) {
    return this.http.post(this.http.apiCommon + '/departments', dept);
  }

  public update(id: any ,data: any) {
    return this.http.put(this.http.apiCommon + '/departments/' + id ,data);
  }

  public delete(id: any) {
    return this.http.delete(this.http.apiCommon + '/departments/' + id);
  }
  
  public getRole(param){
    return this.http.get(this.http.apiCommon + '/roles', param);
  }
  public getDept(param){
    return this.http.get(this.http.apiCommon + '/departments', param);
  }
}
export interface ParamsGetData {
  page?: number;
  limit?: number;
  code?: string;
  name?: string;
}
