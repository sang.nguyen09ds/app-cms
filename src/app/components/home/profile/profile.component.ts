import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder,FormControl, Validators} from '@angular/forms';
import {ToastyService,ToastyConfig} from 'ng2-toasty';
import { UserManagementService } from '../user-management/user-management.service';
import { UsersFunctions } from 'src/app/shared/user.functions';
import { CommonService } from 'src/app/shared/services/common.services';
import { ValidationService, Functions } from 'src/app/shared/shared.module';
import { ProfileServices } from './profile.service';
@Component({
    selector:"profile",
    providers:[UserManagementService,ProfileServices,UsersFunctions,CommonService,ValidationService],
    templateUrl: 'profile.component.html'
})
export class ProfileComponent implements OnInit{

    // settup form
    form:FormGroup;
    public Action:any;
    public currentUser={};
    public submitted:any;
    public changePass:boolean=false;
    constructor(
        public _fb: FormBuilder,
        public user:UserManagementService,
        public profile: ProfileServices,
        public userFunc:UsersFunctions,
        public toastyService: ToastyService){
    }
    ngOnInit(){
        this.buildForm();
        this.getUsers();
    }
    public buildForm(data={}){
        this.form =this._fb.group({
            id:[data['id'] ? data['id'] : ''],
            code: [{value:data['code'], disabled:true}, [Validators.required]],
            password: [{value:data['password'], disabled:this.Action=='view'},[Validators.required]],
            retype_password: [{value:data['password'], disabled:this.Action=='view'},[Validators.required]],
            email: [{value:data['email'], disabled:this.Action=='view'}, [Validators.required]],
            is_active: [{value:data['is_active'], disabled:true}],
            full_name: [{value:data['full_name'] ? data['full_name'] : '', disabled:this.Action=='view'},[Validators.required]],
            last_name: [data['last_name']],
            address: [data['address']],
            language: [data['language'] ? data['language'] : 'VI'],
            phone: [data['phone']],
            birthday: [data['birthday']],
            avatar: [data['avatar'] ? data['avatar'] : ''],
            id_number: [data['id_number']],
        });

        setTimeout(()=>{
            this.form.controls['password'].setErrors(null);
            this.form.controls['retype_password'].setErrors(null);
        })
        this.form.valueChanges.subscribe(
            value=>{
                this.userFunc.matchedPassword(this.form);
            }
        )
    }
    public getUsers(){
        this.profile.getProfile()
            .subscribe( data => {
                this.buildForm(data['data']);
                this.currentUser=data['data'];
            });
    }
    public Save(){
        this.submitted=true;
        if(this.form.valid){
            this.Update();
        }
    }
    public Update(data={}){
        this.profile.updateProfile(this.form.getRawValue())
            .subscribe(
                res => {
                    if(res.status){
                        this.toastyService.success(res.status);
                    }
                    else{
                        this.toastyService.success('Update success !');
                    }
                   
                }
            );
    }
    public _changePass(){
            this.changePass=!this.changePass;
            this.form.controls['password'].setValue('');
            this.form.controls['retype_password'].setValue('');
            this.form.controls['password'].setErrors(null);
            this.form.controls['retype_password'].setErrors(null);
            if(this.changePass){
                this.form.controls['password'].setErrors({'required':true});
                this.form.controls['retype_password'].setErrors({'required':true});
            }
    }
    handleInputChange(e:any) {
        var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        var pattern = /image-*/;
        var reader = new FileReader();
        if (!file.type.match(pattern)) {
            this.toastyService.success('success');
            return;
        }
        // 1048576 kb = 1MB
        if(file.size>1048576){
            this.toastyService.error('Error size too big');
        }
        reader.onload = this._handleReaderLoaded.bind(this);
        reader.readAsDataURL(file);
    }
    _handleReaderLoaded(e:any) {
        var reader = e.target;
        this.form.patchValue({avatar:reader.result});
    }
}