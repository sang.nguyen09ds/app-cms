import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';


@Injectable()
export class ProfileServices {
    private apiUser: String = `${this.http.apiMaster}/users`;

    constructor(
        private http: HttpClient
    ) {}

    public getProfile() {
        return this.http.get(`${this.apiUser}/profile`);
    }

    public changePassword(passwordChangeObj: any) {
        return this.http.post(`${this.apiUser}/change-password`, passwordChangeObj);
    }

    public updateProfile(profileObj: any) {
        return this.http.post(`${this.apiUser}/profile`, profileObj);
    }
}
