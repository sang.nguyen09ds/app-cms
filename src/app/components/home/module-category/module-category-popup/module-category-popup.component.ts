import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { HttpClient, UserPermissionsService } from 'src/app/shared/shared.module';
import { Service } from '../service';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-module-category-popup',
  templateUrl: './module-category-popup.component.html',
})
export class ModuleCategoryPopupComponent implements OnInit {

  public disabled: boolean;
  labelButtonSave;
  public Form: FormGroup;
  popupTile: 'Create Category Modules'
  private data;
  dataValueChange: Subject<Object>;
  constructor(
    private userPermissionsService: UserPermissionsService,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public _Service: Service,
    public toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-CATEGORY-ISSUE',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.buildForm(this.data);
    this.dataValueChange = new Subject();
    this.getModule();
      }
    );
  }
  buildForm(data = { is_active: '1' }) {
    this.labelButtonSave = data['id'] ? 'Update' : 'Create';
    const is_active = data['is_active'] == '1' ? true : false;
    this.Form = this.formBuilder.group({
      id: [data['id']],
      code: [data['code'], Validators.required],
      name: [data['name'], Validators.required],
      module_id: [data['module_id'], Validators.required],
      module_name: [data['module_name'], Validators.required],
      description: [data['description']],
      is_active: [is_active]
    });
  }
  public ListModule: Array<any> = [];
  public getModule(name = ''){
    let params = `?name= ${name}&limit=20`;
    this._Service.getModule(params).subscribe(data =>{
        this.ListModule = data['data'];
        console.log('moudle', data['data'])
    });
  }
  selectModules($event){
    this.Form.patchValue({
      module_id : $event.id,
    });
  }
  public save() {
    const dept = this.Form.value;

    this.dataValueChange.next({
      ...dept,
      is_active: dept['is_active'] ? '1' : '0'
    });
    this.close();
  }

  close() {
    this.bsModalRef.hide();
  }
}

