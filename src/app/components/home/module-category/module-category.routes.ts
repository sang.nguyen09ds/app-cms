import { Routes } from '@angular/router';
import { ModuleCategoryComponent } from './module-category.component';
import { ModuleCategoryListComponent } from './module-category-list/module-category-list.component';
import { ModuleCategoryPopupComponent } from './module-category-popup/module-category-popup.component';

export const moduleCategoryRoutes: Routes = [
    {
        path: '',
        component: ModuleCategoryComponent,
        data: {name: 'Danh mục dự án'},
        children: [
            { path: '', component: ModuleCategoryListComponent, data: { name: 'Danh sách các danh mục dự án' } },
            { path: 'new', component: ModuleCategoryPopupComponent, data: { name: 'Tạo danh mục dự án', action: 'new' } },
            { path: ':id/:edit', component: ModuleCategoryPopupComponent, data: { name: 'Chỉnh sửa danh mục dự án', action: 'edit' } },
            { path: ':id', component: ModuleCategoryPopupComponent, data: { name: 'Chi tiết danh mục dự án', action: 'view' } }
        ]
    }
];
