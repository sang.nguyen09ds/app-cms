import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleCategoryComponent } from './module-category.component';

describe('ModuleCategoryComponent', () => {
  let component: ModuleCategoryComponent;
  let fixture: ComponentFixture<ModuleCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModuleCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
