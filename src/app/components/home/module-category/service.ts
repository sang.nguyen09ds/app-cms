import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {
  constructor(private http: HttpClient) {}

  public get(params: any) {
    return this.http.get(this.http.apiCommon + '/module-category' ,{params});
  }
  public getUserID(id) {
    return this.http.get(this.http.apiCommon + '/module-category/' + id);
  }
  public create(dept: any) {
    return this.http.post(this.http.apiCommon + '/module-category', dept);
  }

  public update(id: any ,data: any) {
    return this.http.put(this.http.apiCommon + '/module-category/' + id ,data);
  }

  public delete(id: any) {
    return this.http.delete(this.http.apiCommon + '/module-category/' + id);
  }
  public getModule(param ){
    return this.http.get(this.http.apiCommon + '/module' + param);
  }
}
export interface ParamsGetData {
  page?: number;
  limit?: number;
  code?: string;
  name?: string;
}
