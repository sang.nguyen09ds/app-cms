import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { Service, ParamsGetData } from '../service';
import { Functions } from 'src/app/shared/functions';
import { SwalService, UserPermissionsService } from 'src/app/shared/shared.module';
import { BsModalService } from 'ngx-bootstrap';
import { ToastyService } from 'ng2-toasty';
import { ModuleCategoryPopupComponent } from '../module-category-popup/module-category-popup.component';

@Component({
  selector: 'app-module-category-list',
  templateUrl: './module-category-list.component.html',
})
export class ModuleCategoryListComponent implements OnInit {

  private subscription: Subscription;
  public perPage = 20;
  private isCreate;
  public searchForm: FormGroup;
  public dataList: Array<any> = [];
  public pagination: any;
  constructor(
    private userPermissionsService: UserPermissionsService,
    private _Service: Service,
    private func: Functions,
    private swalService: SwalService,
    private modalService: BsModalService,
    private toastyService: ToastyService
  ) { }

  ngOnInit() {
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-CATEGORY-ISSUE',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.buildSearchForm();
        this.getList();
        this.getModule();
      }
    );
  }
  getList(params: ParamsGetData = { page: 1 }) {
    this.subscription = this._Service
      .get({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.dataList = reponse.data;
        this.initPagination(reponse);
      });
  }
  public ListModule: Array<any> = [];
  public getModule(name = '') {
    let params = `?name= ${name}&limit=20`;
    this._Service.getModule(params).subscribe(data => {
      this.ListModule = data['data'];
      console.log('moudle', data['data'])
    });
  }
  selectModules($event) {
    this.searchForm.patchValue({
      module_id: $event.id
    });
  }
  buildSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      module_name: new FormControl(''),
      is_active: new FormControl(''),
      name: new FormControl('')
    });
  }
  openPopUpCreate() {
    this.isCreate = true;
    const moduleCategoryPopupComponent = this.modalService.show(ModuleCategoryPopupComponent, {
      backdrop: 'static'
    });
    const subscriber = moduleCategoryPopupComponent.content.dataValueChange.subscribe(data => {
      this.saveData(data);
      subscriber.unsubscribe();
    });
  }
  openPopUpView(data) {
    this.modalService.show(ModuleCategoryPopupComponent, {
      backdrop: 'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }
  openPopUpEdit(data) {
    this.isCreate = false;
    const moduleCategoryPopupComponent = this.modalService.show(ModuleCategoryPopupComponent, {
      backdrop: 'static',
      initialState: {
        data,
        disabled: false
      }
    });
    const subscriber = moduleCategoryPopupComponent.content.dataValueChange.subscribe(data => {
      this.saveData(data);
      subscriber.unsubscribe();
    });
  }
  saveData(data) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this._Service.create(data);
    } else {
      observerChangeData = this._Service.update(data.id, data);
    }
    observerChangeData.subscribe(reponse => {
      this.toastyService.success(
        `${this.isCreate ? 'Create' : 'Update'} success`
      );
      this.pageChange(this.pagination['current_page']);
    });
  }
  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getList({ ...valuesSearch, page: pageNumber });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }
  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this._Service.delete(id).subscribe(reponse => {
        this.toastyService.success(`Delete success`);
        this.pageChange(this.pagination['current_page']);
      });
    });
  }

  public search() {
    const valuesSearch = this.searchForm.value;
    this.getList(valuesSearch);
  }

  public reset() {
    this.searchForm.patchValue({
      code: '',
      name: '',
      module_name: '',
      is_active: ''
    });
    this.getList();
  }
}
