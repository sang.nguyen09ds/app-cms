import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule, BsModalRef } from 'ngx-bootstrap';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { moduleCategoryRoutes } from './module-category.routes';
import { Service } from './service';
import { ModuleCategoryComponent } from './module-category.component';
import { ModuleCategoryListComponent } from './module-category-list/module-category-list.component';
import { ModuleCategoryPopupComponent } from './module-category-popup/module-category-popup.component';

@NgModule({
  imports: [
    RouterModule.forChild(moduleCategoryRoutes),
    SharedModule,
    ModalModule.forRoot(),
    DirectiveModule,
  ],
  providers: [Service,BsModalRef],
  declarations: [ModuleCategoryComponent,ModuleCategoryListComponent,ModuleCategoryPopupComponent]
})
export class ModuleCategoryModule { }
