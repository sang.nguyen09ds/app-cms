import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {

  constructor(private http: HttpClient) {}

  public get(params: any) {
    return this.http.get(this.http.apiCommon + '/issue', {params});
  }
  public getissueMin(params: any) {
    return this.http.get(this.http.apiCommon + '/issueMine', {params});
  }
  public getIssueID(id) {
    return this.http.get(this.http.apiCommon + '/issue/' + id);
  }
  public create(dept: any) {
    return this.http.post(this.http.apiCommon + '/issue', dept);
  }

  public update(id: any ,data: any) {
    return this.http.put(this.http.apiCommon + '/issue/' + id ,data);
  }

  public delete(id: any) {
    return this.http.delete(this.http.apiCommon + '/issue/' + id);
  }
  public getIssue(param ){
    return this.http.get(this.http.apiCommon + '/issue' + param);
  }
  public getModuleCategory(param ){
    return this.http.get(this.http.apiCommon + '/module-category' + param);
  }
  public getModule(param ){
    return this.http.get(this.http.apiCommon + '/module' + param);
  }
  public getUser(param ){
    return this.http.get(this.http.apiCommon + '/users' + param);
  }
  public getListComment(issueId){
    return this.http.get(this.http.apiCommon + '/issues/'+ issueId + '/discuss');
  }
  public getListCommentID(param){
    return this.http.get(this.http.apiCommon + '/discuss'+param);
  }
  public createComment(data){
    return this.http.post(this.http.apiCommon + '/discuss',data);
  }
  public deleteComment(id){
    return this.http.delete(this.http.apiCommon + '/discuss/' + id);
  }
  public getProfileUserID(id){
    return this.http.get(this.http.apiCommon + '/users/profile/' + id)
  }
}
export interface ParamsGetData {
  page?: number;
  limit?: number;
  code?: string;
  name?: string;
  
}
