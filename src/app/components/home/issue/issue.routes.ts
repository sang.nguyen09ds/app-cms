import { Routes } from '@angular/router';
import { IssueComponent } from './issue.component';
import { ListIssueComponent } from './list-issue/list-issue.component';
import { CruIssueComponent } from './cru-issue/cru-issue.component';
import { ListIssueMineComponent } from './list-issue-mine/list-issue-mine.component';
import { CruIssueMineComponent } from './cru-issue-mine/cru-issue-mine.component';


export const issueRoutes: Routes = [
    {
        path: '',
        component: IssueComponent,
        data: {name: 'Công Việc'},
        children: [
          
            { path: '', component: ListIssueComponent, data: { name: 'Danh sách công việc' } },
            { path: 'mine', component: ListIssueMineComponent, data: { name: 'Danh sách công việc Của tôi ' } },
            { path: 'mine/:id/:edit', component: CruIssueMineComponent, data: { name: 'Chỉnh sửa công việc Của tôi ', action: 'edit' } },
            { path: 'mine/:id/:view', component: CruIssueMineComponent, data: { name: 'Xem công việc Của tôi ', action: 'view' } },
            { path: 'new', component: CruIssueComponent, data: { name: 'Tạo công việc', action: 'new' } },
            { path: ':id/:edit', component: CruIssueComponent, data: { name: 'Chỉnh sửa công việc', action: 'edit' } },
            { path: ':id', component: CruIssueComponent, data: { name: 'Chi tiết công việc', action: 'view' } }
        ]
    }
];
