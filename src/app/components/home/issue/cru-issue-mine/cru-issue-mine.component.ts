import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserPermissionsService, SwalService, Functions } from 'src/app/shared/shared.module';
import { Service } from '../service';
import { ToastyService } from 'ng2-toasty';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cru-issue-mine',
  templateUrl: './cru-issue-mine.component.html',
})
export class CruIssueMineComponent implements OnInit {
  public form: FormGroup;
  public formComent: FormGroup;
  public issueId = '';
  public role_code = 'ADMIN';
  public action: string;
  public isView = false;
  public departments: any[] = [];
  public submitted = false;
  disabled = false;
  constructor(

    // private userPermissionsService: UserPermissionsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastyService: ToastyService,
    private _service: Service,
    private swalService: SwalService,
  ) { }

  ngOnInit(): void {
    this.buildForm();
    this.activatedRoute.params.subscribe(params => {
      this.issueId = params['id'];
      if (this.issueId) {
        this.action = params['edit'] === 'edit' ? 'edit' : 'view';
        if (this.action === 'view') {
          this.disabled = true;
        }
        this.fillForm();
      } else {
        this.action = 'new';
      }
    });
    this.getModule();
    this.getModuleCategory();
    this.getUsers();
    this.CommentForm();
    this.getComment(this.issueId);

  }
  private fillForm(): void {
    if (!this.issueId) {
      return;
    }
    this._service.getIssueID(this.issueId).subscribe(
      (res: any) => {
        const issue = res['data'];
        this.form.patchValue({
          user_id: issue.user_id,
          role_code: issue.role_code,
          name: issue.name,
          status: issue.status,
          description: issue.description,
          start_time: issue.start_time,
          deadline: issue.deadline,
          estimated_time: issue.estimated_time,
          module_id: issue.module_id,
          module_name: issue.module_name,
          progress: issue.progress,
          parent_id: issue.parent_id,
          parent_name: issue.parent_name,
          priority: issue.priority,
          full_name: issue.full_name,
          module_category_id: issue.module_category_id,
          module_category_name: issue.module_category_name,
        });

      },
      err => { }
    );
  }
  public ListModuleCategory: Array<any> = [];
  public getModuleCategory(module_id = '', name = '') {
    let params = `?module_id=${module_id}&name= ${name}&limit=20`;
    this._service.getModuleCategory(params).subscribe(data => {
      this.ListModuleCategory = data['data'];
    });
  }
  selectModuleCategory($event) {
    this.form.patchValue({ module_category_id: $event.id });
  }
  public ListModule: Array<any> = [];
  public getModule(name = '') {
    let params = `?name= ${name}&limit=20`;
    this._service.getModule(params).subscribe(data => {
      this.ListModule = data['data'];
    });
  }
  selectModules($event) {
    this.form.patchValue({
      module_id: $event.id
    });
    this.getModuleCategory($event.id);
  }
  public ListUser: Array<any> = [];
  public getUsers(name = '') {
    let params = `?full_name= ${name}&limit=20`;
    this._service.getUser(params).subscribe(data => {
      this.ListUser = data['data'];
    });
  }
  selectUsers($event) {
    this.form.patchValue({ user_id: $event.id });
  }
  public ListComment: Array<any> = [];
  public getComment(issueId) {
    this._service.getListComment(issueId).subscribe(data => {
      this.ListComment = data['data'];
    });

  }
 replyForComment(item){
  item['isCollapsed'] = !item['isCollapsed'];
  this.formComent.patchValue({parent_id: item.id});
 }
 replyForCommentChild (comment){
  comment['isCollapsed'] = !comment['isCollapsed'];
  this.formComent.patchValue({parent_id: comment.id});
  //console.log('ee','a');
 }
  CreateComment(text = '', parent_id = '') {
    const data = {
      parent_id: parent_id,
      issue_id: this.issueId,
      description: text
    }
    this._service.createComment(data).subscribe(res => {
      this.reset();
      this.getComment(this.issueId);
    });
  }
  public reset() {
    this.formComent.patchValue({
      description: '',
    });
  }

  public update(): void {
    this._service.update(this.issueId, this.form.value).subscribe(res => {
      if (res.status) {
        this.toastyService.success(res.status);
      }
      else {
        this.toastyService.success('Update success');
      }
      this.router.navigate(['/issues/min']);
    });
  }

  private buildForm(data = {}): void {
    this.form = new FormGroup({
      id: new FormControl({ value: '', disabled: this.isView }),
      user_id: new FormControl({ value: '', disabled: this.isView }),
      name: new FormControl({ value: '', disabled: this.isView }, Validators.required),
      status: new FormControl({ value: '', disabled: this.isView }),
      description: new FormControl({ value: '', disabled: this.isView }),
      deadline: new FormControl({ value: '', disabled: this.isView }),
      start_time: new FormControl({ value: '', disabled: this.isView }),
      estimated_time: new FormControl({ value: '', disabled: this.isView }),
      module_id: new FormControl({ value: '', disabled: this.isView }),
      module_name: new FormControl({ value: '', disabled: this.isView }),
      progress: new FormControl({ value: '', disabled: this.isView }),
      parent_id: new FormControl({ value: '', disabled: this.isView }),
      parent_name: new FormControl({ value: '', disabled: this.isView }),
      priority: new FormControl({ value: '', disabled: this.isView }),
      full_name: new FormControl({ value: '', disabled: this.isView }),
      module_category_id: new FormControl({ value: '', disabled: this.isView }),
      module_category_name: new FormControl({ value: '', disabled: this.isView }),
    });
  }
  private CommentForm(data = {}): void {

    this.formComent = new FormGroup({
      issue_id: new FormControl(this.issueId),
      description: new FormControl(''),
      is_active: new FormControl('1'),
      parent_id: new FormControl('')
    });

  }
}
