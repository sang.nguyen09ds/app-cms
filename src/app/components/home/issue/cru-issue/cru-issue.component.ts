import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastyService } from 'ng2-toasty';
import { SwalService, UserPermissionsService } from 'src/app/shared/shared.module';
import { HomeMessages } from '../../home-messages';
import { Ng2FrameworkFactory, BaseComponentFactory } from 'ag-grid-angular';
import { Service } from '../service';

@Component({
  selector: 'app-cru-issue',
  providers: [Ng2FrameworkFactory, BaseComponentFactory, Service, HomeMessages],
  templateUrl: './cru-issue.component.html',
})
export class CruIssueComponent implements OnInit {
  public form: FormGroup;
  public formComent: FormGroup;
  public issueId = '';
  public role_code = 'ADMIN';
  public action: string;
  public isView = false;
  public departments: any[] = [];
  public submitted = false;
  disabled = false;
  image_action = 'add_thumbnail';
  Tooltip: string = 'Chỉnh sửa hoặc xóa bình luận này';
  constructor(
    private userPermissionsService: UserPermissionsService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastyService: ToastyService,
    private _service: Service,
    private swalService: SwalService,
  ) { }

  public ngOnInit(): void {
    this.buildForm();
    this.setPermissions();
    this.activatedRoute.params.subscribe(params => {
      this.issueId = params['id'];
      if (this.issueId) {
        this.action = params['edit'] === 'edit' ? 'edit' : 'view';
        if (this.action === 'view') {
          this.disabled =true;
        }
        this.fillForm();
        this.CommentForm();
        this.getListComment(this.issueId);
      } else {
        this.action = 'new';
      }
    });
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-ISSUE',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        if(this.action ==='edit'){
          if(data['role_code']!='ADMIN'){
            this.disabled=true;
          }
        }
        this.getUsers();
        this.getModule();
        this.getModuleCategory();
        this.getIssue();
      }
    );
  }

  private buildForm(data = {}): void {
    this.form = new FormGroup({
      id: new FormControl({ value: '', disabled: this.isView }),
      user_id: new FormControl({ value: '', disabled: this.isView }),
      name: new FormControl({ value: '', disabled: this.isView }, Validators.required),
      status: new FormControl({ value: '', disabled: this.isView }),
      description: new FormControl({ value: '', disabled: this.isView }),
      deadline: new FormControl({ value: '', disabled: this.isView }),
      start_time: new FormControl({ value: '', disabled: this.isView }),
      estimated_time: new FormControl({ value: '', disabled: this.isView }),
      module_id: new FormControl({ value: '', disabled: this.isView }),
      module_name: new FormControl({ value: '', disabled: this.isView }),
      progress: new FormControl({ value: '', disabled: this.isView }),
      parent_id: new FormControl({ value: '', disabled: this.isView }),
      parent_name: new FormControl({ value: '', disabled: this.isView }),
      priority: new FormControl({ value: '', disabled: this.isView }),
      full_name: new FormControl({ value: '', disabled: this.isView }),
      module_category_id: new FormControl({ value: '', disabled: this.isView }),
      module_category_name: new FormControl({ value: '', disabled: this.isView }),
    });
  }
  private CommentForm() {
    this.formComent = new FormGroup({
        issue_id: new FormControl(this.issueId),
        description: new FormControl(''),
        is_active: new FormControl('1'),
        parent_id: new FormControl('')
    });
  }
  public ListComment = [];
  public getListComment(issueId){
    this._service.getListComment(issueId).subscribe(data => {
      this.ListComment =  data['data'];
    });
   
  }
  public ListModule: Array<any> = [];
  public getModule(name = '') {
    let params = `?name= ${name}&limit=20`;
    this._service.getModule(params).subscribe(data => {
      this.ListModule = data['data'];
    });
  }
  selectModules($event) {
    this.form.patchValue({
      module_id: $event.id,
    });
    this.getModuleCategory($event.id);
  }
  public ListIssue: Array<any> = [];
  public getIssue(name = '') {
    let params = `?name= ${name}&limit=20`;
    this._service.getIssue(params).subscribe(data => {
      this.ListIssue = data['data'];
    });
  }
  selectIssue($event) {
    this.form.patchValue({
      parent_id: $event.id,
    });
  }
  public ListModuleCategory: Array<any> = [];
  public getModuleCategory(module_id = '', name = '') {
    let params = `?module_id=${module_id}&name= ${name}&limit=20`;
    this._service.getModuleCategory(params).subscribe(data => {
      this.ListModuleCategory = data['data'];
    });
  }
  selectModuleCategory($event) {
    this.form.patchValue({ module_category_id: $event.id });
  }

  public ListUser: Array<any> = [];
  public getUsers(name = '') {
    let params = `?full_name= ${name}&limit=20`;
    this._service.getUser(params).subscribe(data => {
      this.ListUser = data['data'];
    });
  }
  selectUsers($event) {
    this.form.patchValue({ user_id: $event.id });
  }
  private fillForm(): void {
    if (!this.issueId) {
      return;
    }
    this._service.getIssueID(this.issueId).subscribe(
      (res: any) => {
        const issue = res['data'];
          this.form.patchValue({
          user_id: issue.user_id,
          role_code:issue.role_code,
          name: issue.name,
          status: issue.status,
          description: issue.description,
          start_time: issue.start_time,
          deadline: issue.deadline,
          estimated_time: issue.estimated_time,
          module_id: issue.module_id,
          module_name: issue.module_name,
          progress: issue.progress,
          parent_id: issue.parent_id,
          parent_name: issue.parent_name,
          priority: issue.priority,
          full_name: issue.full_name,
          module_category_id: issue.module_category_id,
          module_category_name: issue.module_category_name,
        });

      },
      err => { }
    );
  }

  public onSubmit(): void {
    this.submitted = true;
    this.issueId ? this.update() : this.create();
  }

  public reset() {
    this.formComent.patchValue({
      description:'',
    });
  }
 public CreateComment(text = '', parent_id = ''){
  const data = {
      parent_id :parent_id,
      issue_id: this.issueId,
      description: text
  }
  this._service.createComment(data).subscribe(res => {
    this.reset();
    this.getListComment(this.issueId);
  });
 }



 replyForComment(item){
  item['isCollapsed'] = !item['isCollapsed'];
  this.formComent.patchValue({parent_id: item.id})
 }
  public create(): void {
    // TODO: Wait integrate with backend to get correct link.
    this._service.create(this.form.value).subscribe(res => {
      if (res.status) {
        this.toastyService.success(res.status);
      }
      else {
        this.toastyService.success('Create success');
      }
      this.router.navigate(['/issues']);
    });
  }

  public update(): void {
    this._service.update(this.issueId, this.form.value).subscribe(res => {
      if (res.status) {
        this.toastyService.success(res.status);
      }
      else {
        this.toastyService.success('Update success');
      }
      this.router.navigate(['/issues']);
    });
  }
  public deleteComment(id): void {
    this._service.deleteComment(id).subscribe(res => {
      this.getListComment(this.issueId);
    });
  }
  selectedImage($event) {

    switch (this.image_action) {
      case 'add_thumbnail':
        this.addThumbnail($event);
        break;
      default:
        break;
    }
  }

  addThumbnail(thumbnail) {
    this.form.patchValue({
      thumbnail_id: thumbnail.id,
      thumbnail: thumbnail.thumbnail
    });
  }

  get thumbnail() {
    if (this.form.value.thumbnail) {
      return this.form.value.thumbnail.replace(/ /g, '%20');
    }
    return null;
  }
  public cancel(): void {
    this.swalService.confirm('Are you sure to cancel?', () => {
      this.router.navigate(['/issues']);
    });
  }
}
