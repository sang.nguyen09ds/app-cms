import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { Service } from '../service';
import { SwalService, Functions } from 'src/app/shared/shared.module';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-list-issue-mine',
  templateUrl: './list-issue-mine.component.html',
})
export class ListIssueMineComponent implements OnInit {

  public dataList: Array<any> = [];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;
  issueAction: any;
  constructor(
    private _Service: Service,
    private swalService: SwalService,
    private func: Functions,
    private toastyService: ToastyService
  ) { }

  ngOnInit() {
    this.buildSearchForm();
    this.getList();
    this.getModule();
    this.getModuleCategory();
    this.getUsers();
  }
  getList(params = { page: 1 }) {
    this.subscription = this._Service
      .getissueMin({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.dataList = reponse.data;
        this.initPagination(reponse);
      });
}
public ListModuleCategory: Array<any> = [];
public getModuleCategory(module_id ='',name = ''){
  let params = `?module_id=${module_id}&name= ${name}&limit=20`;
  this._Service.getModuleCategory(params).subscribe(data =>{
      this.ListModuleCategory = data['data'];
  });
}
selectModuleCategory($event){
  this.searchForm.patchValue({module_category_id : $event.id});
}
public ListModule: Array<any> = [];
public getModule(name = ''){
  let params = `?name= ${name}&limit=20`;
  this._Service.getModule(params).subscribe(data =>{
      this.ListModule = data['data'];
  });
}
selectModules($event){
  this.searchForm.patchValue({
    module_id : $event.id
  });
  this.getModuleCategory($event.id);
}
public ListUser: Array<any> = [];
public getUsers(name = ''){
  let params =  `?full_name= ${name}&limit=20`;
  this._Service.getUser(params).subscribe(data =>{
      this.ListUser = data['data'];
  });
}
selectUsers($event){
  this.searchForm.patchValue({user_id : $event.id});
}
public delete(id: any) {
  this.swalService.confirm('Are you sure to delete this item ?', () => {
    this._Service.delete(id).subscribe(reponse => {
      //this.toastyService.success(`Delete success`);
      this.pageChange(this.pagination['current_page']);
    });
  });
}
public search() {
  const valuesSearch = this.searchForm.value;
  this.getList(valuesSearch);
}

public reset() {
  this.searchForm.patchValue({
    full_name: '',
    name: '',
    module_name:'',
    module_category_name:'',
    priority:'',
    status: ''
});
this.getList();
}

buildSearchForm() {
  this.searchForm = new FormGroup({
    full_name: new FormControl(''),
    name: new FormControl(''),
    module_name: new FormControl(''),
    priority: new FormControl(''),
    module_category_name: new FormControl(''),
    status: new FormControl('')
  });
}

public pageChange(pageNumber) {
  const valuesSearch = this.searchForm.value;
  this.getList({ ...valuesSearch, page: pageNumber });
}
public onPageSizeChanged($event) {
  this.perPage = $event.target.value;
  if (
    (this.pagination['current_page'] - 1) * this.perPage >=
    this.pagination['total']
  ) {
    this.pagination['current_page'] = 1;
  }
  this.pageChange(this.pagination['current_page']);
}

private initPagination(data) {
  const { meta } = data;
  if (meta && meta['pagination']) {
    this.pagination = meta['pagination'];
  } else {
    const { perPage, currentPage, totalCount } = meta;
    const count =
      perPage * currentPage > totalCount
        ? perPage * currentPage - totalCount
        : perPage;
    this.pagination = {
      total: totalCount,
      count: count,
      per_page: perPage,
      current_page: currentPage,
      total_pages: meta['pageCount'],
      links: {
        next: data['link']['next']
      }
    };
  }
  this.pagination['numLinks'] = 3;
  this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
}

}
