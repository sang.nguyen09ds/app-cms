import { NgModule } from '@angular/core';
import { BsModalRef, ModalModule, TabsModule, CollapseModule, BsDatepickerModule, BsDropdownModule,TooltipModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { issueRoutes } from './issue.routes';
import { ListIssueComponent } from './list-issue/list-issue.component';
import { IssueComponent } from './issue.component';
import { CruIssueComponent } from './cru-issue/cru-issue.component';
import { Service } from './service';
import { CKEditorModule } from 'ngx-ckeditor';
import { ListIssueMineComponent } from './list-issue-mine/list-issue-mine.component';
import { CruIssueMineComponent } from './cru-issue-mine/cru-issue-mine.component';

@NgModule({
  imports: [
    RouterModule.forChild(issueRoutes),
    SharedModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DirectiveModule,
    CKEditorModule,
    TabsModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot()
  ],
  providers: [BsModalRef,Service],
  declarations: [ListIssueComponent, IssueComponent,CruIssueComponent, ListIssueMineComponent, CruIssueMineComponent]
})
export class IssueModule { }
