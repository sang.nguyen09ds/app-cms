import { Component } from '@angular/core';
import { UserPermissionsService, Functions } from 'src/app/shared/shared.module';
import { Service } from './dashboard.service';
import { single ,multi} from './data';
import { ToastyService } from 'ng2-toasty';
@Component({
  providers: [Service],
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html',
})
export class DashboardComponent {
  single: any[];
  multi: any[];

  view: any[] = [700, 300];
  autoScale = true;
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['rgb(233, 148, 80)', 'rgb(191, 157, 118)', 'rgb(216, 159, 89)', 'rgb(242, 223, 167)','rgb(165, 215, 198)']
  };
  public selectItem: any;
  public userPermission = {};
  public perPage = 20;
  public pagination: any;
  constructor(
    private readonly userPermissionsService: UserPermissionsService,
    private service: Service,
    private toastyService: ToastyService,
    private func: Functions) {
    Object.assign(this, { single,multi });
  }


  ngOnInit() {
   this.setPermissions();

  }
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-DASHBOARD',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.getStatistic();
        this.getIssue();
        this.getModuleStatus();
        this.SelectModuleCategory(2);
        this.getIssueCompleted();
        this.getDaily();
      }
    );
  }

  public getStatistics: any = [];
  public statistic: any = [];
  getStatistic() {
    this.statistic = this.getStatistics;
    this.service.getStatistics().subscribe((data: any) => {
      this.statistic = data['data'];
    });
  }
  public Issue: any = [];
  public getIssue(pageNum: number = 1) {
    this.service.getIssue('?limit=11&sort[id]=desc').subscribe((data: any) => {
      this.Issue = data['data'];
    });
  }
  public ModuleStatusList: Array<any> = [];
  public getModuleStatus() {
    this.service.getModuleStatus().subscribe((data: any) => {
      this.ModuleStatusList = data['data'];
    });
  }
  public onChange($event) {
    this.SelectModuleCategory($event.target.value);
  }
  public ModuleCategory: Array<any> = [];
  public SelectModuleCategory(id) {
    this.service.getModuleCategoryStatus(id, '?limit=5&sort[id]=desc').subscribe((data: any) => {
      if(data['data'] == ''){
          this.toastyService.error("Dữ liệu rỗng");
      }
      else if(data['data'] !== ''){
        this.ModuleCategory = data['data'];
    }
    });
  }
  public IssueCompleted: any = [];
  public getIssueCompleted(pageNum: number = 1) {
    this.service.getIssueCompleted('?limit=5&sort[id]=desc').subscribe((data: any) => {
      this.IssueCompleted = data['data'];
    });
  }
  public DailyList: any = [];
  public getDaily(pageNum: number = 1) {
    this.service.getDaily('?limit=5&sort[id]=desc').subscribe((data: any) => {
      this.DailyList = data['data'];
    });
  }
  onSelect(event) {
    console.log(event);
  }

}
