import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { dashboardRoutes } from './dashboard.routes';
import { BsDropdownModule } from 'ngx-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
@NgModule({
  imports: [SharedModule, RouterModule.forChild(dashboardRoutes),BsDropdownModule.forRoot(),NgxChartsModule,DirectiveModule],
  declarations: [DashboardComponent]
})
export class DashboardModule {}