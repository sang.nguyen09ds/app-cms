export var single = [
  {
    "name": "Germany",
    "value": 40632
  },
  {
    "name": "United States",
    "value": 49737
  },
  {
    "name": "France",
    "value": 36745
  },
  {
    "name": "United Kingdom",
    "value": 36240
  },
  {
    "name": "Spain",
    "value": 33000
  },
  {
    "name": "Italy",
    "value": 35800
  }
];
export var multi = [
  {
    "name": "Uzbekistan",
    "series": [
      {
        "value": 4991,
        "name": "2016-09-18T15:37:45.070Z"
      },
      {
        "value": 6942,
        "name": "2016-09-15T01:34:43.439Z"
      },
      {
        "value": 2071,
        "name": "2016-09-15T22:27:42.507Z"
      },
      {
        "value": 4085,
        "name": "2016-09-24T00:20:30.466Z"
      },
      {
        "value": 2386,
        "name": "2016-09-13T21:59:41.199Z"
      }
    ]
  },
  {
    "name": "Puerto Rico",
    "series": [
      {
        "value": 3679,
        "name": "2016-09-18T15:37:45.070Z"
      },
      {
        "value": 6831,
        "name": "2016-09-15T01:34:43.439Z"
      },
      {
        "value": 5968,
        "name": "2016-09-15T22:27:42.507Z"
      },
      {
        "value": 5759,
        "name": "2016-09-24T00:20:30.466Z"
      },
      {
        "value": 5819,
        "name": "2016-09-13T21:59:41.199Z"
      }
    ]
  },
  {
    "name": "Comoros",
    "series": [
      {
        "value": 2781,
        "name": "2016-09-18T15:37:45.070Z"
      },
      {
        "value": 6134,
        "name": "2016-09-15T01:34:43.439Z"
      },
      {
        "value": 5122,
        "name": "2016-09-15T22:27:42.507Z"
      },
      {
        "value": 3348,
        "name": "2016-09-24T00:20:30.466Z"
      },
      {
        "value": 3267,
        "name": "2016-09-13T21:59:41.199Z"
      }
    ]
  },
  {
    "name": "Greece",
    "series": [
      {
        "value": 3566,
        "name": "2016-09-18T15:37:45.070Z"
      },
      {
        "value": 4626,
        "name": "2016-09-15T01:34:43.439Z"
      },
      {
        "value": 2252,
        "name": "2016-09-15T22:27:42.507Z"
      },
      {
        "value": 3883,
        "name": "2016-09-24T00:20:30.466Z"
      },
      {
        "value": 3390,
        "name": "2016-09-13T21:59:41.199Z"
      }
    ]
  },
  {
    "name": "Aruba",
    "series": [
      {
        "value": 3021,
        "name": "2016-09-18T15:37:45.070Z"
      },
      {
        "value": 5632,
        "name": "2016-09-15T01:34:43.439Z"
      },
      {
        "value": 5310,
        "name": "2016-09-15T22:27:42.507Z"
      },
      {
        "value": 2384,
        "name": "2016-09-24T00:20:30.466Z"
      },
      {
        "value": 5006,
        "name": "2016-09-13T21:59:41.199Z"
      }
    ]
  }
];
