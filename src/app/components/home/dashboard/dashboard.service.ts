import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {
    constructor(private http: HttpClient) {
    }

    public getStatistics(){
        return this.http.get(this.http.apiMaster + '/statistic');
    }
    public getIssue(param){
        return this.http.get(this.http.apiMaster + '/issue'+ param);
    }
    public getModuleStatus(){
        return this.http.get(this.http.apiMaster + '/module');
    }
    public getModuleCategoryStatus(id: any,param){
        return this.http.get(this.http.apiMaster + '/report-module-status/' + id + param);
    }
    public getIssueCompleted(param){
        return this.http.get(this.http.apiMaster + '/report-issue/completed'+ param);
    }
    public getDaily(param){
        return this.http.get(this.http.apiMaster + '/report-module-status-userDaily'+ param);
    }
   
}
