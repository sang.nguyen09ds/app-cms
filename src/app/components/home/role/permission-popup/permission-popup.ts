import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { Functions, HttpClient } from 'src/app/shared/shared.module';
import { PermissionsGroup } from '../permission-group/permission-group';
import { Service } from '../services/services';
declare const $: any;
@Component({
  selector: 'permission-popup',
  templateUrl: 'permission-popup.html'
})
export class PermissionsPopup {
  @Input('GroupData') set GroupData(value: any) {
    if (value) {
      this._GroupData = value;
    }
  }
  private isCreate;
  public disabled: boolean;
  public _roleParentData = {};
  public Form: FormGroup;
  private _GroupData = {};
  popupTile: 'Permissions Popup';
  labelButtonSave;
  public selectGroupPermisison = {};
  dataValueChange: Subject<Object>;
  private data;
  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public __service: Service,
    private _func: Functions,
    private store: Store<any>,
    private modalService: BsModalService,
    public toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) {}
  ngOnInit() {
    this.buildForm(this.data);
   this.getPermissionsGroupList();
    this.dataValueChange = new Subject();
  }
  buildForm(data = { is_active: '1' }) {
    this.labelButtonSave = data['permission_id'] ? 'Update' : 'Create';
    const is_active = data['is_active'] == '1' ? true : false;
    this.Form = this.formBuilder.group({
      permission_id: [data['permission_id']],
      code: [data['permission_code'], Validators.required],
      name: [data['permission_name'], Validators.required],
      description: data['permission_description'],
      group_id: data['permission_group_id'],
      group_name: data['permission_group_name'],
      group_code: data['permission_group_code'],
      is_active: [is_active]
    });
  }
  ListPermissionGroup: Array<any> = [];
  getPermissionsGroupList($event = '') {
    let param = `?limit=20&name=` + $event;
    this.__service.getPermissionGroup(param).subscribe(data => {
      this.ListPermissionGroup = data['data'];
      if(this.data)
      {
        this.selectGroupPermisison = this.ListPermissionGroup.find(
          item => item.code == this.data.permission_group_code
        );
      }
     
    });
  }

  openPopUpCreateGroup() {
    this.isCreate = true;
    const permissionsGroup = this.modalService.show(PermissionsGroup,{ backdrop:'static'});
    const subscriber = permissionsGroup.content.dataValueChange.subscribe(
      data => {
        this.saveData(data);
        subscriber.unsubscribe();
      }
    );
  }
  openPopUpView(data) {
    this.modalService.show(PermissionsGroup, {
      backdrop:'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }
  openGroupPopUpEdit(data) {
    this.isCreate = false;
    console.log(data);
    const permissionsGroup = this.modalService.show(PermissionsGroup, {
      backdrop:'static',
      initialState: {
        data,
        disabled: false
      }
    });
    const subscriber = permissionsGroup.content.dataValueChange.subscribe(
      data => {
        this.saveData(data);
        subscriber.unsubscribe();
      }
    );

    const subscriberDeleteItem = permissionsGroup.content.dataValueDelete.subscribe(
      data => {
        //delete item data co id nay trong this.ListPermissionGroup
        //con ko don gian reload goi lai api lay danh sach moi
        this.Form.patchValue({
          group_name: '',
          group_code: ''
        });
      }
    );
  }
  saveData(data) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this.__service.addPermissionGroup(data);
    } else {
      observerChangeData = this.__service.updatePermissionGroup(
        data.id,
        data
      );
    }
    observerChangeData.subscribe(reponse => {
      if(reponse.status)
      {
        this.toastyService.success(reponse.status);
      }
      else
      {
        this.toastyService.success(
          `${this.isCreate ? 'Create' : 'Update'} success`
        );
      }
      if (!this.isCreate) {
        this.Form.patchValue({
          group_name: data.name,
          group_code: data.code
        });
      }
      this.getPermissionsGroupList();
    });
  }
  public save() {
    const permission = this.Form.value;

    this.dataValueChange.next({
      ...permission,
      is_active: permission['is_active'] ? '1' : '0'
    });
    
  }
  close() {
    this.bsModalRef.hide();
  }
  _selectGroupPermisison($event) {
    let group = {
      group_id: $event.id,
      group_name: $event.name,
      group_code: $event.code
    };
    this.Form.patchValue(group);
  }
}
