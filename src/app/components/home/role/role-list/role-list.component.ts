import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { Subscription, config } from 'rxjs';

import { RolePopupComponent } from '../role-popup/role-popup.component';
import { Service } from '../services/services';
import { SwalService, Functions, HttpClient, UserPermissionsService } from 'src/app/shared/shared.module';
import { BsModalService, ModalOptions } from 'ngx-bootstrap';

declare const $: any;
@Component({
  selector: 'role-list',
  templateUrl: 'role-list.html'
})
export class RoleListComponent implements OnInit {
  RoleList: Array<any> = [];
  public data: Array<any> = [];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;
  selectedRole = [];
  private ListPermission: Array<any> = [];
  constructor(
    private userPermissionsService: UserPermissionsService,
    private _service: Service,
    private swalService: SwalService,
    private func: Functions,
    private toastyService: ToastyService,
    private modalService: BsModalService,
  ) {}

  ngOnInit() {
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-ROLE',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        console.log('zz00', data['permissions']);
        this.buildSearchForm();
        this.getList();
      }
    );
  }
  getList(params = { page: 1 }): void {
    this.subscription = this._service
      .getRole({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.RoleList = reponse.data;
        console.log('zzzz',reponse.data);
        this.initPagination(reponse);
      });
  }
  buildSearchForm() {
    this.searchForm = new FormGroup({
      code: new FormControl(''),
      name: new FormControl('')
    });
  }

  openPopUpCreate() {
    this.isCreate = true;
    const rolePopupComponent = this.modalService.show(RolePopupComponent, {
      backdrop:'static'
    });
    const subscriber = rolePopupComponent.content.dataValueChange.subscribe(data => {
      this.saveData(data);
      subscriber.unsubscribe();
    });
  }
  openPopUpView(data) {
    this.modalService.show(RolePopupComponent, {
      backdrop:'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }
  openPopUpEdit(data) {
    this.isCreate = false;
    const rolePopupComponent = this.modalService.show(RolePopupComponent, {
      backdrop:'static',
      initialState: {
        data,
        disabled: false
      }
    });
    const subscriber = rolePopupComponent.content.dataValueChange.subscribe(data => {
      this.saveData(data);
      subscriber.unsubscribe();
    });
  }
  saveData(data) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this._service.addRole(data);
    } else {
      observerChangeData = this._service.updateRole(data.id, data);
    }
    observerChangeData.subscribe(reponse => {
      if(reponse.status)
      {
        this.toastyService.success(reponse.status);
      }
      else
      {
        this.toastyService.success(
          `${this.isCreate ? 'Create' : 'Update'} success`
        );
      }
      this.pageChange(this.pagination['current_page']);
    });
  }

  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this._service.deleteRole(id).subscribe(reponse => {
        if(reponse.status)
        {
          this.toastyService.success(reponse.status);
        }
        else
        {
          this.toastyService.success(`Delete success`);
        }
        this.pageChange(this.pagination['current_page']);
      });
    });
  }

  public search() {
    const valuesSearch = this.searchForm.value;
    this.getList(valuesSearch);
  }

  public reset() {
    this.searchForm.reset();
  }

  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getList({ ...valuesSearch, page: pageNumber });
  }

  public initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }
  public filterPermissionByRole(item =[]) {
    this.selectedRole = item;
  }

  public convertPermissisonGroupToArray(
    permisisonList: Array<any> = [],
    data: Array<any> = []
  ) {
    for (let i in permisisonList) {
      if (permisisonList[i]['permissions']) {
        data.push(...permisisonList[i]['permissions']);
        this.convertPermissisonGroupToArray(permisisonList[i]['permissions']);
      }
    }

    return data;
  }

  public countPermission(permisisonList: Array<any> = []) {
    this.ListPermission = this.convertPermissisonGroupToArray(permisisonList);
  }
}
