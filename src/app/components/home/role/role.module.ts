import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, ModalModule, BsModalRef } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { PermissionsGroup } from './permission-group/permission-group';
import { PermissionsPopup } from './permission-popup/permission-popup';
import { Permission } from './permission/permission';
import { RoleListComponent } from './role-list/role-list.component';
import { RolePopupComponent } from './role-popup/role-popup.component';
import { RoleComponent } from './role.component';
import { roleRoutes } from './role.routers';
import { Service } from './services/services';
@NgModule({
  imports: [
    SharedModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    DirectiveModule,
    RouterModule.forChild(roleRoutes)
  ],
  entryComponents: [RolePopupComponent,PermissionsGroup,PermissionsPopup],
  declarations: [
    RoleComponent,
    RoleListComponent,
    RolePopupComponent,
    Permission,
    PermissionsGroup,
    PermissionsPopup
  ],
  providers: [Service,BsModalRef]
})
export class RoleModule {}
