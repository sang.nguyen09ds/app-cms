import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
declare const $: any;
@Component({
  selector: 'app-role-popup',
  templateUrl: './role-popup.component.html',
  styleUrls: ['./role-popup.component.css']
})
export class RolePopupComponent implements OnInit {
  public disabled: boolean;
  public Form: FormGroup;
  popupTile: 'Create Role'
  labelButtonSave;
  private data;
  dataValueChange: Subject<Object>;
  constructor( private formBuilder: FormBuilder,private bsModalRef: BsModalRef,
   
   ) {}

  ngOnInit() {
    this.buildForm(this.data);
    this.dataValueChange = new Subject();
  }
  buildForm(data = { is_active: '1' }) {
    this.labelButtonSave = data['id'] ? 'Update' : 'Create';
    const is_active = data['is_active'] == '1' ? true : false;
    this.Form = this.formBuilder.group({
      id: [data['id']],
      code: [data['code'], Validators.required],
      name: [data['name'], Validators.required],
      role_level: [data['role_level'], Validators.required],
      description: [data['description']],
      is_active: [is_active]
    });
  }
  public save() {
    const role = this.Form.value;

    this.dataValueChange.next({
      ...role,
      is_active: role['is_active'] ? '1' : '0'
    });
    this.close();
  }

  close() {
    this.bsModalRef.hide();
  }

}
