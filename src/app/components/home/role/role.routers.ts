import { Routes } from '@angular/router';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleComponent } from './role.component';

export const roleRoutes: Routes = [
  {
    path: '',
    component: RoleComponent,
    children: [
      { path: '', component: RoleListComponent, data: { name: 'Vai trò người dùng' } }
    ]
  }
];
