import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {
    constructor(private http: HttpClient) {
    }
    public getRole(param) {
        return this.http.get(this.http.apiCommon + '/roles',param);
    }

    public addRole(roleObj: any) {
        return this.http.post(this.http.apiCommon + '/roles', roleObj);
    }

    public updateRole(roleId, roleObj) {
        return this.http.put(this.http.apiCommon + '/roles/' + roleId, roleObj);
    }
  
     public deleteRole(id: any) {
      return this.http.delete(this.http.apiCommon + '/roles/'  + id);
    }
    // Permission
    public getPermission(param) {
      return this.http.get(this.http.apiCommon + '/permissions/groups' + param);
  }
    public getPermissionsByRoleName(roleName: string = '') {
        return this.http.get(this.http.apiCommon + '/permission-groups/' + roleName);
    }
    public addPermission(data) {
      return this.http.post(this.http.apiCommon + '/permissions',data);
  }
    public updatePermission(Id,data) {
      return this.http.put(this.http.apiCommon + `/permissions/` + Id, data);
  }
    public deletePermission(id: any) {
      return this.http.delete(this.http.apiCommon + '/permissions/'  + id);
  }
  // Permission Group
  public getPermissionGroup(param) {
    return this.http.get(this.http.apiCommon + '/permission_groups' + param);
}
  public addPermissionGroup(data) {
    return this.http.post(this.http.apiCommon + '/permission_groups',data);
}
  public updatePermissionGroup(Id,data) {
    return this.http.put(this.http.apiCommon + `/permission_groups/` + Id, data);
}
  public deletePermissionGroup(id: any) {
    return this.http.delete(this.http.apiCommon + '/permission_groups/'  + id);
}
}
