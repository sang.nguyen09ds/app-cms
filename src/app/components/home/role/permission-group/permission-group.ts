import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { SwalService } from 'src/app/shared/shared.module';
import { Service } from '../services/services';
// declare const $: any;
@Component({
  selector: 'permission-group',
  templateUrl: 'permission-group.html'
})
export class PermissionsGroup {
  @Output() GroupData = new EventEmitter<any>();
  private data;
  public disabled: boolean;
  public Form: FormGroup;
  popupTile: 'Permissions Group Popup';
  labelButtonSave;
  dataValueChange: Subject<Object>;
  dataValueDelete: Subject<Object>;
  isCreate:any;
  constructor(
    private swalService: SwalService,
    private formBuilder: FormBuilder,
    public __service: Service,
    private store: Store<any>,
    public toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) {}
  ngOnInit() {
    this.buildForm(this.data);
    this.dataValueChange = new Subject();

    this.dataValueDelete = new Subject();
    this.getPermissionsGroupList();
  }
  buildForm(data = { is_active: '1' }) {
    if (this.Form) {
      this.Form.reset();
    }
    console.log('pergroup_form', data);
    this.labelButtonSave = data['permission_group_id'] ? 'Update' : 'Create';
    const is_active = data['is_active'] == '1' ? true : false;
    this.Form = this.formBuilder.group({
      id: [data['id']],
      code: [data['code'], Validators.required],
      name: [data['name'], Validators.required],
      description: [data['description']],
      is_active: [is_active]
    });
  }

  ListPermissionGroup: Array<any> = [];
  public getPermissionsGroupList() {
    let param = '?limit=20';
    this.__service.getPermission(param).subscribe(data => {
      this.ListPermissionGroup = data['data'];
    });
  }

  public delete(data: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this.__service
        .deletePermissionGroup(data['permission_group_id'])
        .subscribe(reponse => {
          this.toastyService.success(`Delete success`);
          this.GroupData.emit(this.ListPermissionGroup);
          this.dataValueDelete.next(data);

          this.close();
        });
    });
  }
  public save() {
    const group = this.Form.value;

    this.dataValueChange.next({
      ...group,
      is_active: group['is_active'] ? '1' : '0'
    });
    this.close();
  }
  close() {
    this.bsModalRef.hide();
  }
}
