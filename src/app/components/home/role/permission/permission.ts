import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import {
  Functions,
  HttpClient,
  SwalService,
  UserPermissionsService
} from 'src/app/shared/shared.module';
import { PermissionsPopup } from '../permission-popup/permission-popup';
import { Service } from '../services/services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'permission',
  templateUrl: './permission.html'
})
export class Permission {
  @Input('roleParentData') set roleParentData(value: any) {
    if (value) {
      this.SetActivePermission(value);
      this._roleParentData = value;
    }
  }
  @Output() permissionData = new EventEmitter<any>();
  Form: FormGroup;
  public perPage = 20;
  public pagination: any;
  public isCreate;
  public ListPermission: Array<any> = [];
  public popupTile = 'Permission';
  public submitted: boolean = false;
  public _roleParentData = {};
  public ListPermissionGroup: Array<any> = [];
  public currentGroupPermisison = {};
  public openForm: boolean = false;
  permission_key = '';
  constructor(
    private userPermissionsService: UserPermissionsService,
    private swalService: SwalService,
    public _fb: FormBuilder,
    public _func: Functions,
    public http: HttpClient,
    public __service: Service,
    private modalService: BsModalService,
    public toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) {}

  ngOnInit() {
    this.setPermissions();
  }

  public userPermission = {};
    private setPermissions() {
      this.userPermissionsService.getUserPermissions({
        viewPage: 'VIEW-PERMISSION-GROUP',
        deny: true
      }).subscribe(
        (data) => {
          this.userPermission = data['permissions'];
          this.getPermissionsList();
        }
      );
    }
  public getPermissionsList() {
    let param = '?limit=999';
    this.__service.getPermission(param).subscribe(data => {
      this.ListPermission = data['data'];
      console.log(this.ListPermission)
      this.permissionData.emit(this.ListPermission);
      if (this._roleParentData) {
        this.SetActivePermission(this._roleParentData);
      }
    });

    this.permissionData.emit(this.ListPermission);
    if (this._roleParentData) {
      this.SetActivePermission(this._roleParentData);
    }
  }

  public resetPermissionChecked() {
    for (let i in this.ListPermission) {
      let permisison = this.ListPermission[i]['permissions'];
      for (let j in permisison) {
        permisison[j]['selected'] = false;
      }
    }
  }

  public SetActivePermission(data: any) {
    this.resetPermissionChecked();
    for (let i in this.ListPermission) {
      let sum_check = 0;
      this.ListPermission[i]['selected'] = false;
      for (let key in data['permissions']) {
        // sub permisison
        for (let k in this.ListPermission[i]['permissions']) {
          let per = this.ListPermission[i]['permissions'][k];
          if (data['permissions'][key] == per['permission_name']) {
            per['selected'] = true;
            sum_check++;
            continue;
          }
        }
      }
      if (sum_check == this.ListPermission[i]['permissions'].length) {
        this.ListPermission[i]['selected'] = true;
      }
    }
    setTimeout(() => {
    }, 500);
  }

  public checkedPermissions(data: any) {
    setTimeout(() => {
      let sum_check = 0;
      data['permissions'].forEach((item: any) => {
        if (item['selected']) {
          sum_check++;
        }
      });
      if (sum_check == data['permissions'].length) {
        data['selected'] = true;
      } else {
        data['selected'] = false;
      }
    });
  }
  public setActive(data = {}) {
    data['is_active'] = data['is_active'] ? '1' : '0';
  }
  public convertPermissisonGroupToArray(
    permisisonList: Array<any> = [],
    data: Array<any> = []
  ) {
    for (let i in permisisonList) {
      if (permisisonList[i]['permissions']) {
        data.push(...permisisonList[i]['permissions']);
        this.convertPermissisonGroupToArray(permisisonList[i]['permissions']);
      }
    }
    return data;
  }
  public getIdsPermissionSelected() {
    let ids: Array<any> = [];
    let data = this.convertPermissisonGroupToArray(this.ListPermission);
    data.forEach((item: any) => {
      console.log(item);
      if (item['selected']) {
        ids.push(item['permission_id']);
      }
    });
    return ids;
  }
  public addPermissionstoRole() {
    let data = { permission_id: this.getIdsPermissionSelected() };
    this.http
      .put(
        this.http.apiCommon +
          '/roles/' +
          this._roleParentData['id'] +
          '/permissions',
        data
      )
      .subscribe(res => {
        this.permissionData.emit([]);
        setTimeout(() => {
          this.permissionData.emit(this.ListPermission);
        }, 300);
      });
  }

  public setCheckallPermissionGroup(groupItem: Array<any> = [], $event: any) {
    this.setListChecked(groupItem['permissions'], $event);
  }
  public setListChecked(dataList = [], $event: any) {
    dataList.forEach((item: any) => {
      item['selected'] = $event;
    });
  }
  resetGroupValue() {
    this.Form.patchValue({
      permission_group_id: '',
      permission_group_name: ''
    });
  }
  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange($event) {
    const valuesSearch = this.Form.value;
    this.getPermissionsList();
  }
  openPermissionPopUpCreate() {
    this.isCreate = true;
    const permissionsPopUp = this.modalService.show(PermissionsPopup,{ backdrop:'static'});
    const subscriber = permissionsPopUp.content.dataValueChange.subscribe(data => {
      this.saveData(data);
      subscriber.unsubscribe();
    });
  }
  openPopUpView(data) {
    this.modalService.show(PermissionsPopup, {
      backdrop:'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }
  openPermissionPopUpEdit(data) {
    console.log('data',data)
    this.isCreate = false;
    const permissionsPopUp = this.modalService.show(PermissionsPopup, {
      backdrop:'static',
      initialState: {
        data,
        disabled: false
      }
    });
    const subscriber = permissionsPopUp.content.dataValueChange.subscribe(data => {
      this.saveData(data);
      subscriber.unsubscribe();
    });
  }
  saveData(data) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this.__service.addPermission(data);
    } else {
      observerChangeData = this.__service.updatePermission(
        data.permission_id,
        data
      );
    }
    observerChangeData.subscribe(reponse => {
      this.close();
      if(reponse.status)
      {
        this.toastyService.success(reponse.status);
      }
      else
      {
        this.toastyService.success(
          `${this.isCreate ? 'Create' : 'Update'} success`
        );
      }
      this.getPermissionsList();
    }
    
    );
  }
  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this.__service.deletePermission(id).subscribe(reponse => {
        this.toastyService.success(`Delete success`);
        this.getPermissionsList();
      });
    });
  }
  close() {
    this.bsModalRef.hide();
  }

}
