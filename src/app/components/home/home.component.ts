import { AfterViewInit, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SocketProvider } from 'src/app/shared/shared.module';

@Component({
  templateUrl: 'home.html'
})
export class HomeComponent implements AfterViewInit {
  constructor(
    private router: Router,
    private socketProvider: SocketProvider,
    private currentActivatedRoute: ActivatedRoute
  ) {
    router.events.subscribe((e: any) => {
      this.getCurrentCompnonentByRouterActive(this.currentActivatedRoute);
    });
    this.socketProvider.connect().subscribe(msg => {
      console.log('socketProvider.connect', msg);
    });
  }

  ngAfterViewInit() {}

  private getCurrentCompnonentByRouterActive(activeRouter: ActivatedRoute) {
    if (activeRouter.children.length) {
      this.getCurrentCompnonentByRouterActive(activeRouter.children[0]);
    } else {
      const activeComponentName = activeRouter.routeConfig.data['component'];
      localStorage.setItem('activeComponent', activeComponentName);
    }
  }
}
