import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular/main';
import { ToastyModule } from 'ng2-toasty';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './home.component';
import { homeRoutes } from './home.routes';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { ProfileComponent } from './profile/profile.component';
import { BsDatepickerConfig } from 'ngx-bootstrap';


export function getDatepickerConfig(): BsDatepickerConfig {
  return Object.assign(new BsDatepickerConfig(), {
    dateInputFormat: 'DD-MM-YYYY'
  });
}


@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes),
    DirectiveModule,
    ToastyModule.forRoot(),
    AgGridModule.withComponents([]),
    SharedModule,
  ],
  declarations: [HomeComponent, ProfileComponent],
  providers: [{ provide: BsDatepickerConfig, useFactory: getDatepickerConfig }],
  exports: []
})
export class HomeModule {}
