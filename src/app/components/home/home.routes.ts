import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/shared/_guards';
import { HomeComponent } from './home.component';
import { ProfileComponent } from './profile/profile.component';

export const homeRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'role-list',
        loadChildren: './role/role.module#RoleModule'
      },
      {
        path: 'users',
        loadChildren:
          './user-management/user-management.module#UserManagementModule'
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {name: 'Profile'},
      },
      {
        path: 'department',
        loadChildren:
          './department/department.module#DepartmentModule'
      },
      {
        path: 'modules',
        loadChildren:
          './module/module.module#ModuleModule'
      },
      {
        path: 'category-modules',
        loadChildren:
          './module-category/module-category.module#ModuleCategoryModule'
      },
      {
        path: 'issues',
        loadChildren:
          './issue/issue.module#IssueModule'
      },
      {
        path: 'daily',
        loadChildren:
          './report/daily/daily.module#DailyModule'
      },
      {
        path: 'activity',
        loadChildren:
          './activity/activity.module#ActivityModule'
      },
    ],
    canActivate: [AuthGuard]
  }
];
