import { Routes } from '@angular/router';
import { ModuleComponent } from './module.component';
import { ListModuleComponent } from './list-module/list-module.component';
import { ModulePopupComponent } from './module-popup/module-popup.component';

export const moduleRoutes: Routes = [
    {
        path: '',
        component: ModuleComponent,
        data: {name: 'Dự Án'},
        children: [
            { path: '', component: ListModuleComponent, data: { name: 'Danh sách dự án' } },
            { path: 'new', component: ModulePopupComponent, data: { name: 'Tạo dự án', action: 'new' } },
            { path: ':id/:edit', component: ModulePopupComponent, data: { name: 'Chỉnh sửa dự án', action: 'edit' } },
            { path: ':id', component: ModulePopupComponent, data: { name: 'Chi tiết dự án', action: 'view' } }
        ]
    }
];
