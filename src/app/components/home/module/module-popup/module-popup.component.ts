import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { HttpClient, UserPermissionsService } from 'src/app/shared/shared.module';
import { Service } from '../service';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-module-popup',
  templateUrl: './module-popup.component.html',
})
export class ModulePopupComponent implements OnInit {

  public disabled: boolean;
  labelButtonSave;
  public Form: FormGroup;
  popupTile: 'Create Modules'
  private data;
  dataValueChange: Subject<Object>;
  constructor(
    private userPermissionsService: UserPermissionsService,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public __service: Service,
    public toastyService: ToastyService,
    private bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.buildForm(this.data);
   this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-MODULE',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.dataValueChange = new Subject();
      }
    );
  }
  buildForm(data = { is_active: '1' }) {
    this.labelButtonSave = data['id'] ? 'Update' : 'Create';
    const status = data['is_active'] == '1' ? true : false;
    this.Form = this.formBuilder.group({
      id: [data['id']],
      code: [data['code'], Validators.required],
      name: [data['name'], Validators.required],
      description: [data['description']],
      is_active: [{ value: status, disabled: this.disabled }]
    });
  }
  public save() {
    const dept = this.Form.value;

    this.dataValueChange.next({
      ...dept,
      is_active: dept['is_active'] ? '1' : '0'
    });
    this.close();
  }

  close() {
    this.bsModalRef.hide();
  }
}

