import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule, BsModalRef } from 'ngx-bootstrap';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { moduleRoutes } from './module.routes';
import { Service } from './service';
import { ListModuleComponent } from './list-module/list-module.component';
import { ModulePopupComponent } from './module-popup/module-popup.component';
import { ModuleComponent } from './module.component';

@NgModule({
  imports: [
    RouterModule.forChild(moduleRoutes),
    SharedModule,
    ModalModule.forRoot(),
    DirectiveModule,
  ],
  providers: [Service,BsModalRef],
  declarations: [ListModuleComponent,ModuleComponent,ModulePopupComponent]
})
export class ModuleModule { }
