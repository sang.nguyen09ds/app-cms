import { Injectable } from '@angular/core';
@Injectable()
export class HomeMessages {
    public MESSAGES = {
        PERMISSION_DENY: 'Permission deny.',
        SELECT_ITEM: 'Please choose one item.',
        SELECT_ITEM_CONSTRAINT: 'Please choose only one item.',
        RESET_PASS_SUCCESS: 'Reset password of user success.',
        CHANGE_PASS_SUCCESS: 'Change your password success.',
        CREATE_SUCCESS: 'Tạo thành công',
        UPDATE_SUCCESS: 'Update thành công',
        DELETE_SUCCESS: 'Xóa thành công'
    };
}
