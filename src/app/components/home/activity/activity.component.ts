import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserPermissionsService, Functions, SwalService } from 'src/app/shared/shared.module';
import { Service, ParamsGetData } from './Service';
import { BsModalService } from 'ngx-bootstrap';
import { ToastyService } from 'ng2-toasty';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css'],
  providers: [
    Service
  ]
})
export class ActivityComponent implements OnInit {
  private subscription: Subscription;
  public perPage = 20;
  private isCreate;
  public searchForm: FormGroup;
  public dataList: Array<any> = [];
  public pagination: any;

  constructor(
    private userPermissionsService: UserPermissionsService,
    private _Service: Service,
    private func: Functions,
    private swalService: SwalService,
    private modalService: BsModalService,
    private toastyService: ToastyService
  ) { }
  ngOnInit() {
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-LOG-USER',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.getList();
        this.buildSearchForm();
      }
    );
  }
  getList(params: ParamsGetData = { page: 1 }) {
    this.subscription = this._Service
      .get({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.dataList = reponse.data;
        this.initPagination(reponse);
      });
  }
  public buildSearchForm() {
    this.searchForm = new FormGroup({
      user_name: new FormControl('')
    });
  }
  public search() {
    const valuesSearch = this.searchForm.value;
    this.getList(valuesSearch);
  }

  public reset() {
    this.searchForm.patchValue({
      user_name: ''
  });
  this.getList();
  }
  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getList({ ...valuesSearch, page: pageNumber });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }
}
