import { Routes } from '@angular/router';
import { ActivityComponent } from './activity.component';


export const activityRoutes: Routes = [
    {
        path: '',
        component: ActivityComponent,
        data: {name: 'Thống kê hoạt động'},
        children: [
            { path: '', component: ActivityComponent, data: { name: 'Thống kê hoạt động' } },
        ]
    }
];
