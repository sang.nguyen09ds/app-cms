import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {
  constructor(private http: HttpClient) {}

  public get(params: any) {
    return this.http.get(this.http.apiCommon + '/user-logs?&sort[created_at]=desc' ,{params});
  }
}
export interface ParamsGetData {
  page?: number;
  limit?: number;
  code?: string;
  name?: string;
}