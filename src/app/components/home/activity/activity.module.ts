import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { activityRoutes } from './activity.routes';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalModule, BsDatepickerModule, TabsModule, BsModalRef } from 'ngx-bootstrap';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { CKEditorModule } from 'ngx-ckeditor';
import { Service } from './Service';
import { ActivityComponent } from './activity.component';

@NgModule({
  imports: [
    RouterModule.forChild(activityRoutes),
    SharedModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DirectiveModule,
    CKEditorModule,
    TabsModule.forRoot(),
  ],
  providers: [Service,BsModalRef],
  declarations: [ActivityComponent]
})
export class ActivityModule { }
