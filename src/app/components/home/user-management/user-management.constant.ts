export class UserManagementConstant {
    public static STATUSES = [
    {
        value: '',
        display: 'Select Status'
    },
    {
        value: '1',
        display: 'Active'
    },
    {
        value: '0',
        display: 'Inactive'
    }];

    public static STATUSES_LIST = [
    {
        value: '',
        display: 'All'
    },
    {
        value: 1,
        display: 'Hoạt động'
    },
    {
        value: 0,
        display: 'Ngừng hoạt động'
    }];
}
