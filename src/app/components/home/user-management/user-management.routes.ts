import { Routes } from '@angular/router';
import { UserManagementComponent } from './user-management.component';
import { UserListComponent } from './user-list/user-list.component';
import { CRUUserComponent } from './cru-user/cru-user.component';

export const userRoutes: Routes = [
    {
        path: '',
        component: UserManagementComponent,
        data: {name: 'Quản lý người dùng'},
        children: [
            { path: '', component: UserListComponent, data: { name: 'Danh sách người dùng' } },
            { path: 'new', component: CRUUserComponent, data: { name: 'Tạo mới người dùng', action: 'new' } },
            { path: ':id/:edit', component: CRUUserComponent, data: { name: 'Chỉnh sửa người dùng', action: 'edit' } },
            { path: ':id', component: CRUUserComponent, data: { name: 'Chi tiết người dùng', action: 'view' } }
        ]
    }
];
