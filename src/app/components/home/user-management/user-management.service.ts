import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class UserManagementService {
  constructor(private http: HttpClient) {}

  public createUser(user: any) {
    return this.http.post(this.http.apiMaster + '/users', user);
  }

  public getUserRoles() {
    return this.http.get(this.http.apiMaster + '/users/roles');
  }

  public updateUser(id: any, user: any) {
    return this.http.put(this.http.apiMaster + '/users/' + id, user);
  }
  public  getUserProfile(id:any){
      return this.http.get(this.http.apiCommon + '/users/'+id);
  }

  public getUser(userId: string) {
    return this.http.get(this.http.apiMaster + '/users/' + userId);
  }

  public getUsers(params: any) {
    return this.http.get(this.http.apiMaster + '/users' + params);
  }

  public resetPasswordUser(userId: string, passInfo: any) {
    return this.http.post(
      this.http.apiMaster + '/users/reset-password/' + userId,
      passInfo
    );
  }

  public delete(id: any) {
    return this.http.delete(this.http.apiMaster + '/users/' + id);
  }

  public getDepartments() {
    return this.http.get(this.http.apiCommon + '/user-department');
  }

  public getRoles(params: any) {
    return this.http.get(this.http.apiMaster + '/roles' + params);
  }

  public apiExport() {
    return this.http.apiAuthen + '/users/export';
  }
}
