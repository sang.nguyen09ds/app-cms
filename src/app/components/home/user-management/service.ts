import { Injectable } from '@angular/core';
import { HttpClient } from 'src/app/shared/shared.module';

@Injectable()
export class Service {
  constructor(private http: HttpClient) {}

  public get(params: any) {
    return this.http.get(this.http.apiCommon + '/users', {params});
  }
  public getUserID(id) {
    return this.http.get(this.http.apiCommon + '/users/' + id);
  }
  public create(user: any) {
    return this.http.post(this.http.apiCommon + '/users', user);
  }

  public update(id: any, user: any) {
    return this.http.put(this.http.apiCommon + '/users/' + id, user);
  }

  public delete(id: any) {
    return this.http.delete(this.http.apiCommon + '/users/' + id);
  }
  public getRole(param){
    return this.http.get(this.http.apiCommon + '/roles', param);
  }
  public getDept(param){
    return this.http.get(this.http.apiCommon + '/departments', param);
  }
}
export interface ParamsGetData {
  page?: number;
  limit?: number;
  code?: string;
  department_id?: string;
  full_name?: string;
  is_active?: string;
}
