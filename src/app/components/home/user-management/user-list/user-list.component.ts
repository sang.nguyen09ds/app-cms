import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { Subscription } from 'rxjs';
import { Functions, SwalService, UserPermissionsService } from 'src/app/shared/shared.module';
import { ParamsGetData, Service } from '../service';

@Component({
  templateUrl: 'user-list.html'
})
export class UserListComponent implements OnInit {
  public dataList: Array<any> = [];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;

  constructor(
    private userPermissionsService: UserPermissionsService,
    private _Service: Service,
    private swalService: SwalService,
    private func: Functions,
    private toastyService: ToastyService
  ) {}

  ngOnInit() {
    this.setPermissions();
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-USER',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.getList();
        this.buildSearchForm();
        this.getDept();
      }
    );
  }
  getList(params: ParamsGetData = { page: 1 }) {
      this.subscription = this._Service
        .get({ ...params, limit: this.perPage })
        .subscribe(reponse => {
          this.dataList = reponse.data;
          this.initPagination(reponse);
        });
  }
  public ListDept: Array<any> = [];
  public getDept(){
    let params = '&limit=9999';
    this._Service.getDept(params).subscribe(data =>{
        this.ListDept = data['data'];
    });
  }
  buildSearchForm() {
    this.searchForm = new FormGroup({
      username: new FormControl(''),
      code: new FormControl(''),
      full_name: new FormControl(''),
      phone: new FormControl(''),
      department_id: new FormControl(''),
      is_active: new FormControl('')
    });
  }

  saveDate(data) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this._Service.create(data);
    } else {
      observerChangeData = this._Service.update(data.id, data);
    }
    observerChangeData.subscribe(reponse => {
      if(reponse.status){
        this.toastyService.success(reponse.status);
      }
      else
      {
        this.toastyService.success(
          `${this.isCreate ? 'Create' : 'Update'} success`
        );
      }
      this.pageChange(this.pagination['current_page']);
    });
  }

  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this._Service.delete(id).subscribe(reponse => {
        if(reponse.status){
          this.toastyService.success(reponse.status);
        }
        else
      {
        this.toastyService.success('Delete success');
      }
        this.pageChange(this.pagination['current_page']);
      });
    });
  }

  public search() {
    const valuesSearch = this.searchForm.value;
    this.getList(valuesSearch);
  }

  public reset() {
    this.searchForm.patchValue({
      username: '',
      code: '',
      phone: '',
      full_name: '',
      department_id:'',
      is_active: ''
  });
  this.getList();
  }

  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getList({ ...valuesSearch, page: pageNumber });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }
}
