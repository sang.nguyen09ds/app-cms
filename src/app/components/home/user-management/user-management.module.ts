import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { CRUUserComponent } from './cru-user/cru-user.component';
import { Service } from './service';
import { UserListComponent } from './user-list/user-list.component';
import { UserManagementComponent } from './user-management.component';
import { userRoutes } from './user-management.routes';
import { UserManagementService } from './user-management.service';

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes),
    SharedModule,
    ModalModule.forRoot(),
    DirectiveModule,
  ],
  providers: [UserManagementService, Service],
  declarations: [UserManagementComponent, UserListComponent, CRUUserComponent]
})
export class UserManagementModule {}
