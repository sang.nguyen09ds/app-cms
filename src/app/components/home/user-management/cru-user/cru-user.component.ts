import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseComponentFactory, Ng2FrameworkFactory } from 'ag-grid-angular';
import { ToastyService } from 'ng2-toasty';
import { Languages, SwalService, UserPermissionsService } from 'src/app/shared/shared.module';
import { HomeMessages } from '../../home-messages';
import { Service } from '../service';
import { UserManagementConstant } from '../user-management.constant';
@Component({
  selector: 'cru-user',
  providers: [Ng2FrameworkFactory, BaseComponentFactory, Service, HomeMessages],
  templateUrl: 'cru-user.component.html'
})
export class CRUUserComponent implements OnInit {
  public form: FormGroup;
  public userId = '';
  public statuses: any = UserManagementConstant.STATUSES;
  public action: string;
  public isView = false;
  public departments: any[] = [];
  public roles: any[] = [];
  public selectedRoles: any[] = [];
  public rolesOfUser: any[] = [];
  public roleCodeFilter = '';
  public commonMessages: any;
  public selectedAll = false;
  public submitted = false;
  qrcodeEncode: any;
  userDetail: any;
  customerGroupList: Array<any> = [];
  customerTypeList: Array<any> = [];
  public customer = {
    cus_code: '',
    cus_name: ''
  };
  constructor(
    private userPermissionsService: UserPermissionsService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private toastyService: ToastyService,
    private _service: Service,
    private swalService: SwalService,
    private messages: Languages,
    private homeMessages: HomeMessages
  ) {}

  public ngOnInit(): void {
    this.buildForm();
    this.setPermissions();
    this.commonMessages = this.homeMessages.MESSAGES;
    this.activatedRoute.params.subscribe(params => {
      this.userId = params['id'];
      if (this.userId) {
        this.action = params['edit'] === 'edit' ? 'edit' : 'view';
        if (this.action === 'view') {
        }
        this.fillForm();
      } else {
        this.action = 'new';
      }
    });
  }
  public userPermission = {};
  private setPermissions() {
    this.userPermissionsService.getUserPermissions({
      viewPage: 'VIEW-USER',
      deny: true
    }).subscribe(
      (data) => {
        this.userPermission = data['permissions'];
        this.getRole();
        this.getDept();
      }
    );
  }

  private buildForm(data = {}): void {
    this.form = new FormGroup({
      user_id : new FormControl({ value: '', disabled: this.isView }),
      username: new FormControl({ value: '', disabled: this.isView },Validators.required),
      email: new FormControl({ value: '', disabled: this.isView }),
      code: new FormControl({ value: '', disabled: this.isView }),
      phone: new FormControl({ value: '', disabled: this.isView }),
      full_name: new FormControl({ value: '', disabled: this.isView }),
      is_active: new FormControl({ value: '', disabled: this.isView }),
      password: new FormControl({value: '', disabled: this.isView}),
      department_id: new FormControl({ value: '', disabled: this.isView }),
      role_id: new FormControl({ value: '', disabled: this.isView })
    });
  }

  public updateRoles(): void {
    this.selectedRoles = this.rolesOfUser.slice();
    this.roles = this.roles.map(role => {
      const roleTemp = role;
      for (const userRole of this.rolesOfUser) {
        if (roleTemp.code === userRole.code) {
          roleTemp['selected'] = true;
          break;
        }
      }
      return roleTemp;
    });
  }
  public ListRole: Array<any> = [];
  public getRole(){
    let params = '&limit=9999';
    this._service.getRole(params).subscribe(data =>{
        this.ListRole = data['data'];
        console.log('role',this.ListRole);
    });
  }
  public ListDept: Array<any> = [];
  public getDept(){
    let params = '&limit=9999';
    this._service.getDept(params).subscribe(data =>{
        this.ListDept = data['data'];
        console.log('getDept',this.ListDept);
    });
  }
  private fillForm(): void {
    if (!this.userId) {
      return;
    }
    this._service.getUserID(this.userId).subscribe(
      (res: any) => {
        const user = res['data'];
        this.userDetail = res['data'];
        this.form.patchValue({
          username: user.username,
          code: user.code,
          email: user.email,
          full_name: user.full_name,
          role_id: user.role_id ? user.role_id : 1,
          phone: user.phone,
          is_active: user.is_active,
          department_id: user.department_id ? user.department_id : 1
        });
      },
      err => {}
    );
  }

  public onSubmit(): void {
    this.submitted = true;
    this.userId ? this.update() : this.create();
  }

  public create(): void {
    // TODO: Wait integrate with backend to get correct link.
    this._service.create(this.form.value).subscribe(res => {
      if(res.status){
        this.toastyService.success(res.status);
      }
      else
      {
        this.toastyService.success('Create success');
      }
      this.router.navigate(['/users']);
    });
  }

  public update(): void {
    this._service.update(this.userId, this.form.value).subscribe(res => {
      if(res.status){
        this.toastyService.success(res.status);
      }
      else
      {
        this.toastyService.success('Update success');
      }
      this.router.navigate(['/users']);
    });
  }
  public cancel(): void {
    this.swalService.confirm('Are you sure to cancel?',() => {
      this.router.navigate(['/users']);
    });
  }
}
