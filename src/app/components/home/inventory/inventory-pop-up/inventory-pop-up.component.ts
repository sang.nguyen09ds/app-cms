import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastyService } from 'ng2-toasty';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';
import { ItemService, ValidationService } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-inventory-pop-up',
  templateUrl: './inventory-pop-up.component.html',
  styleUrls: ['./inventory-pop-up.component.css']
})
export class InventoryPopUpComponent implements OnInit {
  public disabled: boolean;
  public Form: FormGroup;

  dataValueChange: Subject<Object>;

  public listItemCode;
  public data;
  private defaultActiveValue = '1';

  labelPopup;

  constructor(
    private formBuilder: FormBuilder,
    private _itemServices: ItemService,
    private toastyService: ToastyService,
    private _validationService: ValidationService,
    private bsModalRef: BsModalRef
  ) {}

  ngOnInit() {
    this.buildForm(this.data);
    this.dataValueChange = new Subject();
  }

  getListItemCode($event = '') {
    const params = { search_string: $event };
    this._itemServices.getListItem(params).subscribe(response => {
      this.listItemCode = response;
      if (!this.listItemCode.length) {
        this.toastyService.error('Not Found !');
      }
    });
  }

  selectItemCode($event) {
    this.Form.patchValue({
      itm_name: $event.itm_name,
      uom_code: $event.uom_code
    });
  }

  buildForm(
    data = {
      is_active: this.defaultActiveValue
    }
  ) {
    if (data['inventory_id']) {
      this.labelPopup = 'Update';
    } else {
      this.labelPopup = 'Create';
    }
    if (!this.disabled) {
      this.loadData(data);
    }

    this.Form = this.formBuilder.group({
      sort: [data['sort']],
      uom_code: [data['uom_code']],
      itm_code: [data['itm_code']],
      itm_name: [data['itm_name']],
      qty: [data['qty'], this._validationService.inPutOnlyInteger],
      inventory_id: [data['inventory_id']],
      unit_price: [data['unit_price'], this._validationService.inPutOnlyNumber]
    });
  }

  loadData(data) {
    this.getListItemCode(data['itm_code']);
  }

  public save() {
    const inventory = this.Form.value;

    this.dataValueChange.next(inventory);
  }

  close() {
    this.bsModalRef.hide();
  }
}
