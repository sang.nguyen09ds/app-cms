import { Routes } from '@angular/router';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryComponent } from './inventory.component';

export const inventoryRoutes: Routes = [
  {
    path: '',
    component: InventoryComponent,
    children: [
      {
        path: '',
        component: InventoryListComponent,
        data: { name: 'Inventory List' }
      }
    ]
  }
];
