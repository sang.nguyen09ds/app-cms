import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { DirectiveModule } from 'src/app/shared/_directives/directive.module';
import { InventoryListComponent } from './inventory-list/inventory-list.component';
import { InventoryPopUpComponent } from './inventory-pop-up/inventory-pop-up.component';
import { InventoryComponent } from './inventory.component';
import { inventoryRoutes } from './inventory.routers';

@NgModule({
  imports: [
    SharedModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    DirectiveModule,
    RouterModule.forChild(inventoryRoutes)
  ],
  entryComponents: [InventoryPopUpComponent],
  declarations: [
    InventoryComponent,
    InventoryListComponent,
    InventoryPopUpComponent
  ]
})
export class InventoryModule {}
