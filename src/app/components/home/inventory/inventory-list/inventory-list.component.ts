import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as FileSaver from 'file-saver';
import { ToastyService } from 'ng2-toasty';
import { BsModalService } from 'ngx-bootstrap';
import { Subscription } from 'rxjs';
import { Inventory } from 'src/app/models/inventory.models';
import { ParamsGetInventory } from 'src/app/shared/services/inventory.service';
import {
  Functions,
  InventoryService,
  SwalService
} from 'src/app/shared/shared.module';
import { InventoryPopUpComponent } from '../inventory-pop-up/inventory-pop-up.component';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {
  public listInventory: Inventory[];
  private subscription: Subscription;
  public searchForm: FormGroup;
  public perPage = 20;
  public pagination: any;
  private isCreate;
  private bomPopUp;
  private subscriber;
  fileName = '';
  fileToUpload: File = null;

  constructor(
    private _inventoryService: InventoryService,
    private swalService: SwalService,
    private formBuilder: FormBuilder,
    private func: Functions,
    private toastyService: ToastyService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.getListInventory();
    this.buildSearchForm();
  }

  buildSearchForm(): void {
    this.searchForm = this.formBuilder.group({
      search_bom: [''],
      search_itm: ['']
    });
  }

  getListInventory(params: ParamsGetInventory = { page: 1 }): void {
    this.subscription = this._inventoryService
      .getList({ ...params, limit: this.perPage })
      .subscribe(reponse => {
        this.listInventory = reponse.data;
        this.initPagination(reponse);
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  openPopUpCreate() {
    this.isCreate = true;
    this.bomPopUp = this.modalService.show(InventoryPopUpComponent, {
      backdrop: 'static'
    });
    this.subscriber = this.bomPopUp.content.dataValueChange.subscribe(data => {
      this.saveData(data);
    });
  }

  openPopUpView(data: Inventory) {
    this.modalService.show(InventoryPopUpComponent, {
      backdrop: 'static',
      initialState: {
        data,
        disabled: true
      }
    });
  }

  openPopUpEdit(data: Inventory) {
    this.isCreate = false;

    this.bomPopUp = this.modalService.show(InventoryPopUpComponent, {
      backdrop: 'static',
      initialState: {
        data,
        disabled: false
      }
    });

    this.subscriber = this.bomPopUp.content.dataValueChange.subscribe(data => {
      this.saveData(data);
    });
  }

  saveData(dataSave: Inventory) {
    let observerChangeData;
    if (this.isCreate) {
      observerChangeData = this._inventoryService.create(dataSave);
    } else {
      observerChangeData = this._inventoryService.update(
        dataSave.inventory_id,
        dataSave
      );
    }
    observerChangeData.subscribe(reponse => {
      this.bomPopUp.hide();
      this.subscriber.unsubscribe();
      this.toastyService.success(
        `${this.isCreate ? 'Create' : 'Update'} success`
      );
      this.pageChange(this.pagination['current_page']);
    });
  }

  public delete(id: any) {
    this.swalService.confirm('Are you sure to delete this item ?', () => {
      this._inventoryService.delete(id).subscribe(reponse => {
        this.toastyService.success(`Delete success`);
        this.pageChange(this.pagination['current_page']);
      });
    });
  }
  public search() {
    const valuesSearch = this.searchForm.value;
    this.getListInventory(valuesSearch);
  }

  public reset() {
    this.searchForm.reset();
    this.getListInventory();
  }

  public onPageSizeChanged($event) {
    this.perPage = $event.target.value;
    if (
      (this.pagination['current_page'] - 1) * this.perPage >=
      this.pagination['total']
    ) {
      this.pagination['current_page'] = 1;
    }
    this.pageChange(this.pagination['current_page']);
  }

  public pageChange(pageNumber) {
    const valuesSearch = this.searchForm.value;
    this.getListInventory({ ...valuesSearch, page: pageNumber });
  }

  private initPagination(data) {
    const { meta } = data;
    if (meta && meta['pagination']) {
      this.pagination = meta['pagination'];
    } else {
      const { perPage, currentPage, totalCount } = meta;
      const count =
        perPage * currentPage > totalCount
          ? perPage * currentPage - totalCount
          : perPage;
      this.pagination = {
        total: totalCount,
        count: count,
        per_page: perPage,
        current_page: currentPage,
        total_pages: meta['pageCount'],
        links: {
          next: data['link']['next']
        }
      };
    }
    this.pagination['numLinks'] = 3;
    this.pagination['tmpPageCount'] = this.func.processPaging(this.pagination);
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileName = this.fileToUpload.name;
    console.log(this.fileToUpload);
  }

  import() {
    this._inventoryService.importIventory(this.fileToUpload).subscribe(res => {
      this.getListInventory();
      this.fileName = '';
      FileSaver.saveAs(
        res,
        'Result_Import_' + new Date().getTime() + EXCEL_EXTENSION
      );
    });
  }

  exportIventory() {
    this._inventoryService.exportIventory().subscribe(res => {
      FileSaver.saveAs(
        res,
        'Inventory_Export_' + new Date().getTime() + EXCEL_EXTENSION
      );
    });
  }
}
