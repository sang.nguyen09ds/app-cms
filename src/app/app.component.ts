import { Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css',
  ]
})
export class AppComponent {
  title = 'app';
  constructor() {}
}
